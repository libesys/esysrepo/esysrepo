/*!
 * \file esys/repo/libgit2/guard.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/libgit2/guardrelease.h"
#include "esys/repo/libgit2/guardassign.h"
#include "esys/repo/libgit2/guardequal.h"
#include "esys/repo/libgit2/guardprint.h"

namespace esys::repo::libgit2
{

template<typename T>
class Guard
{
public:
    template<typename U>
    friend int guard_assign(Guard<U> *dest, const Guard<U> &src);

    template<typename U>
    friend bool guard_equal(const Guard<U> &left, const Guard<U> &right);

    template<typename U>
    friend void guard_print(std::ostream &os, const Guard<U> &object);

    Guard();
    ~Guard();

    T *get() const;
    T **get_p();

    void reset();

    void operator=(const Guard &other);

    //! Equal to comparison operator
    bool operator==(const Guard &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Guard &other) const;

private:
    T *m_data = nullptr;
};

template<typename T>
Guard<T>::Guard()
{
}

template<typename T>
Guard<T>::~Guard()
{
    guard_release<T>(m_data);
}

template<typename T>
T *Guard<T>::get() const
{
    return m_data;
}

template<typename T>
T **Guard<T>::get_p()
{
    return &m_data;
}

template<typename T>
void Guard<T>::reset()
{
    if (m_data == nullptr) return;

    guard_release<T>(m_data);
    m_data = nullptr;
}

template<typename T>
void Guard<T>::operator=(const Guard &other)
{
    int result = guard_assign<T>(this, other);
    // \TODO throw exception??
}

template<typename T>
bool Guard<T>::operator==(const Guard &other) const
{
    if ((get() == nullptr) && (other.get() == nullptr)) return true;
    if ((get() == nullptr) || (other.get() == nullptr)) return false;

    return guard_equal(*this, other);
}

template<typename T>
bool Guard<T>::operator!=(const Guard &other) const
{
    return !operator==(other);
}

template<typename T>
void libgit2_print(std::ostream &os, const Guard<T> &object)
{
    guard_print(os, object);

    //os << "<" << typeid(T).name()  << ">";
}

} // namespace esys::repo::libgit2

namespace std
{

template<typename T>
ostream &operator<<(ostream &os, const esys::repo::libgit2::Guard<T> &guard)
{
    if (guard.get() == nullptr)
    {
        os << "<nullptr>";
        return os;
    }

    esys::repo::libgit2::libgit2_print(os, guard);
    return os;
}

} // namespace std
