/*!
 * \file esys/repo/manifest/directoryfilexmlimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/directoryfilexml.h"

#include <esysfile/xml/data.h>

#include <string>

namespace esys::repo::manifest
{

class ESYSREPO_API DirectoryFileXMLImpl;

/*! \class DirectoryFileXMLImpl esys/repo/manifest/directoryfilexmlimpl.h "esys/repo/manifest/directoryfilexmlimpl.h"
 * \brief
 */
class ESYSREPO_API DirectoryFileXMLImpl
{
public:
    //! Default constructor
    DirectoryFileXMLImpl(DirectoryFileXML *self);

    //! Destructor
    ~DirectoryFileXMLImpl();

    Result read(const std::string &path);

    Result read(std::istream &is);

    Result write(const std::string &path);

    Result write(std::ostream &os);

    DirectoryFileXML *self() const;

    Result write(std::shared_ptr<esysfile::xml::Element> parent_el, std::shared_ptr<Directory> directory);
    Result write_xml();

    Result read(std::shared_ptr<esysfile::xml::Data> data);
    Result read_projects(std::shared_ptr<esysfile::xml::Element> el);

private:
    //!< \cond DOXY_IMPL
    DirectoryFileXML *m_self = nullptr;
    std::shared_ptr<esysfile::xml::Data> m_xml_data;
    //!< \endcond
};

} // namespace esys::repo::manifest
