/*!
 * \file esys/repo/ssh.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#ifndef ESYSREPO_USE_LIBSSH2
#define ESYSREPO_USE_LIBSSH2 1
#endif

#include "esys/repo/libssh2/ssh.h"

//<swig_inc/>
