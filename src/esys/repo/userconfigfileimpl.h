/*!
 * \file esys/repo/userconfigfileimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/userconfigfile.h"

#include <esysfile/xml/data.h>

namespace esys::repo
{

/*! \class UserConfigFileImpl esys/repo/userconfigfileimpl.h "esys/repo/userconfigfileimpl.h"
 * \brief PIMPL for UserConfigFile
 */
class ESYSREPO_API UserConfigFileImpl
{
public:
    //! Default constructor
    UserConfigFileImpl(UserConfigFile *self);

    //! Destructor
    ~UserConfigFileImpl();

    //! Open and read a config file
    /*!
     * \param[in] path the path of the config file to open and read
     */
    Result read(const std::string &path = "");

    //! Write a config file
    /*!
     * \param[in] path the path of the config file to write
     */
    Result write(const std::string &path = "");

    Result write_xml();
    Result write(std::shared_ptr<esysfile::xml::Element> parent_el, std::shared_ptr<UserConfig> user_config);
    Result write(std::shared_ptr<esysfile::xml::Element> parent_el,
                 std::shared_ptr<manifest::Directories> manifest_directories);

    Result read(std::shared_ptr<esysfile::xml::Data> data);
    Result read_manifest_directories(std::shared_ptr<esysfile::xml::Element> parent_el);
    Result read_paths(std::shared_ptr<esysfile::xml::Element> parent_el);

    //! Get self
    /*!
     * \return self
     */
    UserConfigFile *self();

    //! Get the PIMPL
    /*!
     * \return the PIMPL
     */
    const UserConfigFile *self() const;

private:
    //!< \cond DOXY_IMPL
    UserConfigFile *m_self = nullptr; //!< self
    std::shared_ptr<esysfile::xml::Data> m_xml_data;

    //!< \endcond
};

} // namespace esys::repo
