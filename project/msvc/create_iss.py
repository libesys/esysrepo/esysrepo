import argparse
import jinja2
import os


class CreateISS:
    def __init__(self) -> None:
        self.m_args = None
        self.m_name = None
        self.m_script_path = None
        self.m_rel_search_path = None
        self.m_sign = False

    def set_name(self, name):
        self.m_name = name

    def get_name(self):
        return self.m_name

    def set_args(self, args):
        self.m_args = args

    def get_args(self):
        return self.m_args

    def set_script_path(self, script_path):
        self.set_script_folder(os.path.dirname(script_path))

    def set_script_folder(self, script_folder):
        self.m_script_folder = script_folder

    def get_script_folder(self):
        return self.m_script_folder

    def set_rel_search_path(self, rel_search_path):
        self.m_rel_search_path = rel_search_path

    def get_rel_search_path(self):
        return self.m_rel_search_path

    def set_sign(self, sign):
        self.m_sign = sign

    def get_sign(self):
        return self.m_sign

    def handle_cmd_line(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "--name",
            dest="name",
            action="store",
            default=None,
            required=True,
            help="base filename",
        )
        parser.add_argument(
            "--sign",
            dest="sign",
            action="store_true",
            default=False,
            help="create a ISS to sign all files",
        )
        args = parser.parse_args()
        self.set_args(args)
        self.set_name(args.name)
        if args.sign:
            self.set_sign(True)

    def do(self):
        script_folder = self.get_script_folder()
        if self.get_rel_search_path() is None:
            input_path = os.path.join(script_folder, ".")
        else:
            input_path = os.path.join(script_folder, self.get_rel_search_path())

        output_path = input_path
        template_file = self.get_name() + ".j2"
        output_file = self.get_name() + ".iss"

        print("Template file : %s" % template_file)
        print("Output file   : %s" % output_file)

        loader = jinja2.FileSystemLoader(searchpath=input_path)
        env = jinja2.Environment(loader=loader)
        env.variable_start_string = "[["
        env.variable_end_string = "]]"
        env.block_start_string = "[%"
        env.block_end_string = "%]"
        env.comment_start_string = "[#"
        env.comment_end_string = "#]"

        template = env.get_template(template_file)

        if self.get_sign():
            setup_sign_tool = "SignTool=default"
            define_sign = '#define SIGN "sign"'
        else:
            setup_sign_tool = ""
            define_sign = ""

        data = {
            "define_sign": define_sign,
            "setup_sign_tool": setup_sign_tool,
        }

        # this is where to put args to the template renderer
        output_text = template.render(data)

        # print(output_text)

        output_path = os.path.join(output_path, output_file)
        with open(output_path, "w", encoding="utf-8") as f:
            f.write(output_text)

        return 0


if __name__ == "__main__":
    print("Creating ISS file ...")

    create_iss = CreateISS()
    create_iss.set_script_path(__file__)
    create_iss.handle_cmd_line()

    result = create_iss.do()
    if result == 0:
        print("Creating ISS file done.")
    else:
        print("Error creating ISS file.")
    exit(-result)
