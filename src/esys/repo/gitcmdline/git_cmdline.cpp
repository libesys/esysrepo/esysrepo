/*!
 * \file esys/build/gitcmdline/git_cmdline.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/gitcmdline/git.h"

namespace esys::repo::gitcmdline
{

Git::Git()
    : GitBase()
{
}

Git::~Git() = default;

} // namespace esys::repo::gitcmdline
