/*!
 * \file esys/repo/userconfigfile.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/userconfigfile.h"
#include "esys/repo/userconfigfileimpl.h"

namespace esys::repo
{

UserConfigFile::UserConfigFile()
{
    m_impl = std::make_unique<UserConfigFileImpl>(this);
}

UserConfigFile::~UserConfigFile() = default;

void UserConfigFile::set_path(const std::string &path)
{
    m_path = path;
}

const std::string &UserConfigFile::get_path() const
{
    return m_path;
}

void UserConfigFile::set_config(std::shared_ptr<UserConfig> user_config)
{
    m_user_config = user_config;
}

std::shared_ptr<UserConfig> UserConfigFile::get_config() const
{
    return m_user_config;
}

Result UserConfigFile::read(const std::string &path)
{
    return get_impl()->read(path);
}

Result UserConfigFile::write(const std::string &path)
{
    return get_impl()->write(path);
}

UserConfigFileImpl *UserConfigFile::get_impl()
{
    return m_impl.get();
}

const UserConfigFileImpl *UserConfigFile::get_impl() const
{
    return m_impl.get();
}

} // namespace esys::repo
