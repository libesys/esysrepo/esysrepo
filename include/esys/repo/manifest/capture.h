/*!
 * \file esys/repo/manifest/capture.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

namespace esys::repo::manifest
{

/*! \class Capture esys/repo/manifest/capture.h "esys/repo/manifest/capture.h"
 * \brief Capture the branch of a set of repositories
 */
class ESYSREPO_API Capture
{
public:
    class ESYSREPO_API Item
    {
    public:
        Item();
        Item(const std::string &path, const std::string &revision);
        ~Item();

        void set_path(const std::string &path);
        const std::string &get_path() const;

        void set_revision(const std::string &revision);
        const std::string &get_revision() const;

        void set_remote_name(const std::string &remote_name);
        const std::string &get_remote_name() const;

        void set_remote_url(const std::string &remote_url);
        const std::string &get_remote_url() const;

        //! Equal to comparison operator
        bool operator==(const Item &other) const;

        //! Not equal to comparison operator
        bool operator!=(const Item &other) const;

    private:
        std::string m_path;
        std::string m_revision;
        std::string m_remote_name;
        std::string m_remote_url;
    };

    //! Default constructor
    Capture();

    //! Destructor
    ~Capture();

    void add_item(const std::string &path, const std::string &branch);
    void add_item(std::shared_ptr<Item> item);

    void clear();

    std::shared_ptr<Item> find_item(const std::string &path) const;

    std::vector<std::shared_ptr<Item>> &get_items();
    const std::vector<std::shared_ptr<Item>> &get_items() const;

    std::map<std::string, std::shared_ptr<Item>, std::less<>> &get_map();
    const std::map<std::string, std::shared_ptr<Item>, std::less<>> &get_map() const;

    //! Equal to comparison operator
    bool operator==(const Capture &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Capture &other) const;

private:
    //!< \cond DOXY_IMPL
    std::vector<std::shared_ptr<Item>> m_items;
    std::map<std::string, std::shared_ptr<Item>, std::less<>> m_map_path_item;
    //!< \endcond
};

} // namespace esys::repo::manifest
