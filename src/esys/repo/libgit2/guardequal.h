/*!
 * \file esys/repo/libgit2/guardequal.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <git2.h>

#include <memory>
#include <cassert>

namespace esys::repo::libgit2
{

template<typename T>
class Guard;

template<typename T>
bool guard_equal(const Guard<T> &left, const Guard<T> &right)
{
    assert(false);

    return false;
}

template<>
ESYSREPO_API bool guard_equal<git_remote>(const Guard<git_remote> &left, const Guard<git_remote> &right);

} // namespace esys::repo::libgit2
