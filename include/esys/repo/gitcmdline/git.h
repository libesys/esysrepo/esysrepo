/*!
 * \file esys/repo/gitcmdline/git.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/gitbase.h"

namespace esys
{

namespace repo
{

namespace gitcmdline
{

class ESYSREPO_API Git : public GitBase
{
public:
    Git();
    ~Git() override;
};

} // namespace gitcmdline

} // namespace repo

} // namespace esys
