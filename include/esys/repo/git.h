/*!
 * \file esys/repo/git.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#ifndef ESYSREPO_USE_LIBGIT2
#define ESYSREPO_USE_LIBGIT2 1
#endif

#include "esys/repo/libgit2/git.h"

//<swig_inc/>
