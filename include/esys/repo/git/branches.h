/*!
 * \file esys/repo/git/branches.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/branch.h"

#include <memory>
#include <vector>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class Branch esys/repo/git/branches.h "esys/repo/git/branches.h"
 * \brief Hold information about all branches of a git repo
 */
class ESYSREPO_API Branches
{
public:
    //! Default constructor
    Branches();

    //! Constructor
    explicit Branches(const std::vector<Branch> &branches);

    //! Destructor
    virtual ~Branches();

    //! Add a branch
    /*!
     * \param[in] branch the branch to add
     */
    void add(std::shared_ptr<Branch> branch);

    //! Clear all branches
    /*!
     */
    void clear();

    //! Get the head
    /*!
     * \param[in] is_head true if this branch is the head/default branch
     */
    std::shared_ptr<Branch> get_head() const;

    std::size_t size() const;

    //! Sort the branches such that the head is the first one
    /*!
     */
    void sort();

    std::vector<std::shared_ptr<Branch>> &get();
    const std::vector<std::shared_ptr<Branch>> &get() const; //<swig_out/>

    std::shared_ptr<Branch> find(const std::string &name);

    //! Equal to comparison operator
    bool operator==(const Branches &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Branches &other) const;


private:
    //!< \cond DOXY_IMPL
    std::vector<std::shared_ptr<Branch>> m_branches; //!< All the branches
    std::shared_ptr<Branch> m_head;
    //!< \endcond
};

} // namespace esys::repo::git

namespace std
{
ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::Branches &branches); //<swig_out/>
}
