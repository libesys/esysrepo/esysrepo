/*!
 * \file esys/repo/manifest/directory.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

namespace esys::repo::manifest
{

/*! \class Directory esys/repo/manifest/directory.h "esys/repo/manifest/directory.h"
 * \brief Hold a list of projects with their manifest URL
 */
class ESYSREPO_API Directory
{
public:
    class ESYSREPO_API Item
    {
    public:
        Item();
        ~Item();

        void set_name(const std::string &name);
        const std::string &get_name() const;

        void set_url_ssh(const std::string &url_ssh);
        const std::string &get_url_ssh() const;

        void set_url_https(const std::string &url_https);
        const std::string &get_url_https() const;

        //! Equal to comparison operator
        bool operator==(const Item &other) const;

        //! Not equal to comparison operator
        bool operator!=(const Item &other) const;

    private:
        std::string m_name;
        std::string m_url_ssh;
        std::string m_url_https;
    };

    //! Default constructor
    Directory();

    //! Destructor
    ~Directory();

    void set_name(const std::string &name);
    const std::string &get_name() const;

    void add_item(const std::string &name, const std::string &url_ssh, const std::string &url_https = "");
    void add_item(std::shared_ptr<Item> item);

    const std::vector<std::shared_ptr<Item>> &get_items() const;

    std::shared_ptr<Item> find(const std::string &name) const;

    void clear();

    //! Equal to comparison operator
    bool operator==(const Directory &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Directory &other) const;


private:
    //!< \cond DOXY_IMPL
    std::string m_name;
    std::vector<std::shared_ptr<Item>> m_items;
    std::map<std::string, std::shared_ptr<Item>, std::less<>> m_map_items;
    //!< \endcond
};

} // namespace esys::repo::manifest
