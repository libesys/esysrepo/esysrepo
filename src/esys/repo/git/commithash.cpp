/*!
 * \file esys/repo/git/commit.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/commit.h"

#include <ostream>

namespace esys::repo::git
{

CommitHash::CommitHash() = default;

CommitHash::~CommitHash() = default;

void CommitHash::set_hash(const std::string &hash)
{
    m_hash = hash;
}

const std::string &CommitHash::get_hash() const
{
    return m_hash;
}

std::string &CommitHash::get_hash()
{
    return m_hash;
}

bool CommitHash::operator==(const CommitHash &other) const
{
    return get_hash() == other.get_hash();
}

bool CommitHash::operator!=(const CommitHash &other) const
{
    return get_hash() != other.get_hash();
}

} // namespace esys::repo::git

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::CommitHash &commit_hash)
{
    os << commit_hash.get_hash();
    return os;
}

} // namespace std
