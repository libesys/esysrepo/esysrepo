/*!
 * \file esys/repo/git/filestatus_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/filestatus.h"

namespace esys::repo::git
{

FileStatus::FileStatus(const std::string &old_name)
    : m_old_name(old_name)
{
}

FileStatus::~FileStatus() = default;

void FileStatus::set_old_name(const std::string &old_name)
{
    m_old_name = old_name;
}

const std::string &FileStatus::get_old_name() const
{
    return m_old_name;
}

void FileStatus::add(std::shared_ptr<Status> status)
{
    m_status.push_back(status);

    if (status->get_type() == StatusType::INDEX)
        set_head_to_index(status);
    else if (status->get_type() == StatusType::WORKING_DIR)
        set_index_to_work_dir(status);
}

void FileStatus::set_head_to_index(std::shared_ptr<Status> head_to_index)
{
    m_head_to_index = head_to_index;
}

std::shared_ptr<Status> FileStatus::get_head_to_index() const
{
    return m_head_to_index;
}

void FileStatus::set_index_to_work_dir(std::shared_ptr<Status> index_to_work_dir)
{
    m_index_to_work_dir = index_to_work_dir;
}

std::shared_ptr<Status> FileStatus::get_index_to_work_dir() const
{
    return m_index_to_work_dir;
}

std::vector<std::shared_ptr<Status>> &FileStatus::get_status()
{
    return m_status;
}

const std::vector<std::shared_ptr<Status>> &FileStatus::get_status() const
{
    return m_status;
}

bool FileStatus::operator==(const FileStatus &other) const
{
    if (get_old_name() != other.get_old_name()) return false;
    if ((get_head_to_index() != nullptr) && (other.get_head_to_index() != nullptr))
    {
        if (*get_head_to_index() != *other.get_head_to_index()) return false;
    }
    else if ((get_head_to_index() != nullptr) || (other.get_head_to_index() != nullptr))
        return false;

    if ((get_index_to_work_dir() != nullptr) && (other.get_index_to_work_dir() != nullptr))
    {
        if (*get_index_to_work_dir() != *other.get_index_to_work_dir()) return false;
    }
    else if ((get_index_to_work_dir() != nullptr) || (other.get_index_to_work_dir() != nullptr))
        return false;

    bool equal = is_equal(get_status(), other.get_status());
    if (!equal) return false;
    return true;
}

bool FileStatus::is_equal(const std::vector<std::shared_ptr<Status>> &left,
                          const std::vector<std::shared_ptr<Status>> &right) const
{
    if (left.size() != right.size()) return false;

    for (auto idx = 0; idx < left.size(); ++idx)
    {
        if ((left[idx] == nullptr) && (right[idx] == nullptr)) return true;
        if ((left[idx] == nullptr) || (right[idx] == nullptr)) return false;
        if (*left[idx] != *right[idx]) return false;
    }
    return true;
}

bool FileStatus::operator!=(const FileStatus &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git
