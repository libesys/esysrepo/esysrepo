/*!
 * \file esys/repo/manifest/multifilexml.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/multifilebase.h"

#include <memory>

namespace esys::repo::manifest
{

class ESYSREPO_API MultiFileXMLImpl;

/*! \class MultiFileXML esys/repo/manifest/multifileXML.h "esys/repo/manifest/multifileXML.h"
 * \brief To store the content of a Capture instance to a file
 */
class ESYSREPO_API MultiFileXML : public MultiFileBase
{
public:
    //! Default constructor
    MultiFileXML();

    //! Destructor
    ~MultiFileXML() override;

    Result read(const std::string &path) override;

    Result read(std::istream &is) override;

    Result write(const std::string &path) override;

    Result write(std::ostream &os) override;

    MultiFileXMLImpl *get_impl() const;

private:
    //!< \cond DOXY_IMPL
    std::unique_ptr<MultiFileXMLImpl> m_impl;
    //!< \endcond
};

} // namespace esys::repo::manifest
