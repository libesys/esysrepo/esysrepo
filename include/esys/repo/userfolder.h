/*!
 * \file esys/repo/userfolder.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/result.h"
#include "esys/repo/userconfig.h"

#include <esys/log/user.h>

#include <string>
#include <vector>
#include <memory>

//<swig_inc/>

//<swig>%shared_ptr(esys::repo::UserFolder);</swig>

namespace esys::repo
{

/*! \class ConfigFolder esys/repo/userfolder.h "esys/repo/userfolder.h"
 * \brief Handle the user folder used by ESysRepo to store information
 */
class ESYSREPO_API UserFolder : public log::User
{
public:
    //! Default constructor
    UserFolder();

    //! Destructor
    ~UserFolder() override;

    //! Set the path of the UserFolder
    /*!
     * \param[in] path the path of the UserFolder
     */
    void set_path(const std::string &path);

    //! Get the path of the UserFolder
    /*!
     * \return the path of the UserFolder
     */
    const std::string &get_path() const;

    //! Set the home path of the user
    /*!
     * \param[in] user_home_path the home path of the user
     */
    void set_user_home_path(const std::string &user_home_path);

    //! Get the home path of the user
    /*!
     * \return the home path of the user
     */
    const std::string &get_user_home_path() const;

    //! Set the path of the UserConfigFile in the user folder
    /*!
     * \return the path of the UserConfigFile in the user folder
     */
    void set_user_config_file_path(const std::string &user_config_file_path);

    //! Get the path of the UserConfigFile in the user folder
    /*!
     * \return the path of the UserConfigFile in the user folder
     */
    const std::string &get_user_config_file_path() const;

    void set_user_config(std::shared_ptr<UserConfig> user_config);
    std::shared_ptr<UserConfig> get_user_config() const;

    //! Create a new UserFolder
    /*!
     * \param[in] path the path of the UserFolder
     */
    Result create(bool ok_if_exists = false);

    //! Open a UserFolder
    /*!
     * \param[in] path the path of the UserFolder
     */
    //<swig>%rename(open_folder) open;</swig>
    Result read(const std::string &path = "", bool print_error = true);

    //! Write and update a UserFolder
    /*!
     * \param[in] path the path of the UserFolder
     */
    Result write(const std::string &path = "");

    //! Populate all paths based on path of the UserFolder
    Result populate_all_pathes();

    //! Detect the home folder of the user
    /*!
     * \return
     */
    Result detect_home_folder();

private:
    Result create_folder(const std::string &folder_path, bool ok_if_exists = false);

    //!< \cond DOXY_IMPL
    std::string m_user_home_path;                 //!< The path of the user home
    std::string m_path;                           //!< The path of the user folder
    std::string m_user_config_file_path;          //!< The path of the user configuration file
    std::shared_ptr<UserConfig> m_user_config;          //!< The user configuration data
    // std::shared_ptr<UserConfigFile> m_config_file; //!< The user configuration file
    //!< \endcond
};

} // namespace esys::repo
