/*!
 * \file esys/repo/version.h
 * \brief Version info for esysrepo
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/version_defs.h"

// Don't bump-up manually, this is done automatically with the tool Commitizen
#define ESYSREPO_VERSION_NUM_DOT_STRING "0.0.1"
#define ESYSREPO_VERSION_NUM_STRING "0001"

//<swig_inc/>

namespace esys::repo
{

ESYSREPO_API int get_major_version();
ESYSREPO_API int get_minor_version();
ESYSREPO_API int get_patch_version();

ESYSREPO_API bool check_at_least_version(int major, int minor, int patch = -1);
ESYSREPO_API bool check_at_least_version(const char *version, int major, int minor, int patch = -1);

ESYSREPO_API int get_int_till_dot(const char *str, int skip_dot = 0, bool till_end_str = false);

} // namespace esys::repo
