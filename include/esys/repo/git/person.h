/*!
 * \file esys/repo/git/person.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <string>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class Person esys/repo/git/person.h "esys/repo/git/person.h"
 * \brief Hold information about a person
 */
class ESYSREPO_API Person
{
public:
    //! Default constructor
    Person();

    //! Destructor
    ~Person();

    //! Set the name of the author
    /*!
     * \param[in] name the name of the author
     */
    void set_name(const std::string &name);

    //! Get the name of the author
    /*!
     * \return the name of the author
     */
    const std::string &get_name() const;

    //! Set the email of the author
    /*!
     * \param[in] email the email of the author
     */
    void set_email(const std::string &email);

    //! Get the email of the author
    /*!
     * \return the email of the author
     */
    const std::string &get_email() const;

    //! Equal to comparison operator
    bool operator==(const Person &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Person &other) const;

private:
    //!< \cond DOXY_IMPL
    std::string m_name;  //!< The name of the author
    std::string m_email; //!< The email of the author
    //!< \endcond
};

} // namespace esys::repo::git
