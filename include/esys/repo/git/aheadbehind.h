/*!
 * \file esys/repo/git/aheadbehind.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <cstddef>
#include <ostream>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class AheadBehind esys/repo/git/aheadbehind.h "esys/repo/git/aheadbehind.h"
 * \brief Hold information about the number of commits ahead or behind
 */
class ESYSREPO_API AheadBehind
{
public:
    //! Default constructor
    AheadBehind();

    //! Set the number of ahead commits
    /*!
     * \param[in] ahead the number of ahead commits
     */
    void set_ahead(std::size_t ahead);

    //! Get the number of ahead commits
    /*!
     * \return the number of ahead commits
     */
    std::size_t get_ahead() const;

    //! Set the number of behind commits
    /*!
     * \param[in] behind the number of behind commits
     */
    void set_behind(std::size_t behind);

    //! Get the number of behind commits
    /*!
     * \return the number of behind commits
     */
    std::size_t get_behind() const;

    //! Equal to comparison operator
    bool operator==(const AheadBehind &other) const;

    //! Not equal to comparison operator
    bool operator!=(const AheadBehind &other) const;

private:
    //!< \cond DOXY_IMPL
    std::size_t m_ahead = 0;  //!< The number of ahead commits
    std::size_t m_behind = 0; //!< The number of behind commits
    //!< \endcond
};

} // namespace esys::repo::git

namespace std
{
ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::AheadBehind &ahead_behind); //<swig_out/>
}
