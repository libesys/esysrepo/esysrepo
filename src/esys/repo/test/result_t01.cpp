/*!
 * \file esys/repo/test/result_t01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/result_t.h>
#include <esys/repo/errorstack.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::test
{

namespace result_t01
{

Result_t<bool> fct_a()
{
    return true;
}

Result_t<bool> fct_b()
{
    return ESYSREPO_RESULT_T(ResultCode::GENERIC_ERROR, false);
}

Result_t<bool> fct_c()
{
    Result_t<bool> result = fct_b();

    return ESYSREPO_RESULT_T(result);
}

Result fct_d()
{
    return ESYSREPO_RESULT(ResultCode::ERROR_OPENING_FILE);
}

Result_t<bool> fct_e()
{
    Result result = fct_d();

    return ESYSREPO_RESULT_T(result, true);
}

std::string fct_f_err_str = "This is an error";

Result_t<bool> fct_f()
{
    Result result = fct_d();

    return ESYSREPO_RESULT_T(result, true, fct_f_err_str);
}

/*! \class Result_t01 esys/repo/test/result_t01.cpp "esys/repo/test/result_t01.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(Result_t01)
{
    auto result = fct_a();
    ESYSTEST_REQUIRE_EQUAL(result.success(), true);
    ESYSTEST_REQUIRE_EQUAL(result.error(), false);
    ESYSTEST_REQUIRE_EQUAL(result, ResultCode::OK);
    ESYSTEST_REQUIRE_EQUAL(result.get_error_info(), nullptr);
    ESYSTEST_REQUIRE_EQUAL(result.get(), true);
    ESYSTEST_REQUIRE_EQUAL(result(), true);

    result = fct_b();
    ESYSTEST_REQUIRE_EQUAL(result.success(), false);
    ESYSTEST_REQUIRE_EQUAL(result.error(), true);
    ESYSTEST_REQUIRE_EQUAL(result, ResultCode::GENERIC_ERROR);
    ESYSTEST_REQUIRE_NE(result.get_error_info(), nullptr);
    ESYSTEST_REQUIRE_EQUAL(result.get_error_info()->get_line_number(), 40);
    ESYSTEST_REQUIRE_EQUAL(result.get(), false);
    ESYSTEST_REQUIRE_EQUAL(result(), false);

    boost::filesystem::path path = result.get_error_info()->get_file();
    ESYSTEST_REQUIRE_EQUAL(path.filename(), "result_t01.cpp");

    result = fct_c();
    ESYSTEST_REQUIRE_EQUAL(result.success(), false);
    ESYSTEST_REQUIRE_EQUAL(result.error(), true);
    ESYSTEST_REQUIRE_EQUAL(result, ResultCode::GENERIC_ERROR);
    ESYSTEST_REQUIRE_NE(result.get_error_info(), nullptr);
    ESYSTEST_REQUIRE_EQUAL(result.get_error_info()->get_line_number(), 47);
    ESYSTEST_REQUIRE_EQUAL(result.get(), false);
    ESYSTEST_REQUIRE_EQUAL(result(), false);

    std::shared_ptr<ErrorInfo> error_info = result.get_error_info()->get_prev();
    ESYSTEST_REQUIRE_NE(error_info, nullptr);
    ESYSTEST_REQUIRE_EQUAL(error_info->get_line_number(), 40);
    ESYSTEST_REQUIRE_EQUAL(error_info->get_prev(), nullptr);

    ErrorStack error_stack(&result);

    int result_int = error_stack.analyze();
    ESYSTEST_REQUIRE_EQUAL(result_int, 0);
    ESYSTEST_REQUIRE_EQUAL(error_stack.get_error_infos().size(), 2);

    std::cout << error_stack.get_output() << std::endl;

    result = fct_e();
    ESYSTEST_REQUIRE_EQUAL(result.success(), false);
    ESYSTEST_REQUIRE_EQUAL(result.error(), true);
    ESYSTEST_REQUIRE_EQUAL(result, ResultCode::ERROR_OPENING_FILE);
    ESYSTEST_REQUIRE_NE(result.get_error_info(), nullptr);
    ESYSTEST_REQUIRE_EQUAL(result.get_error_info()->get_line_number(), 59);
    ESYSTEST_REQUIRE_EQUAL(result.get(), true);
    ESYSTEST_REQUIRE_EQUAL(result(), true);

    error_info = result.get_error_info()->get_prev();
    ESYSTEST_REQUIRE_NE(error_info, nullptr);
    ESYSTEST_REQUIRE_EQUAL(error_info->get_line_number(), 52);
    ESYSTEST_REQUIRE_EQUAL(error_info->get_prev(), nullptr);

    result_int = error_stack.analyze();
    ESYSTEST_REQUIRE_EQUAL(result_int, 0);
    ESYSTEST_REQUIRE_EQUAL(error_stack.get_error_infos().size(), 2);

    std::cout << error_stack.get_output() << std::endl;

    result = fct_f();
    ESYSTEST_REQUIRE_EQUAL(result.success(), false);
    ESYSTEST_REQUIRE_EQUAL(result.error(), true);
    ESYSTEST_REQUIRE_EQUAL(result.get_error_info()->get_text(), fct_f_err_str);

}

} // namespace result01_t

} // namespace esys::repo::test
