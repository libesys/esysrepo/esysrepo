/*!
 * \file esys/repo/manifest/directories.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/directory.h"
#include "esys/repo/result.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

namespace esys::repo::manifest
{

/*! \class Directories esys/repo/manifest/directories.h "esys/repo/manifest/directories.h"
 * \brief Hold a list of projects with their manifest URL
 */
class ESYSREPO_API Directories
{
public:
    class ESYSREPO_API Item : public Directory::Item
    {
    public:
        Item();
        ~Item();

        void set_directory(std::shared_ptr<Directory> directory);
        std::shared_ptr<Directory> get_directory() const;

        //! Equal to comparison operator
        bool operator==(const Item &other) const;

        //! Not equal to comparison operator
        bool operator!=(const Item &other) const;

    private:
        std::shared_ptr<Directory> m_directory;
    };

    //! Default constructor
    Directories();

    //! Destructor
    ~Directories();

    void add_item(const std::string &name, std::shared_ptr<Item> Item);
    Result del_item(const std::string &name);

    const std::vector<std::shared_ptr<Item>> &get_items() const;

    std::shared_ptr<Item> find_item(const std::string &name) const;
    std::shared_ptr<Directory::Item> find_project(const std::string &name) const;

    void add_projects(std::shared_ptr<Directory> directory);

    void clear();

    void add_default_libesys();

    //! Equal to comparison operator
    bool operator==(const Directories &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Directories &other) const;

private:
    //!< \cond DOXY_IMPL
    std::vector<std::shared_ptr<Item>> m_items;
    std::map<std::string, std::shared_ptr<Item>, std::less<>> m_map_items;
    std::map<std::string, std::shared_ptr<Directory::Item>, std::less<>> m_map_projects;
    //!< \endcond
};

} // namespace esys::repo::manifest
