/*!
 * \file esys/repo/git/walkcommit_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/walkcommit.h"

namespace esys::repo::git
{

WalkCommit::WalkCommit() = default;

WalkCommit::~WalkCommit() = default;

} // namespace esys::repo::git
