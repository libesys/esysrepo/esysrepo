/*!
 * \file esys/repo/git/noteid.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/commithash.h"

#include <ostream>
#include <string>

//<swig_inc/>

namespace esys::repo::git
{

class ESYSREPO_API NoteId : public CommitHash
{
public:
    NoteId();
    ~NoteId();

    void set_reference(const std::string &reference);
    const std::string &get_reference() const;

    void set_commit_hash(const CommitHash &commit_hash);
    const CommitHash &get_commit_hash() const;
    CommitHash &get_commit_hash();

    //! Equal to comparison operator
    bool operator==(const NoteId &other) const;

    //! Not equal to comparison operator
    bool operator!=(const NoteId &other) const;

private:
    std::string m_reference;
    CommitHash m_commit_hash;
};

} // namespace esys::repo::git

namespace std
{
ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::NoteId &note_id); //<swig_out/>
}
