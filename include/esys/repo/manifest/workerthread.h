/*!
 * \file esys/repo/manifest/workerthread.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <thread>

namespace esys
{

namespace repo
{

namespace manifest
{

/*! \class WorkerThread esys/repo/manifest/workerthread.h "esys/repo/manifest/workerthread.h"
 * \brief
 */
class ESYSREPO_API WorkerThread
{
public:
    explicit WorkerThread(int id);

    void set_id(int id);
    int get_id() const;

private:
    int m_id = -1;
};

} // namespace manifest

} // namespace repo

} // namespace esys
