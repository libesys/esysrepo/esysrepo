/*!
 * \file esys/repo/exe/cmdinit.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/exe/cmdinit.h"
#include "esys/repo/filesystem.h"
#include "esys/repo/git/url.h"
#include "esys/repo/manifest/file.h"
#include "esys/repo/manifest/xmlfile.h"
#include "esys/repo/grepo/manifest.h"
#include "esys/repo/manifest/directorymngr.h"

#include <esys/trace/call.h>
#include <esys/trace/macros.h>

#include <boost/filesystem.hpp>

#include <cassert>
#include <iostream>
#include <sstream>

namespace esys::repo::exe
{

CmdInit::CmdInit()
    : Cmd("Init")
{
}

CmdInit::~CmdInit() = default;

void CmdInit::set_url(const std::string &url)
{
    m_url = url;
}

const std::string &CmdInit::get_url() const
{
    return m_url;
}

void CmdInit::set_branch(const std::string &branch)
{
    m_branch = branch;
}

const std::string &CmdInit::get_branch() const
{
    return m_branch;
}

void CmdInit::set_manifest_name(const std::string &manifest_name)
{
    m_manifest_name = manifest_name;
}

const std::string &CmdInit::get_manifest_name() const
{
    return m_manifest_name;
}

void CmdInit::set_project_name(const std::string &project_name)
{
    m_project_name = project_name;
}

const std::string &CmdInit::get_project_name() const
{
    return m_project_name;
}

void CmdInit::set_google_manifest(bool google_manifest)
{
    m_google_manifest = google_manifest;
    m_manifest_type = manifest::Type::GOOGLE_MANIFEST;
}

bool CmdInit::get_google_manifest() const
{
    return m_google_manifest;
}

void CmdInit::set_git_super_project(bool git_super_project)
{
    m_git_super_project = git_super_project;
    m_manifest_type = manifest::Type::GOOGLE_MANIFEST;
}

bool CmdInit::get_git_super_project() const
{
    return m_git_super_project;
}

std::string CmdInit::get_extra_start_msg()
{
    return "\n    url : " + get_url();
}

Result CmdInit::impl_run()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    if (get_google_manifest()) warn("The option --google is not implemented yet");
    if (get_git_super_project()) warn("The option --git-super is not implemented yet");

    result = only_one_folder_or_workspace();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    result = load_esysrepo_folder();
    if (result.ok())
    {
        if (get_url().empty() && !get_project_name().empty())
        {
            info("Finding URL for project " + get_project_name() + " ...");
            result = find_url_from_project_name();
            if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));
            if (!get_url().empty()) info("Found URL : '" + get_url() + "'.");
        }
        return ETRC_RET(load_esysrepo_folder_succeeded());
    }
    return ETRC_RET(load_esysrepo_folder_failed());
}

Result CmdInit::load_esysrepo_folder_succeeded()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    assert(get_config_folder() != nullptr);
    if (get_config_folder() == nullptr)
    {
        error("Internal error [CmdInit::load_esysrepo_folder_succeeded] get_config_folder() == nullptr");
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));
    }

    auto config = get_config_folder()->get_config();
    assert(config != nullptr);
    if (config == nullptr)
    {
        error("Internal error [CmdInit::load_esysrepo_folder_succeeded] config == nullptr");
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));
    }

    git::URL url(get_url());
    if (url != config->get_manifest_url())
    {
        std::stringstream oss;

        oss << "Current manifest url is:" << std::endl;
        oss << "    '" << config->get_manifest_url() << "'" << std::endl;
        oss << "But manifest url given is different:" << std::endl;
        oss << "    '" << get_url() << "'" << std::endl;
        oss << "Use --force if this is really wanted.";
        error(oss.str());
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::CMD_INCORRECT_PARAMETERS_IN_CONTEXT, oss.str()));
    }

    if (get_branch().empty())
    {
        warn("Nothing to do as a branch is not provided");
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
    }

    boost::filesystem::path manifest_git_path = get_config_folder()->get_manifest_repo_path();

    auto git_helper = new_git_helper();

    debug(1, "Test debug, before git_helper");
    git_helper->debug(1, "Test debug with git_helper");

    result = git_helper->open(manifest_git_path.normalize().make_preferred().string(), log::DEBUG);
    if (result.error())
    {
        std::string err_str = "Opening git repo holding the manifest failed.";
        error(err_str);
        return ETRC_RET(ESYSREPO_RESULT(result, err_str));
    }

    result = get_git()->checkout(get_branch() /*, get_force()*/);
    if (result.error())
    {
        std::string err_str = "Checkout branch failed.";
        error(err_str);
        return ETRC_RET(ESYSREPO_RESULT(result, err_str));
    }

    result = get_git()->close();

    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result CmdInit::load_esysrepo_folder_failed()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    // Since it failed, either there is no .esysrepo folder or the xml config
    // file is corrupted
    result = create_esysrepo_folder();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    // Now there are 2 cases, we couldn't load be the ESysRepo config folder because
    // it got corrupted, or it's not there at all.
    result = fetch_manifest();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    return ETRC_RET(Result::OK);
}

Result CmdInit::fetch_manifest()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    if (get_google_manifest())
        return ETRC_RET(fetch_google_manifest());
    else if (get_git_super_project())
        return ETRC_RET(fetch_git_super_project());

    // Here we can't be sure, either it's a google manifest, a esysrepo manifest or a git super project
    return ETRC_RET(fetch_unknown_manifest());
}

Result CmdInit::fetch_google_manifest()
{
    return ESYSREPO_RESULT(ResultCode::NOT_IMPLEMENTED);
}

Result CmdInit::read_esysrepo_manifest(std::shared_ptr<Manifest> manifest, const std::string &filename)
{
    Result result;
    ETRC_CALL_RET_NP(result);

    manifest::File manifest_file;

    manifest_file.set_data(manifest);
    result = manifest_file.read(filename);
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result CmdInit::read_esysrepo_manifest_xml(std::shared_ptr<Manifest> manifest, const std::string &filename)
{
    Result result;
    ETRC_CALL_RET_NP(result);

    manifest::XMLFile manifest_file;

    manifest_file.set_data(manifest);
    result = manifest_file.read(filename);
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result CmdInit::read_esysrepo_manifest_json(std::shared_ptr<Manifest> manifest, const std::string &filename)
{
    //! \TODO
    return ESYSREPO_RESULT(ResultCode::NOT_IMPLEMENTED);
}

Result CmdInit::fetch_esysrepo_manifest(GitHelper &git_helper, const std::string &git_repo_path,
                                        const std::string &manifest_filename)
{
    Result result;
    ETRC_CALL_RET(result, git_repo_path, manifest_filename);

    std::string manifest_path;
    boost::filesystem::path manifest_filename_path = manifest_filename;
    std::string manifest_filename_ext = manifest_filename_path.extension().string();

    info("ESysRepo manifest detected.");
    debug(0, "Manifest filename : " + manifest_filename);

    auto config_file = get_config_folder()->get_or_new_config();

    config_file->set_manifest_type(manifest::Type::ESYSREPO_MANIFEST);
    config_file->set_manifest_url(get_url());

    boost::filesystem::path source = git_repo_path;
    boost::filesystem::path rel_source = boost::filesystem::relative(source);
    boost::filesystem::path target;
    boost::filesystem::path file_path = git_repo_path;
    file_path /= manifest_filename;

    std::shared_ptr<Manifest> manifest = std::make_shared<Manifest>();

    if (manifest_filename_ext == ".manifest")
    {
        result = read_esysrepo_manifest(manifest, file_path.string());
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));
    }
    else if (manifest_filename_ext == ".xml")
    {
        result = read_esysrepo_manifest_xml(manifest, file_path.string());
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));
        manifest->set_format(manifest::Format::XML);
        config_file->set_manifest_format(manifest::Format::XML);
    }
    else if (manifest_filename_ext == ".json")
    {
        result = read_esysrepo_manifest_json(manifest, file_path.string());
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));
        manifest->set_format(manifest::Format::JSON);
        config_file->set_manifest_format(manifest::Format::JSON);
    }
    else
    {
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::MANIFEST_FILE_EXT_UNKNOWN, manifest_filename_ext));
    }

    config_file->set_manifest_format(manifest->get_format());

    if (manifest->get_kind() == manifest::Kind::NOT_SET) manifest->set_kind(manifest::Kind::ISOLATED);

    if (manifest->get_kind() == manifest::Kind::EMBEDDED)
    {
        info("Embedded kind.");
        config_file->set_manifest_kind(manifest::Kind::EMBEDDED);
        target = get_config_folder()->get_workspace_path();
        result = git_helper.move(source.string(), target.string(), true, log::Level::DEBUG);
        if (result == ResultCode::FAILED_TO_COPY) return ETRC_RET(ESYSREPO_RESULT(result));
        if (result == ResultCode::FAILED_TO_REMOVE_ALL)
            warn("While moving folder " + rel_source.string() + " some files were left behind.");
        target = "..";
        target /= manifest_filename;
        manifest_path = target.string();
    }
    else if (manifest->get_kind() == manifest::Kind::ISOLATED)
    {
        info("Isolated kind.");
        config_file->set_manifest_kind(manifest::Kind::ISOLATED);
        target = get_config_folder()->get_path();

        target /= manifest::Base::get_folder_name();
        boost::filesystem::path rel_target;
        rel_target = boost::filesystem::relative(target);

        bool result_bool = boost::filesystem::create_directory(target);
        if (!result_bool)
        {
            error("Couldn't create the folder : " + rel_target.string());
            return ETRC_RET(ESYSREPO_RESULT(ResultCode::FOLDER_CREATION_ERROR, target.string()));
        }
        else
            debug(0, "Created folder : " + rel_target.string());

        result = git_helper.move(source.string(), target.string(), true, log::Level::DEBUG);
        if (result == ResultCode::FAILED_TO_COPY) return ETRC_RET(ESYSREPO_RESULT(result));
        if (result == ResultCode::FAILED_TO_REMOVE_ALL)
            warn("While moving folder " + rel_source.string() + " some files were left behind.");
        target = manifest::Base::get_folder_name();
        target /= manifest_filename;
        manifest_path = target.generic().string();
    }
    else
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::MANIFEST_KIND_UNKNOWN));

    config_file->set_manifest_path(manifest_path);
    result = get_config_folder()->write_config_file();
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result CmdInit::fetch_git_super_project()
{
    return ESYSREPO_RESULT(ResultCode::NOT_IMPLEMENTED);
}

Result CmdInit::fetch_unknown_manifest()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    if (get_git() == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    if (get_url().empty() && !get_project_name().empty())
    {
        info("Finding URL for project " + get_project_name() + " ...");
        result = find_url_from_project_name();
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));
        if (!get_url().empty()) info("Found URL : '" + get_url() + "'.");
    }

    if (get_url().empty())
    {
        error("URL is empty");
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::CMDINIT_NO_URL));
    }

    info("Detecting the manifest type ...");

    boost::filesystem::path path = get_config_folder()->get_temp_path();
    path /= "unknown_manifest";

    auto git_helper = new_git_helper();

    debug(1, "Test debug, before git_helper");
    git_helper->debug(1, "Test debug with git_helper");

    result = git_helper->clone(get_url(), path.normalize().make_preferred().string(), false, log::DEBUG);
    if (result.error())
    {
        error("Cloning failed.");
        return ETRC_RET(ESYSREPO_RESULT(result));
    }

    if (!get_branch().empty())
    {
        result = get_git()->checkout(get_branch() /*, get_force()*/);
        if (result.error())
        {
            std::string err_str = "Cloning failed : checkout branch failed.";
            error(err_str);
            return ETRC_RET(ESYSREPO_RESULT(result, err_str));
        }
    }

    result = git_helper->close(log::DEBUG);
    if (result.error())
    {
        std::string err_str = "Cloning failed : can't clod the git repo.";
        error(err_str);
        return ETRC_RET(ESYSREPO_RESULT(result, err_str));
    }

    boost::filesystem::path file_path = path;
    file_path /= ".esysrepo.manifest";

    if (boost::filesystem::exists(file_path))
    {
        result = fetch_esysrepo_manifest(*git_helper.get(), path.string(), ".esysrepo.manifest");
        return ETRC_RET(ESYSREPO_RESULT(result));
    }

    file_path = path;
    file_path /= ".esysrepo.manifest.xml";

    if (boost::filesystem::exists(file_path))
    {
        result = fetch_esysrepo_manifest(*git_helper.get(), path.string(), ".esysrepo.manifest.xml");
        return ETRC_RET(ESYSREPO_RESULT(result));
    }

    file_path = path;
    file_path /= ".esysrepo.manifest.json";

    if (boost::filesystem::exists(file_path))
    {
        result = fetch_esysrepo_manifest(*git_helper.get(), path.string(), ".esysrepo.manifest.json");
        return ETRC_RET(ESYSREPO_RESULT(result));
    }
    file_path = path;
    if (!get_manifest_name().empty())
        file_path /= get_manifest_name();
    else
        file_path /= "default.xml";

    if (boost::filesystem::exists(file_path))
    {
        info("Google repo tool manifest detected.");
        auto config = get_config_folder()->get_or_new_config();
        config->set_manifest_type(manifest::Type::GOOGLE_MANIFEST);
        config->set_manifest_format(manifest::Format::XML);
        config->set_manifest_url(get_url());

        boost::filesystem::path source = path.string();
        boost::filesystem::path rel_source = boost::filesystem::relative(source);
        boost::filesystem::path target = get_config_folder()->get_path();
        boost::filesystem::path rel_target = boost::filesystem::relative(target);
        target /= grepo::Manifest::get_folder_name();
        rel_target = boost::filesystem::relative(target);

        bool result_bool = boost::filesystem::create_directory(target);
        if (!result_bool)
        {
            std::string err_str = "Couldn't create the folder : " + rel_target.string();
            error(err_str);
            return ETRC_RET(ESYSREPO_RESULT(ResultCode::FOLDER_CREATION_ERROR, err_str));
        }
        else
            debug(0, "Created folder : " + rel_target.string());

        result = git_helper->move(source.string(), target.string(), true, log::Level::DEBUG);
        if (result == ResultCode::FAILED_TO_COPY) return ETRC_RET(ESYSREPO_RESULT(result));
        if (result == ResultCode::FAILED_TO_REMOVE_ALL)
            warn("While moving folder " + rel_source.string() + " some files were left behind.");

        std::string manifest_path = "grepo/";
        if (!get_manifest_name().empty())
            manifest_path += get_manifest_name();
        else
            manifest_path += "default.xml";

        get_config_folder()->get_config()->set_manifest_path(manifest_path);
        return get_config_folder()->write_config_file();
    }

    file_path = path;
    file_path /= ".gitmodules";

    if (boost::filesystem::exists(file_path))
    {
        info("Git submodule detected.");
        auto config = get_config_folder()->get_or_new_config();

        config->set_manifest_type(manifest::Type::GIT_SUPER_PROJECT);
        config->set_manifest_url(get_url());
        boost::filesystem::path source = path.string();
        boost::filesystem::path target = get_config_folder()->get_workspace_path();

        result = git_helper->move(source.string(), target.string(), true, log::Level::DEBUG);
        if (result == ResultCode::FAILED_TO_COPY) return ETRC_RET(ESYSREPO_RESULT(result));
        if (result == ResultCode::FAILED_TO_REMOVE_ALL)
            warn("While moving folder " + source.string() + " some files were left behind.");
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
    }

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::CMDINIT_FAILED_FETCH_UNKNOWN_MANIFEST));
}

Result CmdInit::load_esysrepo_folder()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    auto config_folder = std::make_shared<ConfigFolder>();
    config_folder->set_logger_if(get_logger_if());

    set_config_folder(config_folder);

    ETRC_MSG("parent_path = " + get_workspace_path());

    result = config_folder->open(get_workspace_path(), false);
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result CmdInit::create_esysrepo_folder()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    auto config_folder = get_config_folder();
    if (config_folder == nullptr)
    {
        config_folder = std::make_shared<ConfigFolder>();
        config_folder->set_logger_if(get_logger_if());

        set_config_folder(config_folder);
    }

    ETRC_MSG("parent_path = " + get_workspace_path());

    result = config_folder->create(get_workspace_path(), true);
    return ESYSREPO_RESULT(result);
}

Result CmdInit::find_url_from_project_name()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    manifest::DirectoryMngr mngr;

    boost::filesystem::path path = get_config_folder()->get_temp_path();
    path /= "manifest_directories";
    mngr.set_folder_path(path.string());

    if (get_directories() == nullptr)
    {
        auto directories = std::make_shared<manifest::Directories>();
        directories->add_default_libesys();

        mngr.set_directories(directories);
        set_directories(directories);
    }

    result = mngr.load();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    auto project = get_directories()->find_project(get_project_name());
    if (project == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GENERIC_ERROR));

    set_url(project->get_url_ssh());
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

void CmdInit::set_directories(std::shared_ptr<manifest::Directories> directories)
{
    m_directories = directories;
}

std::shared_ptr<manifest::Directories> CmdInit::get_directories() const
{
    return m_directories;
}

} // namespace esys::repo::exe
