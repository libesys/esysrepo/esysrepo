/*!
 * \file esys/repo/manifest/cmake/config_defs.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#define ESYSREPO_USE_BOOST_FILESYSTEM_NO_RECURSIVE 1
#define ESYSREPO_HAS_STD_FILESYSTEM 0
