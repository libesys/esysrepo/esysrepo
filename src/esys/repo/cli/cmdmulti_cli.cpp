/*!
 * \file esys/repo/cli/cmdmulti_cli.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/cli/cmdmulti.h"

namespace esys::repo::cli
{

CmdMulti::CmdMulti(AppBase *app)
    : BaseType(app, "multi", "Handle branches from multiple repositories")
{
}

CmdMulti::~CmdMulti() = default;

int CmdMulti::configure_cmd(CmdType &cmd)
{
    if (get_vm().count("capture"))
        cmd.set_task(CmdType::Task::CAPTURE);
    else if (get_vm().count("show"))
        cmd.set_task(CmdType::Task::SHOW);
    else if (get_vm().count("delete"))
        cmd.set_task(CmdType::Task::DELETE);
    else if (get_vm().count("push"))
        cmd.set_task(CmdType::Task::PUSH);

    return 0;
}

std::shared_ptr<po::options_description> CmdMulti::get_desc()
{
    auto desc = Cmd::get_desc();
    if (desc != nullptr) return desc;

    desc = std::make_shared<po::options_description>("Multi options");
    // clang-format off
    desc->add_options()
        ("capture,c", "capture the branch different from manifest for all repositories")
        ("show,s", "show the captured branches")
        ("delete,d", "delete the captured branches")
        ("push,p", "push the captured branches")
        ;
    // clang-format on
    Cmd::set_desc(desc);
    return desc;
}

int CmdMulti::print_doc(std::ostream &os)
{
    return -1;
}

} // namespace esys::repo::cli
