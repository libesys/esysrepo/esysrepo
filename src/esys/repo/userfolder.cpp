/*!
 * \file esys/repo/userfolder.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/userfolder.h"
#include "esys/repo/userconfigfile.h"

#ifdef WIN32
#include <shlobj.h>
#endif

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>

namespace esys::repo
{

UserFolder::UserFolder() = default;

UserFolder::~UserFolder() = default;

void UserFolder::set_path(const std::string &path)
{
    m_path = path;
}

const std::string &UserFolder::get_path() const
{
    return m_path;
}

void UserFolder::set_user_home_path(const std::string &user_home_path)
{
    m_user_home_path = user_home_path;
}

const std::string &UserFolder::get_user_home_path() const
{
    return m_user_home_path;
}

void UserFolder::set_user_config_file_path(const std::string &user_config_file_path)
{
    m_user_config_file_path = user_config_file_path;
}

const std::string &UserFolder::get_user_config_file_path() const
{
    return m_user_config_file_path;
}

void UserFolder::set_user_config(std::shared_ptr<UserConfig> user_config)
{
    m_user_config = user_config;
}

std::shared_ptr<UserConfig> UserFolder::get_user_config() const
{
    return m_user_config;
}

Result UserFolder::create_folder(const std::string &folder_path, bool ok_if_exists)
{
    if (!boost::filesystem::exists(folder_path))
    {
        bool result = boost::filesystem::create_directory(folder_path);
        if (!result)
        {
            error("The following folder couldn't be created : " + folder_path);
            return ESYSREPO_RESULT(ResultCode::FOLDER_CREATION_ERROR, folder_path);
        }
        else
            info("Created folder : " + folder_path);
    }
    else
    {
        if (!ok_if_exists)
        {
            error("The following folder already exits : " + folder_path);
            return ESYSREPO_RESULT(ResultCode::FOLDER_ALREADY_EXISTS, folder_path);
        }
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result UserFolder::create(bool ok_if_exists)
{
    auto result = populate_all_pathes();
    if (result.error()) return ESYSREPO_RESULT(result);

    if (get_user_config() == nullptr) ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    result = create_folder(get_path(), ok_if_exists);
    if (result.error()) return ESYSREPO_RESULT(result);

    result = create_folder(get_user_config()->get_temp_path(), ok_if_exists);
    if (result.error()) return ESYSREPO_RESULT(result);

    result = create_folder(get_user_config()->get_manifest_directories_path(), ok_if_exists);
    if (result.error()) return ESYSREPO_RESULT(result);

    result = create_folder(get_user_config()->get_traces_path(), ok_if_exists);
    if (result.error()) return ESYSREPO_RESULT(result);

    UserConfigFile user_cfg_file;
    user_cfg_file.set_config(get_user_config());
    result = user_cfg_file.write(get_user_config_file_path());
    if (result.error()) return ESYSREPO_RESULT(result);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result UserFolder::read(const std::string &path, bool print_error)
{
    boost::filesystem::path esysrepo_path = path;

    if (path.empty())
    {
        auto result = populate_all_pathes();
        if (result.error()) return ESYSREPO_RESULT(result);
    }
    else
    {
        if (!boost::filesystem::exists(path)) return ESYSREPO_RESULT(ResultCode::FILE_NOT_EXISTING, path);

        if (boost::filesystem::is_regular_file(esysrepo_path))
            esysrepo_path = esysrepo_path.parent_path();
        else if (boost::filesystem::is_directory(esysrepo_path))
        {
            if (boost::filesystem::exists(esysrepo_path / ".esysrepo"))
                esysrepo_path /= ".esysrepo";
            else if (esysrepo_path.filename() != ".esysrepo")
                return ESYSREPO_RESULT(ResultCode::FILE_NOT_EXISTING);
        }
        else
            return ESYSREPO_RESULT(ResultCode::GENERIC_ERROR);

        if (esysrepo_path.filename() == ".esysrepo")
            set_user_home_path(esysrepo_path.parent_path().string());
        else
            return ESYSREPO_RESULT(ResultCode::FILE_NOT_EXISTING);

        auto result = populate_all_pathes();
        if (result.error()) return ESYSREPO_RESULT(result);
    }

    UserConfigFile user_cfg_file;
    user_cfg_file.set_config(get_user_config());
    auto result = user_cfg_file.read(get_user_config_file_path());
    if (result.error()) return ESYSREPO_RESULT(result);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result UserFolder::write(const std::string &path)
{
    auto result = populate_all_pathes();
    if (result.error()) return ESYSREPO_RESULT(result);

    if (get_user_config() == nullptr) ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    result = create_folder(get_path(), true);
    if (result.error()) return ESYSREPO_RESULT(result);

    if (boost::filesystem::exists(get_user_config_file_path()))
    {
        boost::filesystem::remove(get_user_config_file_path());
    }

    UserConfigFile user_cfg_file;
    user_cfg_file.set_config(get_user_config());
    result = user_cfg_file.write(get_user_config_file_path());
    if (result.error()) return ESYSREPO_RESULT(result);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result UserFolder::populate_all_pathes()
{
    if (get_user_home_path().empty())
    {
        auto result = detect_home_folder();
        if (result.error()) return ESYSREPO_RESULT(result);
    }
    boost::filesystem::path base_path = get_user_home_path();
    boost::filesystem::path user_folder_path = base_path / ".esysrepo";
    boost::filesystem::path temp_path = user_folder_path / "temp";
    boost::filesystem::path user_config_file_path = user_folder_path / "user_config.xml";
    boost::filesystem::path manifest_directories_path = user_folder_path / "manifest_directories";
    boost::filesystem::path manifest_directories_file_path = user_folder_path / "manifest_directories.xml";
    boost::filesystem::path traces_path = user_folder_path / "traces";

    set_path(user_folder_path.string());
    set_user_config_file_path(user_config_file_path.string());

    if (get_user_config() == nullptr) set_user_config(std::make_shared<UserConfig>());

    get_user_config()->set_temp_path(temp_path.string());
    get_user_config()->set_manifest_directories_path(manifest_directories_path.string());
    get_user_config()->set_traces_path(traces_path.string());

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result UserFolder::detect_home_folder()
{
#ifdef WIN32
    PWSTR path = nullptr;
    auto hresult = SHGetKnownFolderPath(FOLDERID_Profile, 0, nullptr, &path);
    if (SUCCEEDED(hresult))
    {
        auto char_path = boost::locale::conv::utf_to_utf<char>(path);
        set_user_home_path(char_path);
        CoTaskMemFree(path);
        return ESYSREPO_RESULT(ResultCode::OK);
    }
    else
    {
        CoTaskMemFree(path);
        return ESYSREPO_RESULT(ResultCode::GENERIC_ERROR);
    }
#else
    auto path = std::getenv("XDG_CONFIG_HOME");
    if (path != nullptr)
    {
        set_user_home_path(path);
        return ESYSREPO_RESULT(ResultCode::OK);
    }
    path = std::getenv("HOME");
    if (path != nullptr)
    {
        set_user_home_path(path);
        return ESYSREPO_RESULT(ResultCode::OK);
    }
    return ESYSREPO_RESULT(ResultCode::GENERIC_ERROR);
#endif
}

} // namespace esys::repo
