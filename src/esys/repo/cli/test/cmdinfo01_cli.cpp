/*!
 * \file esys/repo/cli/test/cmdinfo01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/cli/cmdinfo.h>

namespace esys
{

namespace repo
{

namespace cli
{

namespace test
{

/*! \class CmdInfo01Cli esys/repo/cli/test/cmdinfo01_cli.cpp
 * "esys/repo/cli/test/cmdinfo01_cli.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(CmdInfo01Cli)
{
    CmdInfo cmd_info;
    std::vector<std::string> args;
    int result = 0;

    args = {"info"};
    cmd_info.set_args(args);
    result = cmd_info.parse_and_configure();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace test

} // namespace cli

} // namespace repo

} // namespace esys
