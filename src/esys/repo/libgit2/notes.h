/*!
 * \file esys/repo/libgit2/notes.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/result.h"
#include "esys/repo/git/note.h"

#include <git2.h>

#include <map>

namespace esys::repo::libgit2
{

class ESYSREPO_API Git;
class ESYSREPO_API GitImpl;

struct ESYSREPO_API Comparegit_oid
{
    bool operator()(const git_oid &lhs, const git_oid &rhs) const;
};

/*! \class Notes esys/repo/libgit2/notes.h "esys/repo/libgit2/notes.h"
 * \brief Handle all the notes of a git repository
 */
class ESYSREPO_API Notes
{
public:
    //! Default constructor
    /*!
     * \param[in] git the git instance used
     */
    explicit Notes(Git *git);

    //! Get the git instance used
    /*!
     * \return the git instance used
     */
    Git *get_git();

    //! Get the git implementation instance used
    /*!
     * \return the git implementation instance used
     */
    GitImpl *get_git_impl();

    //! Load all notes references added to the git instance
    /*!
     * \return 0 if successful, < 0 otherwise
     */
    Result load_all();

    //! Load all notes references added to the git instance
    /*!
     * \param[in] ref the notes reference to load
     * \return 0 if successful, < 0 otherwise
     */
    Result load(const std::string &ref);

    Result get_note(const git_oid &note_oid, std::vector<std::shared_ptr<git::Note>> &notes) const;

    //! Tells if the notes have been loaded already
    /*!
     * \return true if the notes where already loaded, false otherwise
     */
    bool is_loaded() const;

    //! Set if all notes have been loaded already
    /*!
     * \param[in] loaded true all notes have been loaded already, false otherwise
     */
    void set_loaded(bool loaded);

    void clear();
private:
    //!< \cond DOXY_IMPL

    struct RefMapNotesItem;

    void add_note(std::shared_ptr<RefMapNotesItem> map, const git_oid &note_oid, const git_oid &commit_oid);

    Git *m_git = nullptr;  //!< The git instance to be used
    bool m_loaded = false; //!< Tells if the notes have been loaded already

    struct OIDCommitMapNoteItem
    {
        OIDCommitMapNoteItem();

        OIDCommitMapNoteItem(Notes *notes, const git_oid &note_oid, const git_oid &comment_oid,
                             RefMapNotesItem *ref_map);

        std::shared_ptr<git::Note> get_note();

        git_oid m_note_oid;
        git_oid m_comment_oid;
        std::shared_ptr<git::Note> m_note;
        RefMapNotesItem *m_ref_map = nullptr;
        Notes *m_notes = nullptr;
        std::string m_ref;
    };

    using OIDCommitMapNoteVec = std::vector<OIDCommitMapNoteItem>;

    struct RefMapNotesItem
    {
        std::string m_ref;
        std::map<git_oid, std::shared_ptr<OIDCommitMapNoteVec>, Comparegit_oid> m_iod_commit_map_note;
    };

    //! Map from the notes reference to
    std::map<std::string, std::shared_ptr<RefMapNotesItem>, std::less<>> m_ref_map_notes;

    //! Map from the commit oid to the vector of Notes attached to this commit
    std::map<git_oid, std::shared_ptr<OIDCommitMapNoteVec>, Comparegit_oid> m_oid_map_notes;
    //!< \endcond
};

} // namespace esys::repo::libgit2
