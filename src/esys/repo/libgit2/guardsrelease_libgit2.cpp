/*!
 * \file esys/repo/libgit2/guardsrelease_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/libgit2/guardsrelease.h"

namespace esys::repo::libgit2
{

template<>
ESYSREPO_API void guards_release<git_strarray>(git_strarray *data)
{
    if (data == nullptr) return;
    git_strarray_dispose(data);
}

} // namespace esys::repo::libgit2
