/*!
 * \file esys/repo/git/person_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/person.h"

namespace esys::repo::git
{

Person::Person() = default;

Person::~Person() = default;

void Person::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &Person::get_name() const
{
    return m_name;
}

void Person::set_email(const std::string &email)
{
    m_email = email;
}

const std::string &Person::get_email() const
{
    return m_email;
}

bool Person::operator==(const Person &other) const
{
    if (get_name() != other.get_name()) return false;
    if (get_email() != other.get_email()) return false;
    return true;
}


bool Person::operator!=(const Person &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git
