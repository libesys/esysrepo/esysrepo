/*!
 * \file esys/repo/userconfig.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/userconfig.h"

namespace esys::repo
{

UserConfig::UserConfig() = default;

UserConfig::~UserConfig() = default;

void UserConfig::set_temp_path(const std::string &temp_path)
{
    m_temp_path = temp_path;
}

const std::string &UserConfig::get_temp_path() const
{
    return m_temp_path;
}

void UserConfig::set_manifest_directories_path(const std::string &manifest_directories_path)
{
    m_manifest_directories_path = manifest_directories_path;
}

const std::string &UserConfig::get_manifest_directories_path() const
{
    return m_manifest_directories_path;
}

void UserConfig::set_manifest_directories(std::shared_ptr<manifest::Directories> manifest_directories)
{
    m_manifest_directories = manifest_directories;
}

std::shared_ptr<manifest::Directories> UserConfig::get_manifest_directories() const
{
    return m_manifest_directories;
}

void UserConfig::set_traces_path(const std::string &traces_path)
{
    m_traces_path = traces_path;
}

const std::string &UserConfig::get_traces_path() const
{
    return m_traces_path;
}

void UserConfig::clear()
{
    set_manifest_directories_path("");
    set_temp_path("");

    if (get_manifest_directories() != nullptr) get_manifest_directories()->clear();
}

bool UserConfig::operator==(const UserConfig &cfg) const
{
    if (get_manifest_directories_path() != cfg.get_manifest_directories_path()) return false;
    if (get_temp_path() != cfg.get_temp_path()) return false;
    if ((get_manifest_directories() == nullptr) && (cfg.get_manifest_directories() == nullptr)) return true;
    if ((get_manifest_directories() != nullptr) && (cfg.get_manifest_directories() != nullptr))
    {
        if (*get_manifest_directories() != *cfg.get_manifest_directories()) return false;
    }
    return true;
}

bool UserConfig::operator!=(const UserConfig &cfg) const
{
    return !operator==(cfg);
}

} // namespace esys::repo
