/*!
 * \file esys/repo/git/branch.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/branchtype.h"

#include <string>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class Branch esys/repo/git/branch.h "esys/repo/git/branch.h"
 * \brief Hold information about a git branch
 */
class ESYSREPO_API Branch
{
public:
    //! Default constructor
    Branch();

    //! Constructor
    /*!
     * \param[in] name the name of the branch
     * \param[in] is_head true if this is current branch, false otherwise
     */
    Branch(const std::string &name, bool is_head);

    //! Constructor
    /*!
     * \param[in] name the name of the branch
     * \param[in] type the type of the branch, e.g. local or remote
     * \param[in] is_head true if this is current branch, false otherwise
     */
    Branch(const std::string &name, BranchType type, bool is_head);

    //! Destructor
    virtual ~Branch();

    //! Set the name of the branch
    /*!
     * \param[in] name the name of the branch
     */
    void set_name(const std::string &name);

    //! Get the name of the branch
    /*!
     * \return the name of the branch
     */
    const std::string &get_name() const;

    //! Set the reference name of the branch
    /*!
     * \param[in] ref_name the reference name of the branch
     */
    void set_ref_name(const std::string &ref_name);

    //! Get the reference name of the branch
    /*!
     * \return the reference name of the branch
     */
    const std::string &get_ref_name() const;

    //! Set the type of the branch
    /*!
     * \param[in] type the type of the branch
     */
    void set_type(BranchType type);

    //! Get the type of the branch
    /*!
     * \return the type of the branch
     */
    BranchType get_type() const;

    //! Set if the branch is the default branch
    /*!
     * \param[in] is_head true if this branch is the head/default branch
     */
    void set_is_head(bool is_head);

    //! Get if the branch is the default branch
    /*!
     * \return true if this branch is the head/default branch
     */
    bool get_is_head() const;

    void set_remote_branch(const std::string &remote_branch);

    const std::string &get_remote_branch() const;

    void set_remote_branch_name(const std::string &remote_branch_name);

    const std::string &get_remote_branch_name() const;

    void set_remote_name(const std::string &remote_name);

    const std::string &get_remote_name() const;

    void set_detached(bool detached);

    bool get_detached() const;

    //! Equal to comparison operator
    bool operator==(const Branch &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Branch &other) const;

private:
    //!< \cond DOXY_IMPL
    std::string m_name;                      //!< The name of the branch
    std::string m_ref_name;                  //!< The reference name of the branch
    BranchType m_type = BranchType::NOT_SET; //!< The type of branch
    bool m_is_head = false;                  //!< True if default branch
    std::string m_remote_branch;             //!< The remote-tracking branch
    std::string m_remote_name;               //!< The name of the remote
    std::string m_remote_branch_name;        //!< The remote branch name
    bool m_detached = false;                 //!< Branch is detached or not
    //!< \endcond
};

} // namespace esys::repo::git

namespace std
{
ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::Branch &branch); //<swig_out/>
}
