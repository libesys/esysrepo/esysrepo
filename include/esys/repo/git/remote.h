/*!
 * \file esys/repo/git/remote.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <ostream>
#include <string>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class Remote esys/repo/git/remote.h "esys/repo/git/remote.h"
 * \brief Hold information about a git branch
 */
class ESYSREPO_API Remote
{
public:
    //! Default constructor
    Remote();

    //! Destructor
    virtual ~Remote();

    //! Set the name of the remote
    /*!
     * \param[in] name the name of the remote
     */
    void set_name(const std::string &name);

    //! Get the name of the remote
    /*!
     * \return the name of the remote
     */
    const std::string &get_name() const;

    //! Set the url of the remote
    /*!
     * \param[in] url the url of the remote
     */
    void set_url(const std::string &url);

    //! Get the url of the remote
    /*!
     * \return the url of the remote
     */
    const std::string &get_url() const;

    //! Equal to comparison operator
    bool operator==(const Remote &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Remote &other) const;

private:
    //!< \cond DOXY_IMPL
    std::string m_name; //!< The name of the remote
    std::string m_url;  //!< The url of the remote
    //!< \endcond
};

} // namespace esys::repo::git

namespace std
{
ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::Remote &remote); //<swig_out/>
}
