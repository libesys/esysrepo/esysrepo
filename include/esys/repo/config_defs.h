/*!
 * \file esys/repo/config_defs.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef _MSC_VER
#include "esys/repo/msvc/config_defs.h"
#else
#include "esys/repo/cmake/config_defs.h"
#endif
