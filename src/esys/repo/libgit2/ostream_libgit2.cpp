/*!
 * \file esys/repo/libgit2/ostream_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/libgit2/ostream.h"

#include <string>

namespace std
{

void print_helper(std::ostream &os, const std::string &name, const char *value)
{
    os << name << " : ";
    if (value != nullptr)
        os << value;
    else
        os << "<nullptr>";
    os << std::endl;
}

ESYSREPO_API ostream &operator<<(ostream &os, git_remote *data)
{
    if (data == nullptr)
    {
        os << "<nullptr>";
        return os;
    }

    print_helper(os, "name", git_remote_name(data));
    print_helper(os, "url", git_remote_url(data));

    return os;
}

} // namespace std
