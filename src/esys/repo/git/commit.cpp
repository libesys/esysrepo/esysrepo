/*!
 * \file esys/repo/git/commit.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/commit.h"

#include <date/date.h>

namespace esys::repo::git
{

Commit::Commit() = default;

Commit::~Commit() = default;

void Commit::clear()
{
    get_hash().set_hash("");
    set_message("");
    set_summary("");
    set_body("");
    set_author("");
    set_email("");
    m_all_notes.clear();
    m_map_ref_notes.clear();
}

void Commit::set_hash(const std::string &hash)
{
    get_hash().set_hash(hash);
}

const CommitHash &Commit::get_hash() const
{
    return m_hash;
}

CommitHash &Commit::get_hash()
{
    return m_hash;
}

void Commit::set_message(const std::string &message)
{
    m_message = message;
}

const std::string &Commit::get_message() const
{
    return m_message;
}

void Commit::set_summary(const std::string &summary)
{
    m_summary = summary;
}

const std::string &Commit::get_summary() const
{
    return m_summary;
}

void Commit::set_body(const std::string &body)
{
    m_body = body;
}

const std::string &Commit::get_body() const
{
    return m_body;
}

void Commit::set_author(const std::string &author)
{
    m_author_sign.set_name(author);
}

const std::string &Commit::get_author() const
{
    return m_author_sign.get_name();
}

void Commit::set_email(const std::string &email)
{
    m_author_sign.set_email(email);
}

const std::string &Commit::get_email() const
{
    return m_author_sign.get_email();
}

void Commit::set_date_time(const std::chrono::system_clock::time_point &date_time)
{
    m_author_sign.set_date_time(date_time);
}

const std::chrono::system_clock::time_point &Commit::get_date_time() const
{
    return m_author_sign.get_date_time();
}

std::vector<std::shared_ptr<Note>> &Commit::get_all_notes()
{
    return m_all_notes;
}

const std::vector<std::shared_ptr<Note>> &Commit::get_all_notes() const
{
    return m_all_notes;
}

int Commit::get_notes(const std::string &reference, std::vector<std::shared_ptr<Note>> &notes) const
{
    notes.clear();

    auto it = m_map_ref_notes.find(reference);
    if (it == m_map_ref_notes.end()) return 0;

    notes = it->second;
    return 0;
}

void Commit::add_note(std::shared_ptr<Note> note)
{
    m_all_notes.push_back(note);
    if (note->get_reference().empty()) return;

    auto it = m_map_ref_notes.find(note->get_reference());
    if (it != m_map_ref_notes.end())
    {
        it->second.push_back(note);
        return;
    }

    NoteVec vec;
    vec.push_back(note);
    m_map_ref_notes[note->get_reference()] = vec;
}

Signature &Commit::get_author_sign()
{
    return m_author_sign;
}

const Signature &Commit::get_author_sign() const
{
    return m_author_sign;
}

Signature &Commit::get_committer_sign()
{
    return m_committer_sign;
}

const Signature &Commit::get_committer_sign() const
{
    return m_committer_sign;
}

bool Commit::operator==(const Commit &other) const
{
    if (get_hash() != other.get_hash()) return false;
    if (get_message() != other.get_message()) return false;
    if (get_summary() != other.get_summary()) return false;
    if (get_body() != other.get_body()) return false;
    if (get_author() != other.get_author()) return false;
    if (get_email() != other.get_email()) return false;
    if (get_date_time() != other.get_date_time()) return false;
    if (get_all_notes().size() != other.get_all_notes().size()) return false;

    for (auto idx = 0; idx < get_all_notes().size(); ++idx)
    {
        if ((get_all_notes()[idx] == nullptr) && (other.get_all_notes()[idx] == nullptr))
            continue;
        else if ((get_all_notes()[idx] == nullptr) || (other.get_all_notes()[idx] == nullptr))
            return false;
        if (*get_all_notes()[idx] != *other.get_all_notes()[idx]) return false;
    }
    return true;
}

bool Commit::operator!=(const Commit &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::Commit &commit)
{
    os << "hash   : " << commit.get_hash() << std::endl;
    os << "author : " << commit.get_author() << std::endl;
    os << "date   : " << date::format("%D %T %Z", commit.get_date_time()) << std::endl;
    os << "message: " << commit.get_message() << std::endl;
    os << "body   : " << commit.get_body();
    return os;
}

} // namespace std
