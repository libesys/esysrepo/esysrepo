/*!
 * \file esys/repo/git/aheadbehind_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/aheadbehind.h"

namespace esys::repo::git
{

AheadBehind::AheadBehind() = default;

void AheadBehind::set_ahead(std::size_t ahead)
{
    m_ahead = ahead;
}

std::size_t AheadBehind::get_ahead() const
{
    return m_ahead;
}

void AheadBehind::set_behind(std::size_t behind)
{
    m_behind = behind;
}

std::size_t AheadBehind::get_behind() const
{
    return m_behind;
}

bool AheadBehind::operator==(const AheadBehind &other) const
{
    if (get_ahead() != other.get_ahead()) return false;
    if (get_behind() != other.get_behind()) return false;
    return true;
}

bool AheadBehind::operator!=(const AheadBehind &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::AheadBehind &ahead_behind)
{
    os << "ahead  : " << ahead_behind.get_ahead() << std::endl;
    os << "behind : " << ahead_behind.get_behind();
    return os;
}

} // namespace std
