/*!
 * \file esys/repo/version.cpp
 * \brief Version info for esystest
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/version.h"

#include <string.h>
#include <stdlib.h>

namespace esys::repo
{

ESYSREPO_API int get_major_version()
{
    return get_int_till_dot(ESYSREPO_VERSION_NUM_DOT_STRING);
}

ESYSREPO_API int get_minor_version()
{
    return get_int_till_dot(ESYSREPO_VERSION_NUM_DOT_STRING, 1);
}

ESYSREPO_API int get_patch_version()
{
    return get_int_till_dot(ESYSREPO_VERSION_NUM_DOT_STRING, 2, true);
}

ESYSREPO_API bool check_at_least_version(int major, int minor, int patch)
{
    return check_at_least_version(ESYSREPO_VERSION_NUM_DOT_STRING, major, minor, patch);
}

ESYSREPO_API bool check_at_least_version(const char *version, int major, int minor, int patch)
{
    int major_test = get_int_till_dot(version);
    bool minor_till_end = (patch == -1);
    int minor_test = get_int_till_dot(version, 1, minor_till_end);
    int patch_test = get_int_till_dot(version, 2, true);

    if (major < 0) return false;
    if (major_test < major) return false;
    if (major_test > major) return true;

    if (minor < 0) return false;
    if (minor_test < minor) return false;
    if (minor_test > minor) return true;

    if ((patch < 0) && (patch_test < 0)) return true;
    if (patch_test >= patch) return true;

    return false;
}

ESYSREPO_API int get_int_till_dot(const char *str, int skip_dot, bool till_end_str)
{
    int idx = 0;
    int value = 0;

    while (skip_dot != 0)
    {
        while ((idx < strlen(str)) && (str[idx] != '.')) ++idx;
        if (str[idx] != '.') return -1;
        ++idx;
        --skip_dot;
    }

    while (idx < strlen(str))
    {
        if (idx > 0) value *= 10;

        if ((str[idx] < '0') || (str[idx] > '9')) return -1;
        value += str[idx] - '0';

        ++idx;

        if (str[idx] == '.')
        {
            return value;
        }
    }
    if (till_end_str) return value;
    return -1;
}

} // namespace esys::repo
