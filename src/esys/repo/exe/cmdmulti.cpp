/*!
 * \file esys/repo/exe/cmdmulti.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/exe/cmdmulti.h"
#include "esys/repo/manifest/capture.h"
#include "esys/repo/manifest/multifilexml.h"
#include "esys/repo/manifest/repository.h"
#include "esys/repo/workspace.h"

#include <boost/filesystem.hpp>

#include <iomanip>
#include <sstream>

namespace esys::repo::exe
{

CmdMulti::CmdMulti()
    : Cmd("Multi")
{
}

CmdMulti::~CmdMulti() = default;

void CmdMulti::set_task(Task task)
{
    m_task = task;
}

CmdMulti::Task CmdMulti::get_task() const
{
    return m_task;
}

Result CmdMulti::impl_run()
{
    switch (get_task())
    {
        case Task::CAPTURE: return do_task_capture();
        case Task::DELETE: return do_task_delete();
        case Task::PUSH: return do_task_push();
        case Task::SHOW: return do_task_show();
        default: return ResultCode::GENERIC_ERROR;
    }
}

Result CmdMulti::do_task_capture()
{
    Workspace workspace;
    std::string folder_path;

    if (!get_folder().empty() && get_workspace_path().empty())
        folder_path = get_folder();
    else if (!get_workspace_path().empty())
        folder_path = get_workspace_path();
    else
        folder_path = boost::filesystem::current_path().string();

    Result result = workspace.load(folder_path);
    if (result.error()) return ESYSREPO_RESULT(result);

    set_workspace_path(workspace.get_path());

    Result rresult;
    git::Branches branches;
    git::AheadBehind ahead_behind;
    auto git_helper = new_git_helper();
    std::ostringstream info_oss;
    auto capture = std::make_shared<manifest::Capture>();

    for (auto location : workspace.get_manifest()->get_locations())
    {
        for (auto repo : location->get_repos())
        {
            info_oss.str("");
            info_oss << "repo '" << repo->get_path() << "'" << std::endl;

            std::string repo_full_path = workspace.get_full_path(repo);
            result = git_helper->open(repo_full_path, log::Level::DEBUG);
            GitHelper::AutoClose auto_close(git_helper.get());
            if (result.error())
            {
                info_oss << "    failed to open the git repository";
                error(info_oss.str());

                rresult.add(ESYSREPO_RESULT(ResultCode::GIT_ERROR_OPENING_REPO, repo_full_path));
                continue;
            }

            git::Branch head_branch;
            git::Remote head_remote;
            result = git_helper->get_git()->get_head_branch_remote(head_branch, head_remote);
            if (result.error())
            {
                info_oss << "    couldn't get the information about the HEAD";
                error(info_oss.str());

                rresult.add(ESYSREPO_RESULT(result));
                continue;
            }

            if ((repo->get_location_str() != head_remote.get_name()) || (repo->get_url() != head_remote.get_url()))
            {
                auto item = std::make_shared<manifest::Capture::Item>();

                item->set_path(repo->get_path());
                item->set_remote_name(head_remote.get_name());
                item->set_remote_url(head_remote.get_url());
                item->set_revision(head_branch.get_name());
                capture->add_item(item);
                continue;
            }

            auto revision = workspace.get_manifest()->get_repo_revision(repo);
            result = git_helper->get_git()->get_ahead_behind(ahead_behind, head_branch.get_name(), revision);
            if (result.error())
            {
                info_oss << "    failed to get the number of commits ahead or behind between local branch and branch "
                            "in manifest";
                error(info_oss.str());

                rresult.add(ESYSREPO_RESULT(result, repo->get_path()));
                continue;
            }

            if ((ahead_behind.get_ahead() == 0) && (ahead_behind.get_behind() == 0))
            {
                info_oss << "    HEAD is at the same position as the revision in the manifest";
                info(info_oss.str());
                continue;
            }

            // Check if the local branch is ahead or behind of the upstream
            result = git_helper->get_git()->get_ahead_behind(ahead_behind, head_branch);
            if (result.error())
            {
                info_oss
                    << "    failed to get the number of commits ahead or behind between local and upstream branches";
                error(info_oss.str());
                rresult.add(ESYSREPO_RESULT(result, repo_full_path));
                continue;
            }

            if (ahead_behind.get_ahead() != 0)
            {
                std::ostringstream oss;

                info_oss << "    local branch is " << ahead_behind.get_ahead()
                         << " commits ahead of the remote. Push your changes.";
                error(info_oss.str());
                continue;
            }

            capture->add_item(repo->get_path(), head_branch.get_name());
        }
    }

    if (rresult.error()) return ESYSREPO_RESULT(rresult);

    if (capture->get_items().empty())
    {
        info("Nothing to capture.");
        return ESYSREPO_RESULT(ResultCode::OK);
    }

    manifest::MultiFileXML file_xml;
    std::ostringstream oss;

    file_xml.set_capture(capture);
    result = file_xml.write(oss);
    if (result.error()) return ESYSREPO_RESULT(result);

    git_helper = new_git_helper();

    if (!get_folder().empty())
        folder_path = get_folder();
    else if (!get_workspace_path().empty())
        folder_path = get_workspace_path();
    else
        folder_path = boost::filesystem::current_path().string();

    if (folder_path.empty()) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    folder_path = find_git_root_path(folder_path);

    result = git_helper->open(folder_path, log::Level::DEBUG);
    if (result.error()) return ESYSREPO_RESULT(result);

    GitHelper::AutoClose auto_close(git_helper.get());

    auto person = std::make_shared<git::Person>();
    std::string email = "esysrepo@libesys.org";
    std::string name = "ESysRepo";
    person->set_email(email);
    person->set_name(name);
    git_helper->get_git()->set_author(person);

    git::NoteId note_id;
    result = git_helper->get_git()->add_note(note_id, "esysrepo", oss.str(), true);
    if (result.error()) return ESYSREPO_RESULT(result);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result CmdMulti::do_task_delete()
{
    std::string folder_path;

    if (!get_folder().empty())
        folder_path = get_folder();
    else if (!get_workspace_path().empty())
        folder_path = get_workspace_path();
    else
        folder_path = boost::filesystem::current_path().string();

    if (folder_path.empty()) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    folder_path = find_git_root_path(folder_path);

    auto git_helper = new_git_helper();
    auto result = git_helper->open(folder_path, log::Level::DEBUG);
    if (result.error()) return ESYSREPO_RESULT(result);

    GitHelper::AutoClose auto_close(git_helper.get());

    git::Commit last_commit;

    git_helper->get_git()->add_notes_ref("esysrepo");
    result = git_helper->get_git()->get_last_commit(last_commit, true);
    if (result.error()) return ESYSREPO_RESULT(result);

    if (last_commit.get_all_notes().empty())
    {
        info("No notes found to delete.");
        return ESYSREPO_RESULT(ResultCode::OK);
    }

    auto person = std::make_shared<git::Person>();
    std::string email = "esysrepo@libesys.org";
    std::string name = "ESysRepo";
    person->set_email(email);
    person->set_name(name);
    git_helper->get_git()->set_author(person);

    result = git_helper->get_git()->remove_note_last_commit("esysrepo");
    if (result.error()) return ESYSREPO_RESULT(result);
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result CmdMulti::do_task_push()
{
    // First we need to get the working folder
    std::string folder_path;

    if (!get_folder().empty())
        folder_path = get_folder();
    else if (!get_workspace_path().empty())
        folder_path = get_workspace_path();
    else
        folder_path = boost::filesystem::current_path().string();

    if (folder_path.empty()) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    folder_path = find_git_root_path(folder_path);

    // Then load the workspace
    Workspace workspace;

    Result result = workspace.load(folder_path);
    if (result.error()) return ESYSREPO_RESULT(result);

    set_workspace_path(workspace.get_path());

    auto repo_rel_path = boost::filesystem::relative(folder_path, get_workspace_path());
    auto repo = workspace.get_manifest()->find_repo_by_path(repo_rel_path.generic_string());

    std::string remote_str = repo->get_location_str();
    if (remote_str.empty()) return ESYSREPO_RESULT(ResultCode::MANIFEST_REPOSITORY_WITHOUT_LOCATION);

    auto git_helper = new_git_helper();
    result = git_helper->open(folder_path, log::Level::DEBUG);
    if (result.error()) return ESYSREPO_RESULT(result);

    GitHelper::AutoClose auto_close(git_helper.get());

    auto person = std::make_shared<git::Person>();
    std::string email = "esysrepo@libesys.org";
    std::string name = "ESysRepo";
    person->set_email(email);
    person->set_name(name);
    git_helper->get_git()->set_author(person);

    result = git_helper->get_git()->push_notes(remote_str, "esysrepo");
    if (result.error()) return ESYSREPO_RESULT(result);

    return ResultCode::NOT_IMPLEMENTED;
}

Result CmdMulti::do_task_show()
{
    std::string folder_path;

    if (!get_folder().empty())
        folder_path = get_folder();
    else if (!get_workspace_path().empty())
        folder_path = get_workspace_path();
    else
        folder_path = boost::filesystem::current_path().string();

    if (folder_path.empty()) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    folder_path = find_git_root_path(folder_path);

    auto git_helper = new_git_helper();

    auto result = git_helper->open(folder_path, log::Level::DEBUG);
    if (result.error()) return ESYSREPO_RESULT(result);

    GitHelper::AutoClose auto_close(git_helper.get());

    int result_int = git_helper->get_git()->add_notes_ref("esysrepo");
    if (result_int < 0) return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int);

    result = git_helper->get_git()->fetch_all_notes();
    if (result.error()) return ESYSREPO_RESULT(result);

    git::Commit last_commit;

    result = git_helper->get_git()->get_last_commit(last_commit, true);
    if (result.error()) return ESYSREPO_RESULT(result);

    std::vector<std::shared_ptr<git::Note>> notes;
    result_int = last_commit.get_notes("esysrepo", notes);
    if (result_int < 0) return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int);

    std::ostringstream oss;

    oss << "Found " << notes.size() << " esysrepo notes.";

    info(oss.str());

    if (notes.empty()) return ESYSREPO_RESULT(ResultCode::OK);

    for (auto idx = 0; idx < notes.size(); ++idx)
    {
        oss.str("");

        oss << "[" << idx << "] :";

        result = print_esysrepo_note(oss, notes[idx]->get_message());
        if (result.error()) ESYSREPO_RESULT(result);

        info(oss.str());
    }

    return ESYSREPO_RESULT(ResultCode::OK);
}

} // namespace esys::repo::exe
