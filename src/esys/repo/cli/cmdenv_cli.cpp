/*!
 * \file esys/repo/cli/cmdenv_cli.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/cli/cmdenv.h"

#include <msword2md/cpp/string.h>

#include <termcolor/termcolor.hpp>

#include <vector>

namespace esys::repo::cli
{

//#include "esys/repo/cli/cmdenv_doc.cpp"

CmdEnv::CmdEnv(AppBase *app)
    : BaseType(app, "env", "Env")
{
}

CmdEnv::~CmdEnv() = default;

std::shared_ptr<po::options_description> CmdEnv::get_desc()
{
    return nullptr;
}

int CmdEnv::configure_cmd(CmdType &cmd)
{
    return 0;
}

int CmdEnv::print_doc(std::ostream &os)
{
    // os << cmdversion_doc_strings;
    return 0;
}

} // namespace esys::repo::cli
