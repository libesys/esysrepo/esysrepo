/*!
 * \file esys/repo/git/mergeanalysisresult_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/mergeanalysisresult.h"

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::MergeAnalysisResult &result)
{
    switch (result)
    {
        case esys::repo::git::MergeAnalysisResult::NOT_SET: os << "NOT_SET"; break;
        case esys::repo::git::MergeAnalysisResult::NONE: os << "NONE"; break;
        case esys::repo::git::MergeAnalysisResult::NORMAL: os << "NORMAL"; break;
        case esys::repo::git::MergeAnalysisResult::UP_TO_DATE: os << "UP_TO_DATE"; break;
        case esys::repo::git::MergeAnalysisResult::FASTFORWARD: os << "FASTFORWARD"; break;
        case esys::repo::git::MergeAnalysisResult::UNBORN: os << "UNBORN"; break;
        default: os << "<unknown>";
    }
    return os;
}

} // namespace std
