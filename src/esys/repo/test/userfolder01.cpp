/*!
 * \file esys/repo/test/userfolder01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/userfolder.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::test
{

/*! \class UserFolder01 esys/repo/test/userfolder01.cpp "esys/repo/test/userfolder01.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(UserFolder01)
{
    boost::filesystem::path file_path;

    auto &ctrl = repo::test::TestCaseCtrl::get();
    file_path = ctrl.delete_create_temp_folder("userfolder01");
    ESYSTEST_REQUIRE_EQUAL(file_path.string().empty(), false);

    UserFolder user_folder;

    auto result = user_folder.populate_all_pathes();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
    ESYSTEST_REQUIRE_EQUAL(user_folder.get_user_home_path().empty(), false);
    ESYSTEST_REQUIRE_EQUAL(user_folder.get_path().empty(), false);
    ESYSTEST_REQUIRE_NE(user_folder.get_user_config(), nullptr);
    ESYSTEST_REQUIRE_EQUAL(user_folder.get_user_config()->get_temp_path().empty(), false);
    ESYSTEST_REQUIRE_EQUAL(user_folder.get_user_config_file_path().empty(), false);
    ESYSTEST_REQUIRE_EQUAL(user_folder.get_user_config()->get_manifest_directories_path().empty(), false);

    user_folder.set_user_home_path(file_path.string());
    result = user_folder.create();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    UserFolder read_user_folder;
    result = read_user_folder.read(file_path.string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    bool compare = *user_folder.get_user_config() == *read_user_folder.get_user_config();
    ESYSTEST_REQUIRE_EQUAL(compare, true);
}

} // namespace esys::repo::test
