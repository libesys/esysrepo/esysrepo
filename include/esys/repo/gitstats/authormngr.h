/*!
 * \file esys/repo/gitstats/authormngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/person.h"
#include "esys/repo/git/commit.h"

#include <map>
#include <memory>
#include <string>

//<swig_inc/>

namespace esys::repo::gitstats
{

/*! \class AuthorMngr esys/repo/gitstats/authormngr.h "esys/repo/gitstats/authormngr.h"
 * \brief
 */
class ESYSREPO_API AuthorMngr
{
public:
    //! Default constructor
    AuthorMngr();

    //! Destructor
    ~AuthorMngr();

    std::shared_ptr<git::Person> get_author(std::shared_ptr<git::Commit> commit);
    std::shared_ptr<git::Person> get_author_or_new(std::shared_ptr<git::Commit> commit);

private:
    //!< \cond DOXY_IMPL
    std::map<std::string, std::shared_ptr<git::Person>, std::less<>> m_authors; //!< The authors

    //!< \endcond
};

} // namespace esys::repo::gitstats
