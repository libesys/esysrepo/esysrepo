/*!
 * \file esys/repo/manifest/directoryfilexml_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/directoryfilexml.h"
#include "esys/repo/manifest/directoryfilexmlimpl.h"

namespace esys::repo::manifest
{

DirectoryFileXML::DirectoryFileXML()
{
    m_impl = std::make_unique<DirectoryFileXMLImpl>(this);
}

DirectoryFileXML::~DirectoryFileXML() = default;

void DirectoryFileXML::set_directory(std::shared_ptr<Directory> directory)
{
    m_directory = directory;
}

std::shared_ptr<Directory> DirectoryFileXML::get_directory() const
{
    return m_directory;
}

Result DirectoryFileXML::read(const std::string &path)
{
    return m_impl->read(path);
}

Result DirectoryFileXML::read(std::istream &is)
{
    return m_impl->read(is);
}

Result DirectoryFileXML::write(const std::string &path)
{
    return m_impl->write(path);
}

Result DirectoryFileXML::write(std::ostream &os)
{
    return m_impl->write(os);
}
DirectoryFileXMLImpl *DirectoryFileXML::get_impl() const
{
    return m_impl.get();
}

} // namespace esys::repo::manifest
