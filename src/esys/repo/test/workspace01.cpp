/*!
 * \file esys/repo/test/workspace01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/workspace.h>
#include <esys/repo/manifest/repository.h>

#include <iostream>

namespace esys::repo::test
{

/*! \class Workspace01 esys/repo/test/workspace01.cpp "esys/repo/test/workspace01.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(Workspace01)
{
    Workspace workspace;

    auto result = workspace.load();
    ESYSTEST_REQUIRE_EQUAL(result.success(), true);

    auto manifest = workspace.get_manifest();
    ESYSTEST_REQUIRE_NE(manifest, nullptr);

    auto repository = manifest->find_repo_by_path("src/esysrepo");
    ESYSTEST_REQUIRE_NE(repository, nullptr);

    std::string str = repository->get_name();
    ESYSTEST_REQUIRE_EQUAL(str, "esysrepo/esysrepo");

    str = repository->get_location_str();
    ESYSTEST_REQUIRE_EQUAL(str, "origin");

    repository = manifest->find_repo_by_path("extlib/nlohmann_json");
    ESYSTEST_REQUIRE_NE(repository, nullptr);

    str = repository->get_name();
    ESYSTEST_REQUIRE_EQUAL(str, "nlohmann/json");

    str = repository->get_location_str();
    ESYSTEST_REQUIRE_EQUAL(str, "github");
}

} // namespace esys::repo::test
