/*!
 * \file esys/repo/manifest/base_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/base.h"

namespace esys::repo::manifest
{

const std::string Base::s_folder_name = "esysrepo";

const std::string &Base::get_folder_name()
{
    return s_folder_name;
}

Base::Base() = default;

Base::~Base() = default;

} // namespace esys::repo::manifest
