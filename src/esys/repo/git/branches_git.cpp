/*!
 * \file esys/repo/git/branches_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/branches.h"

#include <algorithm>

namespace esys::repo::git
{

Branches::Branches() = default;

Branches::Branches(const std::vector<Branch> &branches)
{
    for (auto &branch_raw : branches)
    {
        std::shared_ptr<Branch> branch = std::make_shared<Branch>(branch_raw);

        add(branch);
    }
}

Branches::~Branches() = default;

void Branches::add(std::shared_ptr<Branch> branch)
{
    m_branches.push_back(branch);
    if (branch->get_is_head()) m_head = branch;
}

void Branches::clear()
{
    m_head = nullptr;
    m_branches.clear();
}

std::shared_ptr<Branch> Branches::get_head() const
{
    return m_head;
}

std::size_t Branches::size() const
{
    return m_branches.size();
}

void Branches::sort()
{
    auto head_first = [](const std::shared_ptr<git::Branch> b0, const std::shared_ptr<git::Branch> b1) -> bool {
        return b0->get_is_head();
    };

    std::sort(get().begin(), get().end(), head_first);
}

std::vector<std::shared_ptr<Branch>> &Branches::get()
{
    return m_branches;
}

const std::vector<std::shared_ptr<Branch>> &Branches::get() const
{
    return m_branches;
}

std::shared_ptr<Branch> Branches::find(const std::string &name)
{
    for (auto branch : get())
    {
        if (branch->get_name() == name) return branch;
    }
    return nullptr;
}

bool Branches::operator==(const Branches &other) const
{
    if (get().size() != other.get().size()) return false;
    for (auto idx = 0; idx < get().size(); ++idx)
    {
        if ((get()[idx] == nullptr) && (other.get()[idx] == nullptr)) continue;
        if ((get()[idx] == nullptr) || (other.get()[idx] == nullptr)) return false;
        if (*get()[idx] != *other.get()[idx]) return false;
    }
    if ((get_head() == nullptr) && (other.get_head() == nullptr)) return true;
    if ((get_head() == nullptr) || (other.get_head() == nullptr)) return false;
    return (*get_head() == *other.get_head());
}

bool Branches::operator!=(const Branches &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::Branches &branches)
{
    os << "Branches(" << branches.size() << ")" << std::endl;
    os << "{" << std::endl;
    for (auto branch : branches.get())
    {
        os << "    name: " << branch->get_name();
        if (branch->get_is_head()) os << " [head]";
        os << std::endl;
    }
    os << "}";
    return os;
}

} // namespace std
