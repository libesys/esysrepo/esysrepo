/*!
 * \file esys/repo/manifest/directoryfilexml.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/directory.h"
#include "esys/repo/result.h"

#include <memory>
#include <string>

namespace esys::repo::manifest
{

class ESYSREPO_API DirectoryFileXMLImpl;

/*! \class DirectoryFileXML esys/repo/manifest/directoryfilexml.h "esys/repo/manifest/directoryfilexml.h"
 * \brief
 */
class ESYSREPO_API DirectoryFileXML
{
public:


    //! Default constructor
    DirectoryFileXML();

    //! Destructor
    ~DirectoryFileXML();

    //! Set the Directory data
    /*!
     * \param[in] directory the capture data
     */
    void set_directory(std::shared_ptr<Directory> directory);

    //! Get the Directory data
    /*!
     * \return the Directory data
     */
    std::shared_ptr<Directory> get_directory() const;


    Result read(const std::string &path);

    Result read(std::istream &is);

    Result write(const std::string &path);

    Result write(std::ostream &os);

    DirectoryFileXMLImpl *get_impl() const;


private:
    //!< \cond DOXY_IMPL
    std::unique_ptr <DirectoryFileXMLImpl> m_impl;
    std::shared_ptr<Directory> m_directory;
    //!< \endcond
};

} // namespace esys::repo::manifest
