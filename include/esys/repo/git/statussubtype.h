/*!
 * \file esys/repo/git/statussubtype.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <string>

//<swig_inc/>

namespace esys
{

namespace repo
{

namespace git
{

enum class StatusSubType
{
    NOT_SET,
    NEW,
    MODIFIED,
    DELETED,
    TYPECHANGE,
    RENAMED,
    UNREADABLE
};

} // namespace git

} // namespace repo

} // namespace esys
