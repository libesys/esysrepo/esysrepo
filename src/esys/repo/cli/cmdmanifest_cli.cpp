/*!
 * \file esys/repo/cli/cmdmanifest_cli.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/cli/cmdmanifest.h"

#include <msword2md/cpp/string.h>

#include <termcolor/termcolor.hpp>

#include <boost/filesystem.hpp>

#include <vector>

namespace esys::repo::cli
{

#include "esys/repo/cli/cmdmanifest_doc.cpp"

CmdManifest::CmdManifest(AppBase *app)
    : BaseType(app, "manifest", "Manifest tools")
{
}

CmdManifest::~CmdManifest() = default;

std::shared_ptr<po::options_description> CmdManifest::get_desc()
{
    auto desc = Cmd::get_desc();
    if (desc != nullptr) return desc;

    desc = std::make_shared<po::options_description>("Manifest options");
    // clang-format off
    desc->add_options()
        ("help,h", "produce help message")
        ("revision-as-HEAD,r", "Save revisions as current HEAD")
        ("output-file,o", po::value<std::string>(), "File to save the manifest to")
        ("list-dirs,l", "List all manifest directories")
        ("list-projects,p", po::value<std::string>()->implicit_value(""),
            "List all projects from one or all manifest directories")
        ("add-dir,a", po::value<std::string>(), "Add a manifest directory <name> <ssh> <https>")
        ("del-dir,d", po::value<std::string>(), "Delete a manifest directory <name>")
        ;
    // clang-format on
    Cmd::set_desc(desc);
    return desc;
}

int CmdManifest::configure_cmd(CmdType &cmd)
{
    if (get_vm().count("revision-as-HEAD"))
        cmd.set_task(CmdType::Task::REVISION_AS_HEAD);
    else if (get_vm().count("list-dirs"))
        cmd.set_task(CmdType::Task::LIST_DIRS);
    else if (get_vm().count("list-projects"))
    {
        cmd.set_task(CmdType::Task::LIST_PROJECTS);
        cmd.set_dirs(get_vm()["list-projects"].as<std::string>());
    }
    else if (get_vm().count("add-dir"))
    {
        CmdType::TaskAddDir add_dir;

        add_dir.set_name(get_vm()["add-dir"].as<std::string>());

        for (auto const &url : get_sub_args())
        {
            if (url.find("ssh://") != std::string::npos)
                add_dir.set_url_ssh(url);
            else if (url.find("https://") != std::string::npos)
                add_dir.set_url_https(url);
            else
                error("url not known protocol for '" + url + "'");
        }

        cmd.set_task_add_dir(add_dir);
    }
    else if (get_vm().count("del-dir"))
    {
        CmdType::TaskDelDir del_dir;

        del_dir.set_name(get_vm()["del-dir"].as<std::string>());

        cmd.set_task_del_dir(del_dir);
    }
    if (get_vm().count("output-file")) cmd.set_output_file(get_vm()["output-file"].as<std::string>());

    return 0;
}

int CmdManifest::print_doc(std::ostream &os)
{
    os << cmdmanifest_doc_strings;
    return 0;
}

} // namespace esys::repo::cli
