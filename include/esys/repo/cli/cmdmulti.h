/*!
 * \file esys/repo/cli/cmdmulti.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/cli/cmd_t.h"
#include "esys/repo/exe/cmdmulti.h"

namespace esys::repo::cli
{

class ESYSREPO_API CmdMulti : public Cmd_t<exe::CmdMulti>
{
public:
    using BaseType = Cmd_t<exe::CmdMulti>;

    explicit CmdMulti(AppBase *app = nullptr);
    ~CmdMulti() override;

    int configure_cmd(CmdType &cmd) override;
    std::shared_ptr<po::options_description> get_desc() override;

    int print_doc(std::ostream &os) override;
};

} // namespace esys::repo::cli
