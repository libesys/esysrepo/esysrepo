/*!
 * \file esys/repo/manifest/directory.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/directory.h"

namespace esys::repo::manifest
{

Directory::Item::Item() = default;

Directory::Item::~Item() = default;

void Directory::Item::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &Directory::Item::get_name() const
{
    return m_name;
}

void Directory::Item::set_url_ssh(const std::string &url_ssh)
{
    m_url_ssh = url_ssh;
}

const std::string &Directory::Item::get_url_ssh() const
{
    return m_url_ssh;
}

void Directory::Item::set_url_https(const std::string &url_https)
{
    m_url_https = url_https;
}

const std::string &Directory::Item::get_url_https() const
{
    return m_url_https;
}

bool Directory::Item::operator==(const Item &other) const
{
    if (get_name() != other.get_name()) return false;
    if (get_url_ssh() != other.get_url_ssh()) return false;
    if (get_url_https() != other.get_url_https()) return false;
    return true;
}

bool Directory::Item::operator!=(const Item &other) const
{
    return !operator==(other);
}

Directory::Directory() = default;

Directory::~Directory() = default;

void Directory::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &Directory::get_name() const
{
    return m_name;
}

void Directory::add_item(const std::string &name, const std::string &url_ssh, const std::string &url_https)
{
    auto item = std::make_shared<Item>();
    item->set_name(name);
    item->set_url_ssh(url_ssh);
    item->set_url_https(url_https);

    add_item(item);
}

void Directory::add_item(std::shared_ptr<Item> item)
{
    m_items.push_back(item);
    m_map_items[item->get_name()] = item;
}

const std::vector<std::shared_ptr<Directory::Item>> &Directory::get_items() const
{
    return m_items;
}

std::shared_ptr<Directory::Item> Directory::find(const std::string &name) const
{
    auto it = m_map_items.find(name);
    if (it == m_map_items.end()) return nullptr;
    return it->second;
}

void Directory::clear()
{
    set_name("");

    m_items.clear();
    m_map_items.clear();
}

bool Directory::operator==(const Directory &other) const
{
    if (get_name() != other.get_name()) return false;

    if (get_items().size() != other.get_items().size()) return false;

    for (auto idx = 0; idx < get_items().size(); ++idx)
    {
        if (*get_items()[idx] != *other.get_items()[idx]) return false;
    }
    return true;
}

bool Directory::operator!=(const Directory &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::manifest
