/*!
 * \file esys/repo/userconfigfile.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/result.h"
#include "esys/repo/userconfig.h"

#include <string>
#include <memory>

namespace esys::repo
{

class ESYSREPO_API UserConfigFileImpl;

/*! \class UserConfigFile esys/repo/userconfigfile.h "esys/repo/userconfigfile.h"
 * \brief Read and write the ESysRepo user configuration file
 */
class ESYSREPO_API UserConfigFile
{
public:
    //! Default constructor
    UserConfigFile();

    //! Destructor
    ~UserConfigFile();

    //! Set the path of the config file
    /*!
     * \param[in] path the path of the config file
     */
    void set_path(const std::string &path);

    //! Get the path of the config file
    /*!
     * \return the path of the config file
     */
    const std::string &get_path() const;

    //! Set the user configuration data of the config file
    /*!
     * \param[in] user_config the user configuration data of the config file
     */
    void set_config(std::shared_ptr<UserConfig> user_config);

    //! Get the user configuration data of the config file
    /*!
     * \return the user configuration data of the config file
     */
    std::shared_ptr<UserConfig> get_config() const;

    //! Open and read a config file
    /*!
     * \param[in] path the path of the config file to open and read
     */
    Result read(const std::string &path = "");

    //! Write a config file
    /*!
     * \param[in] path the path of the config file to write
     */
    Result write(const std::string &path = "");

    //! Get the PIMPL
    /*!
     * \return the PIMPL
     */
    UserConfigFileImpl *get_impl();

    //! Get the PIMPL
    /*!
     * \return the PIMPL
     */
    const UserConfigFileImpl *get_impl() const;

private:
    //!< \cond DOXY_IMPL
    std::unique_ptr<UserConfigFileImpl> m_impl; //!< the PIMPL
    std::shared_ptr<UserConfig> m_user_config;  //!< The configuration data
    std::string m_path;                         //!< The path of the config file
    //!< \endcond
};

} // namespace esys::repo
