/*!
 * \file esys/repo/manifest/multifilexmlimpl_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/multifilexmlimpl.h"

#include <esysfile/xml/writer.h>
#include <esysfile/xml/file.h>
#include <esysfile/xml/attr.h>

namespace esys::repo::manifest
{

MultiFileXMLImpl::MultiFileXMLImpl(MultiFileXML *self)
    : m_self(self)
{
}

MultiFileXMLImpl::~MultiFileXMLImpl() = default;

Result MultiFileXMLImpl::read(const std::string &path)
{
    esysfile::xml::File xml_file;

    xml_file.set_root_node_name("esysrepo_multi");
    int result = xml_file.read(path);
    if (result < 0)
    {
        // self()->add_error(result, "XML parsing failed.");
        return ESYSREPO_RESULT(ResultCode::MULTI_ERROR_PARSING_FILE, path);
    }

    if (self()->get_capture() == nullptr)
        self()->set_capture(std::make_shared<Capture>());
    else
        self()->get_capture()->clear();

    return read(xml_file.get_data());
}

Result MultiFileXMLImpl::read(std::shared_ptr<esysfile::xml::Data> data)
{
    Result result;

    for (auto el : data->get_elements())
    {
        if (el->get_name() == "capture")
            result = read_capture(el);
        else
            result = ESYSREPO_RESULT(ResultCode::MULTI_ELEMENT_UNKNOWN, el->get_name());
        if (result.error())
        {
            // self()->add_error(result.get_result_code_int(), "Reading XML element failed : " + el->get_name());
            return result;
        }
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result MultiFileXMLImpl::read_capture(std::shared_ptr<esysfile::xml::Element> el)
{
    if (el->get_name() != "capture") return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    for (auto repo_el : el->get_elements())
    {
        if (repo_el->get_name() != "repository")
            return ESYSREPO_RESULT(ResultCode::MULTI_ELEMENT_UNKNOWN, el->get_name());

        auto path_attr = repo_el->get_attr("path");
        if (path_attr == nullptr) return ESYSREPO_RESULT(ResultCode::MULTI_REPOSITORY_WITHOUT_PATH);

        auto rev_attr = repo_el->get_attr("revision");
        if (rev_attr == nullptr) return ESYSREPO_RESULT(ResultCode::MULTI_REPOSITORY_WITHOUT_REV);

        auto remote_name_attr = repo_el->get_attr("remote_name");
        auto remote_url_attr = repo_el->get_attr("remote_url");

        auto capture_item = std::make_shared<Capture::Item>();
        capture_item->set_path(path_attr->get_value());
        capture_item->set_revision(rev_attr->get_value());
        if (remote_name_attr != nullptr) capture_item->set_remote_name(remote_name_attr->get_value());
        if (remote_url_attr != nullptr) capture_item->set_remote_url(remote_url_attr->get_value());

        self()->get_capture()->add_item(capture_item);
    }

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result MultiFileXMLImpl::read(std::istream &is)
{
    esysfile::xml::File xml_file;

    xml_file.set_root_node_name("esysrepo_multi");
    int result = xml_file.read(is);
    if (result < 0)
    {
        // self()->add_error(result, "XML parsing failed.");
        return ESYSREPO_RESULT(ResultCode::MULTI_ERROR_PARSING_STREAM);
    }

    if (self()->get_capture() == nullptr)
        self()->set_capture(std::make_shared<Capture>());
    else
        self()->get_capture()->clear();

    return read(xml_file.get_data());
}

Result MultiFileXMLImpl::write(const std::string &path)
{
    Result result = write_xml();
    if (result.error()) return ESYSREPO_RESULT(result);

    esysfile::xml::Writer writer;

    writer.set_indent(4);
    int result_int = writer.write(path, m_xml_data);
    if (result_int < 0) return ESYSREPO_RESULT(ResultCode::RAW_INT_ERROR, result_int);
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result MultiFileXMLImpl::write(std::ostream &os)
{
    Result result = write_xml();
    if (result.error()) return ESYSREPO_RESULT(result);

    esysfile::xml::Writer writer;

    writer.set_indent(4);
    int result_int = writer.write(os, m_xml_data);
    if (result_int < 0) return ESYSREPO_RESULT(ResultCode::RAW_INT_ERROR, result_int);
    return ESYSREPO_RESULT(ResultCode::OK);
}

MultiFileXML *MultiFileXMLImpl::self()
{
    return m_self;
}

Result MultiFileXMLImpl::write(std::shared_ptr<esysfile::xml::Element> parent_el, std::shared_ptr<Capture> capture)
{
    auto capture_el = std::make_shared<esysfile::xml::Element>();
    parent_el->add_element(capture_el);

    capture_el->set_name("capture");

    for (auto const &item : capture->get_items())
    {
        auto repo_el = std::make_shared<esysfile::xml::Element>();
        capture_el->add_element(repo_el);

        repo_el->set_name("repository");
        if (!item->get_path().empty()) repo_el->add_attr("path", item->get_path());
        if (!item->get_revision().empty()) repo_el->add_attr("revision", item->get_revision());
        if (!item->get_remote_name().empty()) repo_el->add_attr("remote_name", item->get_remote_name());
        if (!item->get_remote_url().empty()) repo_el->add_attr("remote_url", item->get_remote_url());
    }

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result MultiFileXMLImpl::write_xml()
{
    if (self()->get_capture() == nullptr) return ESYSREPO_RESULT(ResultCode::MULTI_XML_CAPTURE_NULLPTR);

    m_xml_data = std::make_shared<esysfile::xml::Data>();
    m_xml_data->set_root_node_name("esysrepo_multi");

    auto result = write(m_xml_data, self()->get_capture());

    return ESYSREPO_RESULT(result);
}

} // namespace esys::repo::manifest
