/*!
 * \file esys/repo/libgit2/libgit2.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

namespace esys
{

namespace repo
{

namespace libgit2
{

class ESYSREPO_API LibGit2
{
public:
    LibGit2();
    ~LibGit2();
};

} // namespace libgit2

} // namespace repo

} // namespace esys
