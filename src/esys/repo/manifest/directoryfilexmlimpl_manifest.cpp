/*!
 * \file esys/repo/manifest/directoryfilexmlimpl_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/directoryfilexmlimpl.h"

#include <esysfile/xml/writer.h>
#include <esysfile/xml/file.h>
#include <esysfile/xml/attr.h>

namespace esys::repo::manifest
{

DirectoryFileXMLImpl::DirectoryFileXMLImpl(DirectoryFileXML *self)
    : m_self(self)
{
}

DirectoryFileXMLImpl::~DirectoryFileXMLImpl() = default;

Result DirectoryFileXMLImpl::read(const std::string &path)
{
    esysfile::xml::File xml_file;

    xml_file.set_root_node_name("esysrepo_manifest_dir");
    int result = xml_file.read(path);
    if (result < 0)
    {
        // self()->add_error(result, "XML parsing failed.");
        return ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_ERROR_PARSING_FILE, path);
    }

    if (self()->get_directory() == nullptr)
        self()->set_directory(std::make_shared<Directory>());
    else
        self()->get_directory()->clear();

    return read(xml_file.get_data());
}

Result DirectoryFileXMLImpl::read(std::shared_ptr<esysfile::xml::Data> data)
{
    Result result;
    auto directory = self()->get_directory();
    auto name = data->get_attr("name");

    if (name != nullptr) directory->set_name(name->get_value());

    for (auto el : data->get_elements())
    {
        if (el->get_name() == "projects")
            result = read_projects(el);
        else
            result = ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_ELEMENT_UNKNOWN, el->get_name());
        if (result.error())
        {
            return result;
        }
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result DirectoryFileXMLImpl::read_projects(std::shared_ptr<esysfile::xml::Element> el)
{
    if (el->get_name() != "projects") return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    auto directory = self()->get_directory();

    for (auto project_el : el->get_elements())
    {
        if (project_el->get_name() != "project")
            return ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_ELEMENT_UNKNOWN, el->get_name());

        auto item = std::make_shared<Directory::Item>();
        auto name_attr = project_el->get_attr("name");
        item->set_name(name_attr->get_value());

        auto urls_el = project_el->get_element("urls");

        for (auto url_el : urls_el->get_elements())
        {
            auto type_attr = url_el->get_attr("type");
            if (type_attr == nullptr) return ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_MISSING_ATTR, "type");

            auto link_attr = url_el->get_attr("link");
            if (link_attr == nullptr) return ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_MISSING_ATTR, "link");

            if (type_attr->get_value() == "ssh")
                item->set_url_ssh(link_attr->get_value());
            else if (type_attr->get_value() == "https")
                item->set_url_https(link_attr->get_value());
            else
            {
                return ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_UNKNOWN_URL_TYPE, type_attr->get_value());
            }
        }

        directory->add_item(item);
    }

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result DirectoryFileXMLImpl::read(std::istream &is)
{
    esysfile::xml::File xml_file;

    xml_file.set_root_node_name("esysrepo_manifest_dir");
    int result = xml_file.read(is);
    if (result < 0)
    {
        return ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_ERROR_PARSING_STREAM);
    }

    if (self()->get_directory() == nullptr)
        self()->set_directory(std::make_shared<Directory>());
    else
        self()->get_directory()->clear();

    return read(xml_file.get_data());
}

Result DirectoryFileXMLImpl::write(const std::string &path)
{
    Result result = write_xml();
    if (result.error()) return ESYSREPO_RESULT(result);

    esysfile::xml::Writer writer;

    writer.set_indent(4);
    int result_int = writer.write(path, m_xml_data);
    if (result_int < 0) return ESYSREPO_RESULT(ResultCode::RAW_INT_ERROR, result_int);
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result DirectoryFileXMLImpl::write(std::ostream &os)
{
    Result result = write_xml();
    if (result.error()) return ESYSREPO_RESULT(result);

    esysfile::xml::Writer writer;

    writer.set_indent(4);
    int result_int = writer.write(os, m_xml_data);
    if (result_int < 0) return ESYSREPO_RESULT(ResultCode::RAW_INT_ERROR, result_int);
    return ESYSREPO_RESULT(ResultCode::OK);
}

DirectoryFileXML *DirectoryFileXMLImpl::self() const
{
    return m_self;
}

Result DirectoryFileXMLImpl::write(std::shared_ptr<esysfile::xml::Element> parent_el,
                                   std::shared_ptr<Directory> directory)
{
    auto projects_el = std::make_shared<esysfile::xml::Element>();
    parent_el->add_element(projects_el);

    projects_el->set_name("projects");

    for (auto const &item : directory->get_items())
    {
        auto project_el = std::make_shared<esysfile::xml::Element>();
        projects_el->add_element(project_el);

        project_el->set_name("project");
        project_el->add_attr("name", item->get_name());

        auto urls_el = std::make_shared<esysfile::xml::Element>();
        project_el->add_element(urls_el);

        urls_el->set_name("urls");

        auto url_ssh_el = std::make_shared<esysfile::xml::Element>();
        urls_el->add_element(url_ssh_el);

        url_ssh_el->set_name("url");
        url_ssh_el->add_attr("type", "ssh");
        url_ssh_el->add_attr("link", item->get_url_ssh());

        if (!item->get_url_https().empty())
        {
            auto url_https_el = std::make_shared<esysfile::xml::Element>();
            urls_el->add_element(url_https_el);

            url_https_el->set_name("url");
            url_https_el->add_attr("type", "https");
            url_https_el->add_attr("link", item->get_url_https());
        }
    }

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result DirectoryFileXMLImpl::write_xml()
{
    if (self()->get_directory() == nullptr) return ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_DATA_NULLPTR);

    m_xml_data = std::make_shared<esysfile::xml::Data>();
    m_xml_data->set_root_node_name("esysrepo_manifest_dir");
    m_xml_data->set_root_attr_name(self()->get_directory()->get_name());

    auto result = write(m_xml_data, self()->get_directory());

    return ESYSREPO_RESULT(result);
}

} // namespace esys::repo::manifest
