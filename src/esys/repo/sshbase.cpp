/*!
 * \file esys/repo/sshbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/sshbase.h"

namespace esys::repo
{

SSHBase::SSHBase()
    : log::User()
{
}

SSHBase::~SSHBase() = default;

void SSHBase::_set_agent_present(AgentPresent agent_present)
{
    m_agent_present = agent_present;
}

SSHBase::AgentPresent SSHBase::_get_agent_present() const
{
    return m_agent_present;
}

void SSHBase::set_agent_identity_path(const std::string &agent_identity_path)
{
    m_agent_identity_path = agent_identity_path;
}

const std::string &SSHBase::get_agent_identity_path() const
{
    return m_agent_identity_path;
}

} // namespace esys::repo
