/*!
 * \file esys/repo/exe/cmdsync.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/exe/cmdsync.h"
#include "esys/repo/manifest/capture.h"
#include "esys/repo/manifest/sync.h"
#include "esys/repo/manifest/syncrepos.h"

#include <esys/trace/call.h>
#include <esys/trace/macros.h>

#include <boost/filesystem.hpp>

#include <sstream>

namespace esys::repo::exe
{

CmdSync::CmdSync()
    : Cmd("Sync")
{
}

CmdSync::~CmdSync() = default;

void CmdSync::set_force(bool force)
{
    m_force = force;
}

bool CmdSync::get_force() const
{
    return m_force;
}

void CmdSync::set_branch(const std::string &branch)
{
    m_branch = branch;
}

const std::string &CmdSync::get_branch() const
{
    return m_branch;
}

void CmdSync::set_alt_address(bool alt_address)
{
    m_alt_address = alt_address;
}
bool CmdSync::get_alt_address() const
{
    return m_alt_address;
}

void CmdSync::set_multi(bool multi)
{
    m_multi = multi;
}

bool CmdSync::get_multi() const
{
    return m_multi;
}

Result CmdSync::sync_manifest()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    manifest::Sync sync;

    if (get_config_folder() == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::CMD_CONFIG_FOLDER_NULLPTR));

    sync.set_config_folder(get_config_folder());
    sync.set_git(get_git());
    sync.set_logger_if(get_logger_if());
    if (!get_branch().empty()) sync.set_branch(get_branch());

    return ETRC_RET(ESYSREPO_RESULT(sync.run()));
}

Result CmdSync::handle_capture_item(std::shared_ptr<manifest::Capture::Item> item)
{
    Result result;
    ETRC_CALL_RET(result, item);

    if (get_workspace_path().empty()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::CMD_WORKSPACE_PATH_IS_EMPTY));

    boost::filesystem::path git_path_folder = get_workspace_path();
    git_path_folder /= item->get_path();
    git_path_folder = git_path_folder.normalize().make_preferred();

    auto git_helper = new_git_helper();
    result = git_helper->open(git_path_folder.string(), log::Level::DEBUG);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    GitHelper::AutoClose auto_close(git_helper.get());

    bool dirty = false;
    result = git_helper->is_dirty(dirty, log::Level::DEBUG);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    // If there is a remote defined, create it and fetch it
    if (!item->get_remote_name().empty())
    {
        result = git_helper->get_git()->add_remote(item->get_remote_name(), item->get_remote_url());
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

        result = git_helper->get_git()->fetch(item->get_remote_name());
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));
    }

    // Checkout the new revision
    result = git_helper->checkout(item->get_revision(), false, log::Level::DEBUG);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result CmdSync::do_multi()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    // First we need to get the working folder
    std::string folder_path;

    if (!get_folder().empty())
        folder_path = get_folder();
    else if (!get_workspace_path().empty())
        folder_path = get_workspace_path();
    else
        folder_path = boost::filesystem::current_path().string();
    if (folder_path.empty()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    auto git_folder_path = find_git_root_path(folder_path);
    auto git_helper = new_git_helper();

    set_git_folder_path(git_folder_path);
    result = git_helper->open(git_folder_path, log::Level::DEBUG);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    GitHelper::AutoClose auto_close(git_helper.get());

    int result_int = git_helper->get_git()->add_notes_ref("esysrepo");
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    result = git_helper->get_git()->fetch_all_notes();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    git::Commit last_commit;

    result = git_helper->get_git()->get_last_commit(last_commit, true);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    std::vector<std::shared_ptr<git::Note>> notes;
    result_int = last_commit.get_notes("esysrepo", notes);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    std::ostringstream oss;

    oss << "Found " << notes.size() << " esysrepo multi note.";

    info(oss.str());

    if (notes.empty()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
    if (notes.size() != 1) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    std::string note_text = notes[0]->get_message();
    auto capture = std::make_shared<manifest::Capture>();
    result = load_capture(capture, note_text);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    oss.str("");
    print_capture(oss, capture);
    info(oss.str());

    Result rresult;
    for (auto repo : capture->get_items())
    {
        result = handle_capture_item(repo);
        if (result.error())
        {
            std::ostringstream e_oss;

            e_oss << "repo '" << repo->get_path() << "' could set revivions '" << repo->get_revision() << "'";
            error(e_oss.str());
            rresult.add(result);
        }
    }

    return ETRC_RET(ESYSREPO_RESULT(rresult));
}

void CmdSync::set_git_folder_path(const std::string &git_folder_path)
{
    m_git_folder_path = git_folder_path;
}

const std::string &CmdSync::get_git_folder_path() const
{
    return m_git_folder_path;
}

Result CmdSync::impl_run()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    if (get_force()) warn("Option --force-sync is not implemented yet");

    result = default_handling_folder_workspace();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    result = open_esysrepo_folder();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    result = sync_manifest();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    result = load_manifest();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    manifest::SyncRepos sync_repos;

    if (!get_branch().empty()) sync_repos.set_branch(get_branch());

    if (get_sub_args().size() != 0)
        sync_repos.set_folders_to_sync(get_sub_args());
    else if (get_groups().size() != 0)
    {
        std::vector<std::string> repos;

        for (auto &group : get_groups())
        {
            auto group_ptr = get_manifest()->get_groups().find_group_by_name(group);
            if (group_ptr == nullptr)
            {
                //! \TODO
                continue;
            }
            for (auto repo : group_ptr->get_repos())
            {
                repos.push_back(repo->get_path());
            }
        }

        sync_repos.set_folders_to_sync(repos);
    }

    if (get_debug()) sync_repos.set_log_level(log::Level::DEBUG);

    sync_repos.set_job_count(get_job_count());
    sync_repos.set_logger_if(get_logger_if());
    sync_repos.set_config_folder(get_config_folder());
    sync_repos.set_git(get_git());
    sync_repos.set_manifest(get_manifest());

    get_git()->detect_ssh_agent(true);

    result = sync_repos.run();
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    if (!get_multi()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
    return ETRC_RET(ESYSREPO_RESULT(do_multi()));
}

} // namespace esys::repo::exe
