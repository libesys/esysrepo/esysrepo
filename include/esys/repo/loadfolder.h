/*!
 * \file esys/repo/loadfolder.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/config.h"
#include "esys/repo/configfolder.h"
#include "esys/repo/manifest.h"
#include "esys/repo/result.h"

#include <memory>
#include <string>

//<swig_inc/>

namespace esys::repo
{

/*! \class LoadFolder esys/repo/loadfolder.h "esys/repo/loadfolder.h"
 * \brief
 */
class ESYSREPO_API LoadFolder
{
public:
    LoadFolder(const std::string &folder_path = "");
    ~LoadFolder();

    void set_folder_path(const std::string &folder_path);
    const std::string &get_folder_path() const;

    Result run(const std::string &folder_path = "");
    std::shared_ptr<Manifest> get_manifest() const;

    std::string find_repo_path_by_url(const std::string &url);
    std::shared_ptr<manifest::Repository> find_repo_by_url(const std::string &url);

    //! Get the configuration
    /*!
     * \return the configuration
     */
    std::shared_ptr<Config> get_config() const;

    //! Get the ESysRepo configuration folder
    /*!
     * \return the ESysRepo configuration folder
     */
    std::shared_ptr<ConfigFolder> get_config_folder() const;

private:
    void set_manifest(std::shared_ptr<Manifest> manifest);

    std::string m_folder_path;
    std::shared_ptr<Manifest> m_manifest;
    std::shared_ptr<Config> m_config;
    std::shared_ptr<ConfigFolder> m_config_folder;
};

} // namespace esys::repo
