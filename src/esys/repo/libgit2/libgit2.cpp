/*!
 * \file esys/repo/libgit2/libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/libgit2/libgit2.h"

#include <git2.h>

#include <iostream>

namespace esys::repo::libgit2
{

LibGit2::LibGit2()
{
    git_libgit2_init();
}

LibGit2::~LibGit2()
{
    git_libgit2_shutdown();
}

} // namespace esys::repo::libgit2
