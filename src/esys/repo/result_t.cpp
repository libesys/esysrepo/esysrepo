/*!
 * \file esys/repo/result_t.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/result_t.h"

#include <iomanip>

namespace esys::repo
{

template<>
ESYSREPO_API void PrintResult_t<bool>(std::ostream &os, const Result_t<bool> &result)
{
    os << "Return value :" << std::endl;
    os << "    type  : " << typeid(bool).name() << std::endl;
    os << "    value : " << std::boolalpha << result.get() << std::endl;
}

} // namespace esys::repo
