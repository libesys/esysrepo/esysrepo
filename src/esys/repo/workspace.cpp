/*!
 * \file esys/repo/workspace.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/repository.h"
#include "esys/repo/workspace.h"

#include "esys/repo/configfolder.h"

#include <boost/filesystem.hpp>

namespace esys::repo
{

Workspace::Workspace() = default;

Workspace::~Workspace() = default;

void Workspace::set_path(const std::string &path)
{
    m_path = path;
}

const std::string &Workspace::get_path() const
{
    return m_path;
}

void Workspace::set_folder_path(const std::string &folder_path)
{
    m_folder_path = folder_path;
}

const std::string &Workspace::get_folder_path() const
{
    return m_folder_path;
}

void Workspace::set_kind(Kind kind)
{
    m_kind = kind;
}

Workspace::Kind Workspace::get_kind() const
{
    return m_kind;
}

Result Workspace::find(const std::string &path)
{
    boost::filesystem::path cur_path;

    if (!path.empty())
        cur_path = path;
    else
        cur_path = boost::filesystem::current_path();

    set_folder_path(cur_path.string());

    while (!cur_path.empty())
    {
        if (is_esysrepo_folder(cur_path.string()))
        {
            set_path(cur_path.string());
            set_kind(Kind::ESYSREPO);
            return ResultCode::OK;
        }
        if (is_grepo_folder(cur_path.string()))
        {
            set_path(cur_path.string());
            set_kind(Kind::GREPO);
            return ResultCode::OK;
        }
        cur_path = cur_path.parent_path();
    }

    return ResultCode::GENERIC_ERROR;
}

Result Workspace::load(const std::string &path)
{
    auto result = find(path);
    if (result.error()) return ESYSREPO_RESULT(result);

    if (get_load_folder() == nullptr) set_load_folder(std::make_shared<LoadFolder>());

    get_load_folder()->set_folder_path(get_path());
    result = get_load_folder()->run();
    if (result.ok())
    {
        m_manifest = get_load_folder()->get_manifest();
    }
    return ESYSREPO_RESULT(result);
}

std::string Workspace::get_full_path(std::shared_ptr<manifest::Repository> repo) const
{
    boost::filesystem::path p = get_path();

    p /= repo->get_path();

    return p.string();
}

void Workspace::set_load_folder(std::shared_ptr<LoadFolder> load_folder)
{
    m_load_folder = load_folder;
}

std::shared_ptr<LoadFolder> Workspace::get_load_folder() const
{
    return m_load_folder;
}

std::shared_ptr<ConfigFolder> Workspace::get_config_folder() const
{
    if (get_load_folder() == nullptr) return nullptr;
    return get_load_folder()->get_config_folder();
}

std::shared_ptr<Manifest> Workspace::get_manifest() const
{
    return m_manifest;
}

bool Workspace::is_grepo_folder(const std::string &path)
{
    boost::filesystem::path folder_path = path;

    folder_path /= ".repo";
    return boost::filesystem::exists(folder_path);
}

bool Workspace::is_esysrepo_folder(const std::string &path)
{
    boost::filesystem::path folder_path = path;

    folder_path /= ".esysrepo";
    return boost::filesystem::exists(folder_path);
}

} // namespace esys::repo
