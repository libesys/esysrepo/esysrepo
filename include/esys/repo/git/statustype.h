/*!
 * \file esys/repo/git/statustype.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <string>

//<swig_inc/>

namespace esys
{

namespace repo
{

namespace git
{

enum class StatusType
{
    NOT_SET,
    CURRENT,
    INDEX,
    WORKING_DIR,
    IGNORED,
    CONFLICTED,
};

} // namespace git

} // namespace repo

} // namespace esys
