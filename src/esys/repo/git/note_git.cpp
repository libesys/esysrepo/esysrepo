/*!
 * \file esys/repo/git/note_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/note.h"

namespace esys::repo::git
{

Note::Note() = default;

Note::~Note() = default;

void Note::set_message(const std::string &message)
{
    m_message = message;
}

const std::string &Note::get_message() const
{
    return m_message;
}

void Note::set_author(const Signature &author)
{
    m_author = author;
}

const Signature &Note::get_author() const
{
    return m_author;
}

Signature &Note::get_author()
{
    return m_author;
}

void Note::set_committer(const Signature &committer)
{
    m_committer = committer;
}

const Signature &Note::get_committer() const
{
    return m_committer;
}

Signature &Note::get_committer()
{
    return m_committer;
}

void Note::set_reference(const std::string &reference)
{
    m_reference = reference;
}

const std::string &Note::get_reference() const
{
    return m_reference;
}

bool Note::operator==(const Note &other) const
{
    if (get_message() != other.get_message()) return false;
    if (get_author() != other.get_author()) return false;
    if (get_committer() != other.get_committer()) return false;
    if (get_reference() != other.get_reference()) return false;
    return true;
}

bool Note::operator!=(const Note &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git
