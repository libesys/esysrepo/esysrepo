/*!
 * \file esys/repo/userconfigfileimpl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/userconfigfileimpl.h"

#include <esysfile/xml/attr.h>
#include <esysfile/xml/file.h>
#include <esysfile/xml/writer.h>

namespace esys::repo
{

UserConfigFileImpl::UserConfigFileImpl(UserConfigFile *self)
    : m_self(self)
{
}

UserConfigFileImpl::~UserConfigFileImpl() = default;

Result UserConfigFileImpl::read(const std::string &path)
{
    esysfile::xml::File xml_file;

    xml_file.set_root_node_name("esysrepo_user_config");
    int result = xml_file.read(path);
    if (result < 0)
    {
        // self()->add_error(result, "XML parsing failed.");
        return ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_ERROR_PARSING_FILE, path);
    }

    if (self()->get_config() == nullptr)
        self()->set_config(std::make_shared<UserConfig>());
    else
        self()->get_config()->clear();

    return read(xml_file.get_data());
}

Result UserConfigFileImpl::write(const std::string &path)
{
    Result result = write_xml();
    if (result.error()) return ESYSREPO_RESULT(result);

    esysfile::xml::Writer writer;

    writer.set_indent(4);
    int result_int = writer.write(path, m_xml_data);
    if (result_int < 0) return ESYSREPO_RESULT(ResultCode::RAW_INT_ERROR, result_int);
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result UserConfigFileImpl::write_xml()
{
    if (self()->get_config() == nullptr) return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_DATA_NULLPTR);

    m_xml_data = std::make_shared<esysfile::xml::Data>();
    m_xml_data->set_root_node_name("esysrepo_user_config");

    auto result = write(m_xml_data, self()->get_config());

    return ESYSREPO_RESULT(result);
}

Result UserConfigFileImpl::write(std::shared_ptr<esysfile::xml::Element> parent_el,
                                 std::shared_ptr<UserConfig> user_config)
{
    auto paths_el = std::make_shared<esysfile::xml::Element>();
    parent_el->add_element(paths_el);

    paths_el->set_name("paths");

    auto path_el = paths_el->add_element("path");
    path_el->add_attr("name", "manifest_directories");
    path_el->add_attr("value", user_config->get_manifest_directories_path());

    path_el = paths_el->add_element("path");
    path_el->add_attr("name", "temp");
    path_el->add_attr("value", user_config->get_temp_path());

    path_el = paths_el->add_element("path");
    path_el->add_attr("name", "traces");
    path_el->add_attr("value", user_config->get_traces_path());

    if (user_config->get_manifest_directories() == nullptr)
    {
        auto manifest_directories = std::make_shared<manifest::Directories>();
        manifest_directories->add_default_libesys();
        user_config->set_manifest_directories(manifest_directories);
    }

    auto result = write(parent_el, user_config->get_manifest_directories());
    return ESYSREPO_RESULT(result);
}

Result UserConfigFileImpl::write(std::shared_ptr<esysfile::xml::Element> parent_el,
                                 std::shared_ptr<manifest::Directories> manifest_directories)
{
    auto mds_el = parent_el->add_element("manifest_directories");

    for (auto md : manifest_directories->get_items())
    {
        auto md_el = mds_el->add_element("manifest_directory");
        md_el->add_attr("name", md->get_name());

        auto urls_el = md_el->add_element("urls");
        auto url_ssh_el = urls_el->add_element("url");
        url_ssh_el->add_attr("type", "ssh");
        url_ssh_el->add_attr("link", md->get_url_ssh());

        auto url_https_el = urls_el->add_element("url");
        url_https_el->add_attr("type", "https");
        url_https_el->add_attr("link", md->get_url_https());
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result UserConfigFileImpl::read(std::shared_ptr<esysfile::xml::Data> data)
{
    Result result;
    auto user_config = self()->get_config();
    if (user_config == nullptr) self()->set_config(std::make_shared<UserConfig>());

    for (auto el : data->get_elements())
    {
        if (el->get_name() == "paths")
            result = read_paths(el);
        else if (el->get_name() == "manifest_directories")
            result = read_manifest_directories(el);
        else
            result = ESYSREPO_RESULT(ResultCode::DIRECTORY_XML_ELEMENT_UNKNOWN, el->get_name());
        if (result.error())
        {
            return result;
        }
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result UserConfigFileImpl::read_manifest_directories(std::shared_ptr<esysfile::xml::Element> parent_el)
{
    if (parent_el->get_name() != "manifest_directories")
        return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_ELEMENT_UNKNOWN, parent_el->get_name());

    if (self()->get_config()->get_manifest_directories() == nullptr)
        self()->get_config()->set_manifest_directories(std::make_shared<manifest::Directories>());

    for (auto md_el : parent_el->get_elements())
    {
        if (md_el->get_name() != "manifest_directory")
            return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_ELEMENT_UNKNOWN, md_el->get_name());

        auto attr_name = md_el->get_attr("name");
        auto directories_item = std::make_shared<manifest::Directories::Item>();

        directories_item->set_name(attr_name->get_value());

        auto urls_el = md_el->get_element("urls");
        if (urls_el == nullptr) return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_MISSING_ELEMENT, "urls");
        for (auto url_el : urls_el->get_elements())
        {
            if (url_el->get_name() != "url")
                return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_MISSING_ELEMENT, url_el->get_name());
            auto attr_type = url_el->get_attr("type");
            if (attr_type == nullptr) return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_MISSING_ATTR, "type");
            auto attr_link = url_el->get_attr("link");
            if (attr_link == nullptr) return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_MISSING_ATTR, "link");

            if (attr_type->get_value() == "ssh")
                directories_item->set_url_ssh(attr_link->get_value());
            else if (attr_type->get_value() == "https")
                directories_item->set_url_https(attr_link->get_value());
            else
                return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_UNKNOWN_ATTR_VALUE,
                                       "type '" + attr_type->get_value() + "'");
        }
        self()->get_config()->get_manifest_directories()->add_item(attr_name->get_value(), directories_item);
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result UserConfigFileImpl::read_paths(std::shared_ptr<esysfile::xml::Element> parent_el)
{
    if (parent_el->get_name() != "paths")
        return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_ELEMENT_UNKNOWN, parent_el->get_name());

    for (auto path_el : parent_el->get_elements())
    {
        if (path_el->get_name() != "path")
            return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_ELEMENT_UNKNOWN, path_el->get_name());

        auto attr_name = path_el->get_attr("name");
        auto attr_link = path_el->get_attr("value");

        if (attr_name == nullptr) return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_MISSING_ATTR, "name");
        if (attr_link == nullptr) return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_MISSING_ATTR, "link");

        if (attr_name->get_value() == "manifest_directories")
            self()->get_config()->set_manifest_directories_path(attr_link->get_value());
        else if (attr_name->get_value() == "temp")
            self()->get_config()->set_temp_path(attr_link->get_value());
        else if (attr_name->get_value() == "traces")
            self()->get_config()->set_traces_path(attr_link->get_value());
        else
            return ESYSREPO_RESULT(ResultCode::USERCONFIG_XML_UNKNOWN_ATTR_VALUE, attr_name->get_value());
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

UserConfigFile *UserConfigFileImpl::self()
{
    return m_self;
}

const UserConfigFile *UserConfigFileImpl::self() const
{
    return m_self;
}

} // namespace esys::repo
