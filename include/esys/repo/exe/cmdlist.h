/*!
 * \file esys/repo/exe/cmdlist.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/exe/cmd.h"
#include "esys/repo/configfolder.h"
#include "esys/repo/gitbase.h"

#include <memory>
#include <ostream>

namespace esys::repo::exe
{

class ESYSREPO_API CmdList : public Cmd
{
public:
    CmdList();
    ~CmdList() override;

    void set_fullpath(bool fullpath);
    bool get_fullpath() const;

    void set_name_only(bool name_only);
    bool get_name_only() const;

    void set_path_only(bool name_only);
    bool get_path_only() const;

    void print_info(std::ostream &os, std::shared_ptr<manifest::Repository> repo);

protected:
    Result impl_run() override;

private:
    bool m_fullpath = false;
    bool m_name_only = false;
    bool m_path_only = false;
};

} // namespace exe
