/*!
 * \file esys/repo/git/resettype.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <iostream>

//<swig_inc/>

namespace esys::repo::git
{

enum class ResetType
{
    NOT_SET,
    SOFT,
    MIXED,
    HARD
};

} // namespace esys::repo::git

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::ResetType &reset_type);

} // namespace std
