/*!
 * \file esys/repo/git/resettype_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/resettype.h"

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::ResetType &reset_type)
{
    switch (reset_type)
    {
        case esys::repo::git::ResetType::NOT_SET: os << "NOT_SET"; break;
        case esys::repo::git::ResetType::SOFT: os << "SOFT"; break;
        case esys::repo::git::ResetType::MIXED: os << "MIXED"; break;
        case esys::repo::git::ResetType::HARD: os << "HARD"; break;
        default: os << "<unknown>";
    }
    return os;
}

} // namespace std
