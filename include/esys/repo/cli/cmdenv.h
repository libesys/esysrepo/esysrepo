/*!
 * \file esys/repo/cli/cmdenv.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/cli/cmd_t.h"
#include "esys/repo/exe/cmdenv.h"

namespace esys::repo::cli
{

class ESYSREPO_API CmdEnv : public Cmd_t<exe::CmdEnv>
{
public:
    using BaseType = Cmd_t<exe::CmdEnv>;

    explicit CmdEnv(AppBase *app = nullptr);
    ~CmdEnv() override;

    int configure_cmd(CmdType &cmd) override;
    std::shared_ptr<po::options_description> get_desc() override;

    int print_doc(std::ostream &os) override;
};

} // namespace esys::repo::cli
