/*!
 * \file esys/repo/git/commit.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/commithash.h"
#include "esys/repo/git/note.h"
#include "esys/repo/git/signature.h"

#include <chrono>
#include <map>
#include <memory>
#include <string>
#include <vector>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class Commit esys/repo/git/commit.h "esys/repo/git/commit.h"
 * \brief Hold information about a commit
 */
class ESYSREPO_API Commit
{
public:
    //! Default constructor
    Commit();

    //! Destructor
    virtual ~Commit();

    void clear();

    //! Set the commit hash
    /*!
     * \param[in] hash the commit hash
     */
    void set_hash(const std::string &hash);

    //! Get the commit hash
    /*!
     * \return the commit hash
     */
    const CommitHash &get_hash() const; //<swig_out/>

    //! Get the commit hash
    /*!
     * \return the commit hash
     */
    CommitHash &get_hash();

    //! Set the message
    /*!
     * \param[in] message the commit message
     */
    void set_message(const std::string &message);

    //! Get the message
    /*!
     * \return the commit message
     */
    const std::string &get_message() const;

    //! Set the summary
    /*!
     * \param[in] summary the commit summary
     */
    void set_summary(const std::string &summary);

    //! Get the summary
    /*!
     * \return the commit summary
     */
    const std::string &get_summary() const;

    //! Set the body
    /*!
     * \param[in] body the commit body
     */
    void set_body(const std::string &body);

    //! Get the body
    /*!
     * \return the commit body
     */
    const std::string &get_body() const;

    //! Set the author
    /*!
     * \param[in] author the commit author
     */
    void set_author(const std::string &author);

    //! Get the author
    /*!
     * \return the commit author
     */
    const std::string &get_author() const;

    //! Set the email
    /*!
     * \param[in] email the commit email
     */
    void set_email(const std::string &email);

    //! Get the email
    /*!
     * \return the commit email
     */
    const std::string &get_email() const;

    //! Set the date and time
    /*!
     * \param[in] date_time the date and time
     */
    void set_date_time(const std::chrono::system_clock::time_point &date_time);

    //! Get the date and time
    /*!
     * \return the date and time
     */
    const std::chrono::system_clock::time_point &get_date_time() const;

    //! Get the all the notes
    /*!
     * \return the all the notes
     */
    std::vector<std::shared_ptr<Note>> &get_all_notes();

    //! Get the all the notes
    /*!
     * \return the all the notes
     */
    const std::vector<std::shared_ptr<Note>> &get_all_notes() const;

    //! Get the all the notes for a given notes reference
    /*!
     * \param[in] reference the notes reference
     * \param[out] notes the notes retrieved
     * \return 0 if successfull, < 0 otherwise
     */
    int get_notes(const std::string &reference, std::vector<std::shared_ptr<Note>> &notes) const;

    //! Add a note
    /*!
     * \param[in] note the note
     */
    void add_note(std::shared_ptr<Note> note);

    //! Get the author signature
    /*!
     * \return the author signature
     */
    Signature &get_author_sign();

    //! Get the author signature
    /*!
     * \return the author signature
     */
    const Signature &get_author_sign() const;

    //! Get the author committer
    /*!
     * \return the author committer
     */
    Signature &get_committer_sign();

    //! Get the author committer
    /*!
     * \return the author committer
     */
    const Signature &get_committer_sign() const;

    //! Equal to comparison operator
    bool operator==(const Commit &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Commit &other) const;

private:
    //!< \cond DOXY_IMPL

    using NoteVec = std::vector<std::shared_ptr<Note>>;

    CommitHash m_hash;                                           //!< The commit hash
    std::string m_message;                                       //!< The message
    std::string m_summary;                                       //!< The sumary
    std::string m_body;                                          //!< The body
    Signature m_author_sign;                                     //!< The author signature
    Signature m_committer_sign;                                  //!< The committer signature
    NoteVec m_all_notes;                                         //!< All the notes of the commit
    std::map<std::string, NoteVec, std::less<>> m_map_ref_notes; //!< Map between references and notes
    //!< \endcond
};

} // namespace esys::repo::git

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::Commit &commit);

} // namespace std
