/*!
 * \file esys/repo/libgit2/gitimpl_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/libgit2/gitimpl.h"
#include "esys/repo/libgit2/guard.h"
#include "esys/repo/libgit2/guards.h"
#include "esys/repo/git/updatetip.h"
#include "esys/repo/libssh2/ssh.h"

#include <esys/trace/call.h>
#include <esys/trace/macros.h>

#include <git2.h>
#include <libssh2.h>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <cstring>
#include <sstream>
#include <cassert>
#include <algorithm>

#include <iostream>

namespace esys::repo::libgit2
{

std::unique_ptr<LibGit2> GitImpl::s_libgt2 = nullptr;

GitImpl::GitImpl(Git *self)
    : m_self(self)
    , m_notes(self)
{
    if (s_libgt2 == nullptr) s_libgt2 = std::make_unique<LibGit2>();
}

GitImpl::~GitImpl()
{
    if (m_repo != nullptr) close_on_error();
}

Result GitImpl::open(const std::string &folder)
{
    Result result;
    ETRC_CALL_RET(result, folder);

    self()->open_time();

    int result_int = git_repository_open(&m_repo, folder.c_str());
    if (result_int < 0)
    {
        result = ESYSREPO_RESULT(ResultCode::GIT_ERROR_OPENING_REPO, "open git repo " + folder);
        return ETRC_RET(ESYSREPO_RESULT(check_error(result)));
    }

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

bool GitImpl::is_open()
{
    return (m_repo != nullptr);
}

Result GitImpl::init_bare(const std::string &folder_path)
{
    Result result;
    ETRC_CALL_RET(result, folder_path);

    if (is_open()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_ALREADY_OPENED));

    git_repository_init_options initopts = GIT_REPOSITORY_INIT_OPTIONS_INIT;
    initopts.flags = GIT_REPOSITORY_INIT_MKPATH;
    initopts.flags |= GIT_REPOSITORY_INIT_BARE;

    int result_int = git_repository_init_ext(&m_repo, folder_path.c_str(), &initopts);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::close()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_REPO_NOT_OPEN));

    git_repository_free(m_repo);

    m_repo = nullptr;

    self()->close_time();

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

void GitImpl::close_on_error()
{
    ETRC_CALL_NP();
    self()->close_time();
    if (m_repo == nullptr) return;

    git_repository_free(m_repo);
    m_repo = nullptr;
}

Result GitImpl::get_remotes(std::vector<git::Remote> &remotes)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, remotes);
    ETRC_CALL_RET_OUT_END(remotes);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    self()->cmd_start();

    remotes.clear();
    GuardS<git_strarray> data;

    int result_int = git_remote_list(data.get(), m_repo);
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    char **p = data.get()->strings;

    if (data.get()->count == 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));

    if (p == nullptr) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_GENERIC_ERROR, "No data")));

    for (auto idx = 0; idx < data.get()->count; ++idx)
    {
        char *name = *p;
        if (name == nullptr) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_GENERIC_ERROR, "No name")));

        git::Remote remote;

        remote.set_name(name);

        remotes.push_back(remote);
        p += 1;
    }

    for (auto &remote_item : remotes)
    {
        Guard<git_remote> remote; // This will automically release the git_remote

        const git_remote_head **refs = nullptr;
        size_t refs_len = 0;
        size_t i = 0;
        git_remote_callbacks callbacks = GIT_REMOTE_CALLBACKS_INIT;

        // Find the remote by name
        result_int = git_remote_lookup(remote.get_p(), m_repo, remote_item.get_name().c_str());
        if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

        const char *url = git_remote_url(remote.get());
        remote_item.set_url(url);

        /*result = git_remote_ls(&refs, &refs_len, remote.get());
        if (result < 0) return check_error(result, ""); */
    }

    return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
}

Result GitImpl::get_head_branch_remote(git::Branch &branch, git::Remote &remote)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, branch, remote);
    ETRC_CALL_RET_OUT_END(branch, remote);

    if (!is_open()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_REPO_NOT_OPEN));

    Guard<git_reference> head_ref;

    auto result_int = git_repository_head(head_ref.get_p(), m_repo);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));
    auto head_ref_name = git_reference_name(head_ref.get());
    auto head_ref_short = git_reference_shorthand(head_ref.get());

    branch.set_is_head(true);
    branch.set_ref_name(head_ref_name);
    branch.set_name(head_ref_short);
    branch.set_type(git::BranchType::LOCAL);

    result_int = git_repository_head_detached(m_repo);
    if (result_int == 1)
        branch.set_detached(true);
    else if (result_int == 0)
        branch.set_detached(false);
    else if (result_int < 0)
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    git_buf remote_name_buf = {nullptr};
    result_int = git_branch_upstream_remote(&remote_name_buf, m_repo, head_ref_name);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    std::string remote_name(remote_name_buf.ptr, remote_name_buf.ptr + remote_name_buf.size);
    git_buf_dispose(&remote_name_buf);

    branch.set_remote_name(remote_name);
    remote.set_name(remote_name);

    Guard<git_remote> remote_git;
    result = find_remote(remote_git, remote_name.c_str());
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    std::string remote_url = git_remote_url(remote_git.get());
    remote.set_url(remote_url);

    Guard<git_reference> upstream_ref;
    result_int = git_branch_upstream(upstream_ref.get_p(), head_ref.get());
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    auto up_ref_name = git_reference_name(upstream_ref.get());
    branch.set_remote_branch(up_ref_name);
    auto up_ref_short = git_reference_shorthand(upstream_ref.get());
    branch.set_remote_branch_name(up_ref_short);
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::add_remote(const std::string &name, const std::string &url)
{
    Result result;
    ETRC_CALL_RET(result, name, url);

    if (!is_open()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_REPO_NOT_OPEN));

    self()->cmd_start();

    Guard<git_remote> remote;

    int result_int = git_remote_create(remote.get_p(), m_repo, name.c_str(), url.c_str());
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
}

Result GitImpl::get_branches(git::Branches &branches, git::BranchType branch_type)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, branches, branch_type);
    ETRC_CALL_RET_OUT_END(branches);

    if (!is_open()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_REPO_NOT_OPEN));

    self()->cmd_start();

    Guard<git_branch_iterator> branch_it;
    git_branch_t list_flags = GIT_BRANCH_ALL;

    convert(branch_type, list_flags);

    int result_int = git_branch_iterator_new(branch_it.get_p(), m_repo, list_flags);
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    while (true)
    {
        Guard<git_reference> ref;
        git_branch_t git_branch_type = GIT_BRANCH_ALL;

        result_int = git_branch_next(ref.get_p(), &git_branch_type, branch_it.get());
        if (result_int == GIT_ITEROVER) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
        if (result_int < 0) ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

        std::shared_ptr<git::Branch> branch = std::make_shared<git::Branch>();
        const char *branch_name = nullptr;
        result_int = git_branch_name(&branch_name, ref.get());
        if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

        std::string ref_name = git_reference_name(ref.get());

        branch->set_name(branch_name);
        branch->set_ref_name(ref_name);

        switch (git_branch_type)
        {
            case GIT_BRANCH_ALL: branch_type = git::BranchType::ALL; break;
            case GIT_BRANCH_LOCAL: branch_type = git::BranchType::LOCAL; break;
            case GIT_BRANCH_REMOTE: branch_type = git::BranchType::REMOTE; break;
            default: branch_type = git::BranchType::NOT_SET;
        }

        branch->set_type(branch_type);

        result_int = git_branch_is_head(ref.get());
        if (result_int == 1) branch->set_is_head(true);

        if (branch_type == git::BranchType::LOCAL)
        {
            git_buf buf_out = {nullptr};
            result_int = git_branch_upstream_remote(&buf_out, m_repo, ref_name.c_str());
            if (result_int == 0)
            {
                std::string remote_name(buf_out.ptr, buf_out.ptr + buf_out.size);
                branch->set_remote_name(remote_name);
                git_buf_dispose(&buf_out);
            }
            result_int = git_branch_upstream_name(&buf_out, m_repo, ref_name.c_str());
            if (result_int == 0)
            {
                std::string remote_branch(buf_out.ptr, buf_out.ptr + buf_out.size);
                branch->set_remote_branch(remote_branch);
                git_buf_dispose(&buf_out);
            }
        }
        else
            check_error(result, ""); //! \TODO why??
        branches.add(branch);
    }
    return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
}

Result_t<bool> GitImpl::has_branch(const std::string &name, git::BranchType branch_type)
{
    Result_t<bool> result;
    ETRC_CALL_RET(result, name, branch_type);

    Guard<git_reference> ref;
    git_branch_t git_branch_type = GIT_BRANCH_ALL;
    Guard<git_annotated_commit> annotated_commit;
    // Guard<git_reference> branch_ref;
    Guard<git_reference> input_branch_ref;
    std::string new_ref = name;

    result = resolve_ref(input_branch_ref.get_p(), annotated_commit.get_p(), name);
    if (result.error())
    {
        self()->debug(0, "branch not found : " + name);
        // In the case where the content of branch doesn't have the full qualifier,
        // try to to guess

        result = find_ref(input_branch_ref.get_p(), annotated_commit.get_p(), name, new_ref);
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT_T(result, false));
    }

    return ETRC_RET(ESYSREPO_RESULT_T(result, true));
}

Result GitImpl::get_hash(const std::string &revision, std::string &hash, git::BranchType branch_type)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, revision, hash, branch_type);
    ETRC_CALL_RET_OUT_END(hash);

    Guard<git_annotated_commit> annotated_commit;
    Guard<git_reference> revision_ref;
    std::string new_ref = revision;

    result = resolve_ref(revision_ref.get_p(), annotated_commit.get_p(), revision);
    if (result.error())
    {
        self()->debug(0, "revision not found : " + revision);
        // In the case where the content of branch doesn't have the full qualifier,
        // try to to guess

        result = find_ref(revision_ref.get_p(), annotated_commit.get_p(), revision, new_ref);
        if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));
    }

    const git_oid *oid = git_annotated_commit_id(annotated_commit.get());
    if (oid == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_GET_HASH_FAILED));

    result = convert_bin_hex(*oid, hash);
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitImpl::treeish_to_tree(Guard<git_tree> &tree, git_repository *repo, const char *treeish)
{
    Guard<git_object> obj;

    int result = git_revparse_single(obj.get_p(), repo, treeish);
    if (result < 0) return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result);

    result = git_object_peel((git_object **)tree.get_p(), obj.get(), GIT_OBJECT_TREE);
    if (result < 0) return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result);
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::walk_commits(std::shared_ptr<git::WalkCommit> walk_commit)
{
    Result result;
    ETRC_CALL_RET_NP(result);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));
    if (walk_commit == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    git::CommitHash hash;
    Result rresult = get_last_commit(hash);
    if (rresult.error()) return ETRC_RET(ESYSREPO_RESULT(rresult));

    git_oid oid;

    rresult = convert_hex_bin(hash.get_hash(), oid);
    if (rresult.error()) return ETRC_RET(ESYSREPO_RESULT(rresult));

    Guard<git_revwalk> walker;
    Guard<git_commit> commit;

    git_revwalk_new(walker.get_p(), m_repo);
    git_revwalk_sorting(walker.get(), GIT_SORT_TOPOLOGICAL);
    git_revwalk_push(walker.get(), &oid);

    const char *commit_message;
    const git_signature *commit_signature;
    git_time_t commit_time;
    int result_int = 0;

    while (git_revwalk_next(&oid, walker.get()) == 0)
    {
        result_int = git_commit_lookup(commit.get_p(), m_repo, &oid);
        if (result_int != 0)
        {
            return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));
        }

        commit_message = git_commit_message(commit.get());
        commit_signature = git_commit_committer(commit.get());
        commit_time = git_commit_time(commit.get());

        std::time_t commit_time_t = static_cast<std::time_t>(commit_time);
        std::string commit_time_str = std::asctime(std::localtime(&commit_time_t));

        auto commit_info = std::make_shared<git::Commit>();

        commit_info->set_message(commit_message);
        commit_info->set_author(commit_signature->name);
        commit_info->set_email(commit_signature->email);
        commit_info->set_date_time(std::chrono::system_clock::from_time_t(commit_time_t));

        rresult = convert_bin_hex(oid, commit_info->get_hash().get_hash());
        if (rresult.error()) return ETRC_RET(ESYSREPO_RESULT(rresult));

        walk_commit->new_commit(self(), commit_info);
        commit.reset();
    }

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::diff(const git::CommitHash &commit_hash, std::shared_ptr<git::Diff> diff)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, commit_hash, diff);
    ETRC_CALL_RET_OUT_END(diff);

    Guard<git_tree> commit_tree;
    Guard<git_tree> parent_tree;
    bool has_parent = true;

    result = treeish_to_tree(commit_tree, m_repo, commit_hash.get_hash().c_str());
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    git::CommitHash parent_hash;
    result = get_parent_commit(commit_hash, parent_hash);
    if (result.error())
        has_parent = false;
    else
    {
        result = treeish_to_tree(parent_tree, m_repo, parent_hash.get_hash().c_str());
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));
    }

    Guard<git_diff> the_diff;
    git_diff_options diffopts;

    int result_int = git_diff_options_init(&diffopts, GIT_DIFF_OPTIONS_VERSION);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    // diffopts.id_abbrev = 40;

    if (has_parent)
        result_int = git_diff_tree_to_tree(the_diff.get_p(), m_repo, parent_tree.get(), commit_tree.get(), &diffopts);
    else
        result_int = git_diff_tree_to_tree(the_diff.get_p(), m_repo, nullptr, commit_tree.get(), &diffopts);

    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    Guard<git_diff_stats> diff_stats;

    result_int = git_diff_get_stats(diff_stats.get_p(), the_diff.get());
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    /*diff_file_stats *filestats;

    size_t files_changed;
    size_t insertions;
    size_t deletions;
    size_t renames;

    size_t max_name;
    size_t max_filestat;
    int max_digits; */
    diff->set_files_changed(static_cast<unsigned int>(git_diff_stats_files_changed(diff_stats.get())));
    diff->set_insertions(static_cast<unsigned int>(git_diff_stats_insertions(diff_stats.get())));
    diff->set_deletions(static_cast<unsigned int>(git_diff_stats_deletions(diff_stats.get())));
    diff->set_renames(static_cast<unsigned int>(git_diff_stats_renames(diff_stats.get())));
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::get_ahead_behind(git::AheadBehind &ahead_behind, const git::Branch &local_branch)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, ahead_behind, local_branch);
    ETRC_CALL_RET_OUT_END(ahead_behind);

    size_t ahead = 0;
    size_t behind = 0;
    git_oid local;
    git_oid upstream;

    ahead_behind.set_ahead(0);
    ahead_behind.set_behind(0);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));
    if (local_branch.get_remote_branch().empty()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    result = get_commit_hash_from_branch(local_branch.get_name(), local);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    result = get_commit_hash_from_branch(local_branch.get_remote_branch(), upstream);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    int result_int = git_graph_ahead_behind(&ahead, &behind, m_repo, &local, &upstream);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR));

    ahead_behind.set_ahead(ahead);
    ahead_behind.set_behind(behind);
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::get_ahead_behind(git::AheadBehind &ahead_behind, const std::string &first_ref,
                                 const std::string &second_ref)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, ahead_behind, first_ref, second_ref);
    ETRC_CALL_RET_OUT_END(ahead_behind);

    size_t ahead = 0;
    size_t behind = 0;
    git_oid first_ref_oid;
    git_oid second_ref_oid;

    ahead_behind.set_ahead(0);
    ahead_behind.set_behind(0);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    result = get_commit_hash_from_branch(first_ref, first_ref_oid);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    result = get_commit_hash_from_branch(second_ref, second_ref_oid);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    int result_int = git_graph_ahead_behind(&ahead, &behind, m_repo, &first_ref_oid, &second_ref_oid);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR));

    ahead_behind.set_ahead(ahead);
    ahead_behind.set_behind(behind);
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::clone(const std::string &url, const std::string &path, const std::string &branch)
{
    Result rresult;
    ETRC_CALL_RET(rresult, url, path, branch);

    std::string new_ref;
    int result = 0;

    self()->debug(1, "[GitImpl::clone] begin ...");

    if (url.find("https:") == 0)
    {
        self()->debug(1, "[GitImpl::clone] https");
        self()->cmd_start();
        self()->open_time();

        if (branch.empty())
            result = git_clone(&m_repo, url.c_str(), path.c_str(), nullptr);
        else
        {
            git_clone_options opts = GIT_CLONE_OPTIONS_INIT;
            opts.checkout_branch = new_ref.c_str();

            result = git_clone(&m_repo, url.c_str(), path.c_str(), &opts);
        }
    }
    else if ((url.find("ssh:") == 0))
    {
        self()->debug(1, "[GitImpl::clone] ssh");
        if (!is_ssh_agent_running())
        {
            rresult = ESYSREPO_RESULT(ResultCode::GIT_GENERIC_ERROR, "Only authentication via an ssh agent is "
                                                                     "supported");
            return ETRC_RET(check_error(rresult, false));
        }

        self()->cmd_start();
        self()->open_time();

        git_clone_options opts = GIT_CLONE_OPTIONS_INIT;
        if (!branch.empty()) opts.checkout_branch = new_ref.c_str();
        // opts.fetch_opts.callbacks.connect = &Remote::Callbacks::connect;
        // opts.fetch_opts.callbacks.disconnect = &Remote::Callbacks::disconnect;
        opts.fetch_opts.callbacks.sideband_progress = &GitImpl::libgit2_sideband_progress_cb;
        opts.fetch_opts.callbacks.credentials = &GitImpl::libgit2_credentials_cb;
        // opts.fetch_opts.callbacks.certificate_check = &Remote::Callbacks::certificate;
        opts.fetch_opts.callbacks.transfer_progress = &GitImpl::libgit2_transfer_progress_cb;
        opts.fetch_opts.callbacks.update_tips = &GitImpl::libgit2_update_tips_cb;
        opts.fetch_opts.callbacks.pack_progress = &GitImpl::libgit2_pack_progress_cb;
        // opts.fetch_opts.callbacks.resolve_url = &Remote::Callbacks::url;
        opts.fetch_opts.callbacks.payload = this;
        // opts.bare = bare;

        result = git_clone(&m_repo, url.c_str(), path.c_str(), &opts);
    }
    else
    {
        self()->debug(1, "[GitImpl::clone] no protocol");
        self()->cmd_start();
        self()->open_time();

        if (branch.empty())
            result = git_clone(&m_repo, url.c_str(), path.c_str(), nullptr);
        else
        {
            git_clone_options opts = GIT_CLONE_OPTIONS_INIT;
            opts.checkout_branch = new_ref.c_str();

            result = git_clone(&m_repo, url.c_str(), path.c_str(), &opts);
        }
    }

    if (result == 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
    rresult = ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result, "Clone failed");
    return ETRC_RET(check_error(rresult));
}

Result GitImpl::checkout(const std::string &branch, bool force)
{
    Result result;
    ETRC_CALL_RET(result, branch, force);

    Guard<git_annotated_commit> annotated_commit;
    Guard<git_object> treeish;
    Guard<git_commit> target_commit;
    Guard<git_reference> ref;
    Guard<git_reference> branch_ref;
    Guard<git_reference> input_branch_ref;
    std::string new_ref = branch;

    self()->cmd_start();

    result = resolve_ref(input_branch_ref.get_p(), annotated_commit.get_p(), branch);
    if (result.error())
    {
        self()->debug(0, "branch not found : " + branch);
        // In the case where the content of branch doesn't have the full qualifier,
        // try to to guess

        result = find_ref(input_branch_ref.get_p(), annotated_commit.get_p(), branch, new_ref);
        if (result.error())
        {
            return ETRC_RET(check_error(ESYSREPO_RESULT(result)));
        }
    }

    int result_int = git_commit_lookup(target_commit.get_p(), m_repo, git_annotated_commit_id(annotated_commit.get()));
    const git_oid *oid = git_commit_id(target_commit.get());
    std::string oid_str;
    result = convert_bin_hex(*oid, oid_str);

    git_checkout_options opts = GIT_CHECKOUT_OPTIONS_INIT;
    if (force)
        opts.checkout_strategy = GIT_CHECKOUT_FORCE;
    else
        opts.checkout_strategy = GIT_CHECKOUT_SAFE;

    auto t_commit = static_cast<git_object *>(static_cast<void *>(target_commit.get()));
    result_int = git_checkout_tree(m_repo, t_commit, &opts);
    if (result_int < 0)
    {
        return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));
    }

    if (git_annotated_commit_ref(annotated_commit.get()))
    {
        const char *target_head = nullptr;

        result_int = git_reference_lookup(ref.get_p(), m_repo, git_annotated_commit_ref(annotated_commit.get()));
        if (result_int < 0)
        {
            return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));
        }

        if (git_reference_is_remote(ref.get()))
        {
            result_int =
                git_branch_create_from_annotated(branch_ref.get_p(), m_repo, branch.c_str(), annotated_commit.get(), 0);
            if (result_int < 0)
            {
                return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));
            }

            target_head = git_reference_name(branch_ref.get());

            // if (git_reference_is_branch(input_branch_ref.get()))
            // git_
            const char *branch_name = nullptr;
            result_int = git_branch_name(&branch_name, input_branch_ref.get());
            if (result_int < 0)
                check_error(result, "can't get the name of the branch"); //!  \TODO check if this is correct
            else
            {
                result_int = git_branch_set_upstream(branch_ref.get(), branch_name);
                if (result_int < 0) check_error(result, "set branch upstream"); //!  \TODO check if this is correct
            }
        }
        else
        {
            target_head = git_annotated_commit_ref(annotated_commit.get());
        }

        result_int = git_repository_set_head(m_repo, target_head);
        Result rresult;

        if (result_int < 0)
            rresult = ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int);
        else
            rresult = ESYSREPO_RESULT(ResultCode::OK);
        return ETRC_RET(check_error(rresult));
    }
    else
    {
        result_int = git_repository_set_head_detached_from_annotated(m_repo, annotated_commit.get());
        Result rresult;
        if (result_int < 0)
            rresult = ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int);
        else
            rresult = ESYSREPO_RESULT(ResultCode::OK);

        return ETRC_RET(check_error(rresult));
    }
}

Result GitImpl::reset(const git::CommitHash &commit, git::ResetType type)
{
    Result result;
    ETRC_CALL_RET(result, commit, type);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    git_reset_t reset_type = GIT_RESET_SOFT;

    switch (type)
    {
        case git::ResetType::NOT_SET: return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RESET_TYPE_NOT_SET));
        case git::ResetType::HARD: reset_type = GIT_RESET_HARD; break;
        case git::ResetType::MIXED: reset_type = GIT_RESET_MIXED; break;
        case git::ResetType::SOFT: reset_type = GIT_RESET_SOFT; break;
        default: return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RESET_TYPE_UNKNOWN));
    }

    Guard<git_commit> g_commit;
    git_oid oid_commit;

    result = convert_hex_bin(commit.get_hash(), oid_commit);
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    // get the actual commit structure
    int result_int = git_commit_lookup(g_commit.get_p(), m_repo, &oid_commit);
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    auto t_commit = static_cast<git_object *>(static_cast<void *>(g_commit.get()));
    result_int = git_reset(m_repo, t_commit, reset_type, nullptr);
    if (result_int == GIT_OK) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
    return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));
}

Result GitImpl::fastforward(const git::CommitHash &commit)
{
    Result result;
    ETRC_CALL_RET(result, commit);

    git_checkout_options ff_checkout_options = GIT_CHECKOUT_OPTIONS_INIT;
    Guard<git_reference> target_ref;
    Guard<git_reference> new_target_ref;
    Guard<git_object> target;
    git_oid target_oid;

    assert(m_repo != nullptr);

    result = convert_hex_bin(commit.get_hash(), target_oid);
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    // HEAD exists, just lookup and resolve
    int result_int = git_repository_head(target_ref.get_p(), m_repo);
    if (result_int != GIT_OK)
        return ETRC_RET(
            check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int, "failed to get HEAD reference")));

    // Lookup the target object
    result_int = git_object_lookup(target.get_p(), m_repo, &target_oid, GIT_OBJECT_COMMIT);
    if (result_int != GIT_OK)
        return ETRC_RET(check_error(
            ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int, "failed to lookup OID " + commit.get_hash())));

    // Checkout the result so the workdir is in the expected state
    ff_checkout_options.checkout_strategy = GIT_CHECKOUT_SAFE;
    result_int = git_checkout_tree(m_repo, target.get(), &ff_checkout_options);
    if (result_int != 0)
        return ETRC_RET(check_error(
            ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int, "failed to checkout HEAD reference")));

    // Move the target reference to the target OID
    result_int = git_reference_set_target(new_target_ref.get_p(), target_ref.get(), &target_oid, nullptr);
    if (result_int != 0)
        return ETRC_RET(
            check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int, "failed to move HEAD reference")));

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::get_commit_hash(const git_oid &oid_commit, git::CommitHash &commit_hash)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, commit_hash);
    ETRC_CALL_RET_OUT_END(commit_hash);

    std::string hash;

    result = convert_bin_hex(oid_commit, hash);
    if (result.ok())
    {
        commit_hash.set_hash(hash);
        return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
    }

    return ETRC_RET(check_error(ESYSREPO_RESULT(result)));
}

Result GitImpl::get_commit(const git_oid &oid_commit, git::Commit &commit, bool get_all_notes)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, commit, get_all_notes);
    ETRC_CALL_RET_OUT_END(commit);

    Guard<git_commit> g_commit;

    // get the actual commit structure
    auto result_int = git_commit_lookup(g_commit.get_p(), m_repo, &oid_commit);
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    result = get_commit_hash(oid_commit, commit.get_hash());
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    auto message = git_commit_message(g_commit.get());
    if (message != nullptr) commit.set_message(message);

    auto summary = git_commit_summary(g_commit.get());
    if (summary != nullptr) commit.set_summary(summary);

    auto body = git_commit_body(g_commit.get());
    if (body != nullptr) commit.set_body(body);

    auto author = git_commit_author(g_commit.get());
    result = get_signature(author, commit.get_author_sign());
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    auto committer = git_commit_committer(g_commit.get());
    result = get_signature(committer, commit.get_committer_sign());
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    if (!get_all_notes) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));

    result = m_notes.load_all();
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    std::vector<std::shared_ptr<git::Note>> notes;

    result = m_notes.get_note(oid_commit, notes);
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    for (auto note : notes)
    {
        commit.add_note(note);
    }

    return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
}

Result GitImpl::get_last_commit(git::CommitHash &commit_hash)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, commit_hash);
    ETRC_CALL_RET_OUT_END(commit_hash);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    git_oid oid_last_commit;

    self()->cmd_start();

    // resolve HEAD into a SHA1
    auto result_int = git_reference_name_to_id(&oid_last_commit, m_repo, "HEAD");
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    result = get_commit_hash(oid_last_commit, commit_hash);
    return ETRC_RET(check_error(ESYSREPO_RESULT(result)));
}

Result GitImpl::get_last_commit(git::Commit &commit, bool get_all_notes)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, commit, get_all_notes);
    ETRC_CALL_RET_OUT_END(commit);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    git_oid oid_last_commit;

    self()->cmd_start();

    commit.clear();

    // resolve HEAD into a SHA1
    auto result_int = git_reference_name_to_id(&oid_last_commit, m_repo, "HEAD");
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    result = get_commit(oid_last_commit, commit, get_all_notes);
    return ETRC_RET(check_error(ESYSREPO_RESULT(result)));
}

Result GitImpl::get_notes(git_commit *commit_obj, git::Commit &commit, const std::string &notes_ref)
{
    Guard<git_iterator> it;

    auto result_int = git_note_iterator_new(it.get_p(), m_repo, notes_ref.c_str());

    return check_error(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::get_signature(const git_signature *signature_obj, git::Signature &signature)
{
    signature.set_name(signature_obj->name);
    signature.set_email(signature_obj->email);

    git_time_t commit_time = signature_obj->when.time;

    std::time_t commit_time_t = static_cast<std::time_t>(commit_time);
    std::string commit_time_str = std::asctime(std::localtime(&commit_time_t));

    signature.set_date_time(std::chrono::system_clock::from_time_t(commit_time_t));
    signature.set_offset(signature_obj->when.offset);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::get_parent_commit(const git::CommitHash &commit, git::CommitHash &parent, int nth_parent)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, commit, parent, nth_parent);
    ETRC_CALL_RET_OUT_END(parent);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    Guard<git_commit> g_commit;
    Guard<git_commit> cur_commit;
    Guard<git_commit> parent_commit;
    git_oid oid_commit;

    self()->cmd_start();

    if (nth_parent == 0)
    {
        parent = commit;
        return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
    }

    result = convert_hex_bin(commit.get_hash(), oid_commit);
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    // get the actual commit structure
    int result_int = git_commit_lookup(cur_commit.get_p(), m_repo, &oid_commit);
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    unsigned int count = 0;

    for (int idx = 0; idx < nth_parent; ++idx)
    {
        count = git_commit_parentcount(cur_commit.get());
        if (count <= 0)
        {
            return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_NO_PARENT)));
        }

        parent_commit.reset();
        result_int = git_commit_parent(parent_commit.get_p(), cur_commit.get(), 0);
        if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

        cur_commit = parent_commit;
    }

    const git_oid *parent_commit_id = git_commit_id(parent_commit.get());
    if (parent_commit_id == nullptr) check_error(-3);

    std::string hash;
    result = convert_bin_hex(*parent_commit_id, hash);
    if (result.ok())
    {
        parent.set_hash(hash);
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
    }

    return ETRC_RET(check_error(ESYSREPO_RESULT(result)));
}

Result GitImpl::is_dirty(bool &dirty)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, dirty);
    ETRC_CALL_RET_OUT_END(dirty);

    if (m_repo == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    Guard<git_status_list> status;
    git_status_options statusopt = GIT_STATUS_OPTIONS_INIT;

    self()->cmd_start();

    statusopt.show = GIT_STATUS_SHOW_INDEX_AND_WORKDIR;
    statusopt.flags = GIT_STATUS_OPT_RENAMES_HEAD_TO_INDEX | GIT_STATUS_OPT_SORT_CASE_SENSITIVELY;

    int result_int = git_status_list_new(status.get_p(), m_repo, &statusopt);
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    std::size_t count = git_status_list_entrycount(status.get());
    dirty = (count != 0);
    return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
}

Result GitImpl::is_detached(bool &detached)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, detached);
    ETRC_CALL_RET_OUT_END(detached);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    int result_int = git_repository_head_detached(m_repo);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    if (result_int == 1)
        detached = true;
    else
        detached = false;
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::get_status(git::RepoStatus &repo_status)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, repo_status);
    ETRC_CALL_RET_OUT_END(repo_status);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    const git_status_entry *status_entry = nullptr;
    Guard<git_status_list> status_list;
    git_status_options statusopt = GIT_STATUS_OPTIONS_INIT;

    self()->cmd_start();

    statusopt.show = GIT_STATUS_SHOW_INDEX_AND_WORKDIR;
    statusopt.flags =
        GIT_STATUS_OPT_INCLUDE_UNTRACKED | GIT_STATUS_OPT_RENAMES_HEAD_TO_INDEX | GIT_STATUS_OPT_SORT_CASE_SENSITIVELY;

    int result_int = git_status_list_new(status_list.get_p(), m_repo, &statusopt);
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    std::size_t count = git_status_list_entrycount(status_list.get());

    for (std::size_t idx = 0; idx < count; ++idx)
    {
        status_entry = git_status_byindex(status_list.get(), idx);

        handle_status_entry(repo_status, status_entry);
    }
    return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::OK)));
}

Result GitImpl::handle_status_entry(git::RepoStatus &repo_status, const git_status_entry *status_entry)
{
    std::shared_ptr<git::Status> status = std::make_shared<git::Status>();
    Result result;

    if (status_entry->status == GIT_STATUS_CURRENT)
        result = handle_status_entry_current(repo_status, status, status_entry);
    else if (status_entry->status < GIT_STATUS_WT_NEW)
        result = handle_status_entry_index(repo_status, status, status_entry);
    else if (status_entry->status < GIT_STATUS_IGNORED)
        result = handle_status_entry_work_dir(repo_status, status, status_entry);
    else if (status_entry->status == GIT_STATUS_IGNORED)
        result = handle_status_entry_ignored(repo_status, status, status_entry);
    else if (status_entry->status == GIT_STATUS_CONFLICTED)
        result = handle_status_entry_conflicted(repo_status, status, status_entry);
    else
        return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    repo_status.add(status);
    return ESYSREPO_RESULT(result); // It was 0?!
}

Result GitImpl::handle_status_entry_current(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                            const git_status_entry *status_entry)
{
    status->set_type(git::StatusType::CURRENT);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::handle_status_entry_index(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                          const git_status_entry *status_entry)
{
    status->set_type(git::StatusType::INDEX);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::handle_status_entry_work_dir(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                             const git_status_entry *status_entry)
{
    status->set_type(git::StatusType::WORKING_DIR);

    Result result = from_to(status_entry->index_to_workdir, status->get_diff_delta());
    if (result.error()) return ESYSREPO_RESULT(result);

    result = from_to(status_entry->status, status);
    if (result.error()) return ESYSREPO_RESULT(result);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::handle_status_entry_conflicted(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                               const git_status_entry *status_entry)
{
    status->set_type(git::StatusType::CONFLICTED);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::handle_status_entry_ignored(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                            const git_status_entry *status_entry)
{
    status->set_type(git::StatusType::IGNORED);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::from_to(git_status_t status, std::shared_ptr<git::Status> status_ptr)
{
    switch (status)
    {
        case GIT_STATUS_INDEX_NEW:
        case GIT_STATUS_WT_NEW: status_ptr->set_sub_type(git::StatusSubType::NEW); break;

        case GIT_STATUS_INDEX_MODIFIED:
        case GIT_STATUS_WT_MODIFIED: status_ptr->set_sub_type(git::StatusSubType::MODIFIED); break;

        case GIT_STATUS_INDEX_DELETED:
        case GIT_STATUS_WT_DELETED: status_ptr->set_sub_type(git::StatusSubType::DELETED); break;

        case GIT_STATUS_INDEX_RENAMED:
        case GIT_STATUS_WT_RENAMED: status_ptr->set_sub_type(git::StatusSubType::RENAMED); break;

        case GIT_STATUS_INDEX_TYPECHANGE:
        case GIT_STATUS_WT_TYPECHANGE: status_ptr->set_sub_type(git::StatusSubType::TYPECHANGE); break;

        case GIT_STATUS_WT_UNREADABLE: status_ptr->set_sub_type(git::StatusSubType::UNREADABLE); break;

        default: return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::from_to(const git_diff_delta *delta, git::DiffDelta &diff_delta)
{
    diff_delta.set_file_count(delta->nfiles);
    diff_delta.set_similatiry(delta->similarity);

    Result result = from_to(delta->old_file, diff_delta.get_old_file());
    if (result.error()) return ESYSREPO_RESULT(result);

    result = from_to(delta->new_file, diff_delta.get_new_file());
    if (result.error()) return ESYSREPO_RESULT(result);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::from_to(const git_diff_file &file, git::DiffFile &diff_file)
{
    std::string id;
    Result result = convert_bin_hex(file.id, id);
    if (result.ok()) diff_file.set_id(id);

    diff_file.set_path(file.path);
    diff_file.set_size(file.size);

    git::FileMode mode = git::FileMode::NOT_SET;

    switch (file.mode)
    {
        case GIT_FILEMODE_UNREADABLE: mode = git ::FileMode::NEW; break;
        case GIT_FILEMODE_TREE: mode = git ::FileMode::TREE; break;
        case GIT_FILEMODE_BLOB: mode = git ::FileMode::BLOB; break;
        case GIT_FILEMODE_BLOB_EXECUTABLE: mode = git ::FileMode::BLOB_EXECUTABLE; break;
        case GIT_FILEMODE_LINK: mode = git ::FileMode::LINK; break;
        case GIT_FILEMODE_COMMIT: mode = git ::FileMode::COMMIT; break;
        default: mode = git::FileMode::NOT_SET;
    }

    diff_file.set_mode(mode);

    return result;
}

int GitImpl::check_error(int result, const std::string &action, bool show_result)
{
    self()->cmd_end();

    if (result == 0) return 0;

    const git_error *error = git_error_last();

    std::ostringstream oss;
    // oss << "ERROR " << result << " : " << action;
    oss << action;
    if (show_result)
    {
        oss << " (" << result << ").";
        if (error && error->message)
        {
            oss << " - " << error->message;
        }
    }
    self()->error(oss.str());

    return result;
}

Result GitImpl::check_error(Result result, bool show_result)
{
    self()->cmd_end();

    if (result.ok()) return result;

    const git_error *error = git_error_last();

    std::ostringstream oss;
    // oss << "ERROR " << result << " : " << action;
    if (result.get_error_info() != nullptr) oss << result.get_error_info()->get_text();
    if (show_result)
    {
        oss << " (" << result.get_result_code_int() << ").";
        if (error && error->message)
        {
            oss << " - " << error->message;
        }
    }
    self()->error(oss.str());

    return result;
}

Git *GitImpl::self() const
{
    return m_self;
}

int GitImpl::libgit2_credentials_cb(git_credential **out, const char *url, const char *name, unsigned int types,
                                    void *payload)
{
    const git_error *error = git_error_last();
    if (error && error->klass == GIT_ERROR_SSH) return -1;

    auto self = static_cast<GitImpl *>(payload);

    if (self->is_ssh_agent_running())
    {
        self->self()->debug(0, "[GitImpl::libgit2_credentials_cb] agent is running");

        std::string agent_id_path = libssh2::SSH::get_dflt_agent_identity_path();
        if (agent_id_path.empty())
            return git_credential_ssh_key_from_agent(out, name);
        else
        {
            std::ostringstream oss;

            oss << "[GitImpl::libgit2_credentials_cb] use custom agent with path '" << agent_id_path << "'";
            self->self()->debug(0, oss.str());
            return git_credential_ssh_key_from_custom_agent(out, name, agent_id_path.c_str());
        }
    }
    else
        self->self()->debug(0, "[GitImpl::libgit2_credentials_cb] NO agent is running");

    return -177;
}

int GitImpl::libgit2_sideband_progress_cb(const char *str, int len, void *data)
{
    auto impl = static_cast<GitImpl *>(data);

    if (impl == nullptr) return 0;

    std::string txt(str, str + len);

    int result = impl->self()->handle_sideband_progress(txt);
    return result;
}

int GitImpl::libgit2_transfer_progress_cb(const git_indexer_progress *stats, void *payload)
{
    auto impl = static_cast<GitImpl *>(payload);

    if (impl == nullptr) return 0;

    git::Progress progress;

    // std::cout << "[libgit2_transfer_progress_cb]" << std::endl;

    if (stats->received_objects == stats->total_objects)
    {
        progress.set_fetch_step(git::FetchStep::RESOLVING);

        if (stats->indexed_deltas == stats->total_deltas)
        {
            progress.set_done(true);
            progress.set_percentage(git::Progress::MAX_PERCENTAGE);
        }
        else
        {
            double percentage = (100.0 * stats->indexed_deltas) / stats->total_deltas;

            progress.set_done(false);
            progress.set_percentage(static_cast<int>(percentage));
        }
        // std::cout << "Resolving deltas " << stats->indexed_deltas << "/" << stats->total_deltas << std::endl;
    }
    else if (stats->total_objects > 0)
    {
        progress.set_fetch_step(git::FetchStep::RECEIVING);
        // std::cout << "Received " << stats->received_objects << "/" << stats->total_objects << " objects "
        //          << "(" << stats->indexed_objects << ") in " << stats->received_bytes << std::endl;
        if (stats->received_objects == stats->total_objects)
        {
            progress.set_done(true);
            progress.set_percentage(git::Progress::MAX_PERCENTAGE);
        }
        else
        {
            double percentage = (100.0 * stats->received_objects) / stats->total_objects;

            progress.set_done(false);
            progress.set_percentage(static_cast<int>(percentage));
        }
    }

    if (impl->self()->get_progress_callback() != nullptr)
        impl->self()->get_progress_callback()->git_progress_notif(progress);

    return 0;
}

int GitImpl::libgit2_update_tips_cb(const char *refname, const git_oid *a, const git_oid *b, void *data)
{
    std::string a_str;
    std::string b_str;
    auto self = static_cast<GitImpl *>(data);
    std::ostringstream oss;
    git::UpdateTip update_tip;

    auto result = self->convert_bin_hex(*b, b_str);
    if (result.error()) return -1;

    update_tip.set_ref_name(refname);

    if (git_oid_is_zero(a))
    {
        // oss << "[new]     " << b_str << " " << refname;
        // std::cout << oss.str() << std::endl;
        update_tip.set_type(git::UpdateTipType::NEW);
        update_tip.set_new_oid(b_str);
    }
    else
    {
        update_tip.set_type(git::UpdateTipType::UPDATE);
        result = self->convert_bin_hex(*a, a_str);
        if (result.error()) return -1;

        update_tip.set_new_oid(b_str);
        update_tip.set_new_oid(b_str);
        // oss << "[updated] " << a_str << ".." << b_str << " " << refname;
        // std::cout << oss.str() << std::endl;
    }
    return 0;
}

int GitImpl::libgit2_pack_progress_cb(int stage, uint32_t current, uint32_t total, void *payload)
{
    std::cout << "[libgit2_pack_progress_cb]" << std::endl;
    return 0;
}

Result_t<bool> GitImpl::is_ssh_agent_running()
{
    bool present = m_ssh.is_agent_present();

    if (present) self()->debug(0, "SSH agent detected");

    return present;
}

Result GitImpl::merge_analysis(const std::vector<std::string> &refs, git::MergeAnalysisResult &merge_analysis_result,
                               std::vector<git::CommitHash> &commits)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, refs, merge_analysis_result, commits);
    ETRC_CALL_RET_OUT_END(merge_analysis_result, commits);

    git_repository_state_t state = GIT_REPOSITORY_STATE_NONE;
    git_merge_analysis_t analysis = GIT_MERGE_ANALYSIS_NONE;
    git_merge_preference_t preference = GIT_MERGE_PREFERENCE_NONE;
    git_annotated_commit *annotated = nullptr;
    git::CommitHash commit;

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    state = static_cast<git_repository_state_t>(git_repository_state(m_repo));
    if (state != GIT_REPOSITORY_STATE_NONE)
    {
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));
    }

    std::vector<git_annotated_commit *> annotated_vec;
    std::vector<git_oid> oids;
    const git_oid *target_oid = nullptr;
    std::string hash;

    for (auto &ref : refs)
    {
        result = resolve_ref(&annotated, ref);
        if (result.ok())
        {
            target_oid = git_annotated_commit_id(annotated);
            oids.push_back(*target_oid);
            annotated_vec.push_back(annotated);

            result = convert_bin_hex(*target_oid, hash);
            if (result.ok())
            {
                commit.set_hash(hash);
                commits.push_back(commit);
            }
        }
    }

    int result_int = git_merge_analysis(&analysis, &preference, m_repo,
                                        (const git_annotated_commit **)annotated_vec.data(), annotated_vec.size());
    //! \TODO handling error is missing
    for (auto annotated : annotated_vec) git_annotated_commit_free(annotated);

    if (analysis & GIT_MERGE_ANALYSIS_UP_TO_DATE)
        merge_analysis_result = git::MergeAnalysisResult::UP_TO_DATE;
    else if (analysis & GIT_MERGE_ANALYSIS_NORMAL)
    {
        if (analysis & GIT_MERGE_ANALYSIS_FASTFORWARD)
            merge_analysis_result = git::MergeAnalysisResult::FASTFORWARD;
        else
            merge_analysis_result = git::MergeAnalysisResult::NORMAL;
    }
    else if (analysis & GIT_MERGE_ANALYSIS_FASTFORWARD)
        merge_analysis_result = git::MergeAnalysisResult::FASTFORWARD;
    else if (analysis & GIT_MERGE_ANALYSIS_UNBORN)
        merge_analysis_result = git::MergeAnalysisResult::UNBORN;
    else
        merge_analysis_result = git::MergeAnalysisResult::NONE;
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::find_remote(Guard<git_remote> &remote, const std::string &remote_name)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, remote, remote_name);
    ETRC_CALL_RET_OUT_END(remote);

    if (m_repo == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    std::string remote_name_str;

    if (!remote_name.empty())
    {
        int result_int = git_remote_lookup(remote.get_p(), m_repo, remote_name.c_str());
        if (result_int < 0)
        {
            std::string err_str = "The given remote is not known : " + remote_name;
            // The remote is not known
            self()->error(err_str);
            return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int, err_str));
        }

        remote_name_str = git_remote_name(remote.get());
    }
    else
    {
        git::Branches branches;

        result = get_branches(branches);
        if (result.error())
        {
            std::string err_str = "Couldn't get the branches for this git repo";
            self()->error(err_str);
            return ETRC_RET(ESYSREPO_RESULT(result, err_str));
        }

        if (branches.size() == 0)
        {
            self()->error("No branches found for this git repo");
            return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_NO_BRANCH_FOUND));
        }

        branches.sort();

        remote_name_str = branches.get()[0]->get_remote_name();

        int result_int = git_remote_lookup(remote.get_p(), m_repo, remote_name_str.c_str());
        if (result_int < 0)
        {
            std::string err_str = "The given remote is not known : " + remote_name_str;
            // The remote is not known
            self()->error(err_str);
            return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int, err_str));
        }
    }

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::get_commit_hash_from_branch(const std::string &branch, git_oid &oid)
{
    Guard<git_annotated_commit> annotated_commit;
    Guard<git_reference> input_branch_ref;
    std::string new_ref = branch;
    Guard<git_commit> target_commit;

    // Guard<git_object> treeish;

    // Guard<git_reference> ref;
    // Guard<git_reference> branch_ref;

    Result result = resolve_ref(input_branch_ref.get_p(), annotated_commit.get_p(), branch);
    if (result.error())
    {
        self()->debug(0, "branch not found : " + branch);
        // In the case where the content of branch doesn't have the full qualifier,
        // try to to guess

        result = find_ref(input_branch_ref.get_p(), annotated_commit.get_p(), branch, new_ref);
        if (result.error())
        {
            return check_error(ESYSREPO_RESULT(result));
        }
    }

    int result_int = git_commit_lookup(target_commit.get_p(), m_repo, git_annotated_commit_id(annotated_commit.get()));
    if (result_int < 0) return check_error(ESYSREPO_RESULT(ResultCode::GIT_FIND_REF_FAILED));

    const git_oid *target_oid = git_commit_id(target_commit.get());
    if (target_oid == nullptr) ESYSREPO_RESULT(ResultCode::GIT_GENERIC_ERROR);
    oid = *target_oid;
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::fetch(const std::string &remote_str)
{
    Result result;
    ETRC_CALL_RET(result, remote_str);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    Guard<git_remote> remote;

    result = find_remote(remote, remote_str);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    git_fetch_options fetch_opts = GIT_FETCH_OPTIONS_INIT;

    /* Set up the callbacks (only update_tips for now) */
    // fetch_opts.callbacks.update_tips = &update_cb;
    fetch_opts.callbacks.sideband_progress = &GitImpl::libgit2_sideband_progress_cb;
    // fetch_opts.callbacks.transfer_progress = transfer_progress_cb;
    fetch_opts.callbacks.credentials = &GitImpl::libgit2_credentials_cb;
    fetch_opts.callbacks.payload = this;

    int result_int = git_remote_fetch(remote.get(), nullptr, &fetch_opts, "fetch");
    if (result_int < 0)
    {
        std::string err_str = "Fetch failed";
        self()->error(err_str);
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int, err_str));
    }

    const git_indexer_progress *stats = git_remote_stats(remote.get());

    //! \TODO what to do with the stats;

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::fetch_all_notes(const std::string &remote_str)
{
    Result result;
    ETRC_CALL_RET(result, remote_str);

    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    m_notes.clear();

    Guard<git_remote> remote;

    result = find_remote(remote, remote_str);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    Guard<git_refspec> ref_spec;
    int result_int = git_refspec_parse(ref_spec.get_p(), "refs/notes/*:refs/notes/*", 1);
    if (result_int != 0) ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    git_fetch_options fetch_opts = GIT_FETCH_OPTIONS_INIT;

    /* Set up the callbacks (only update_tips for now) */
    // fetch_opts.callbacks.update_tips = &update_cb;
    fetch_opts.callbacks.sideband_progress = &GitImpl::libgit2_sideband_progress_cb;
    // fetch_opts.callbacks.transfer_progress = transfer_progress_cb;
    fetch_opts.callbacks.credentials = &GitImpl::libgit2_credentials_cb;
    fetch_opts.callbacks.payload = this;

    git_strarray refspecs;
    char refspecs_str[] = "refs/notes/*:refs/notes/*";
    char *refspecs_array[1];
    refspecs_array[0] = refspecs_str;
    refspecs.count = 1;
    refspecs.strings = refspecs_array;

    result_int = git_remote_fetch(remote.get(), &refspecs, &fetch_opts, "fetch");
    if (result_int < 0)
    {
        std::string err_str = "Fetch failed";
        self()->error(err_str);
        return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int, err_str));
    }

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::add_note(git::NoteId &note_id, const std::string &reference, const std::string &message, bool overwrite)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, note_id, reference, message, overwrite);
    ETRC_CALL_RET_OUT_END(note_id);

    if (!is_open()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_REPO_NOT_OPEN));
    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));
    if (self()->get_author() == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::NO_AUTHOR_GIVEN));

    std::string the_ref = reference;
    Guard<git_signature> author_sig;
    Guard<git_signature> committer_sig;

    the_ref = normalize_notes_ref(reference);

    result = get_sigs(author_sig.get_p(), committer_sig.get_p());
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    git_oid note_iod;
    git_oid oid_last_commit;

    // resolve HEAD into a SHA1
    auto result_int = git_reference_name_to_id(&oid_last_commit, m_repo, "HEAD");
    if (result_int < 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    result_int = git_note_create(&note_iod, m_repo, the_ref.c_str(), author_sig.get(), committer_sig.get(),
                                 &oid_last_commit, message.c_str(), overwrite);
    if (result_int != 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    m_notes.clear();

    result = get_commit_hash(note_iod, note_id);
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    result = get_commit_hash(oid_last_commit, note_id.get_commit_hash());
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    note_id.set_reference(reference);

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::remove_note(const git::NoteId &note_id)
{
    Result result;
    ETRC_CALL_RET(result, note_id);

    if (!is_open()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_REPO_NOT_OPEN));
    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));
    if (self()->get_author() == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::NO_AUTHOR_GIVEN));

    git_oid note_oid;

    int result_int = git_oid_fromstr(&note_oid, note_id.get_hash().c_str());
    if (result_int != 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    return ETRC_RET(remove_note(note_id.get_commit_hash(), note_id.get_reference()));
}

Result GitImpl::remove_note(const git::CommitHash &commit_hash, const std::string &reference)
{
    Result result;
    ETRC_CALL_RET(result, commit_hash, reference);

    if (!is_open()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_REPO_NOT_OPEN));
    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));
    if (self()->get_author() == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::NO_AUTHOR_GIVEN));

    Guard<git_signature> author_sig;
    Guard<git_signature> committer_sig;
    git_oid target_oid;

    result = get_sigs(author_sig.get_p(), committer_sig.get_p());
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    int result_int = git_oid_fromstr(&target_oid, commit_hash.get_hash().c_str());
    if (result_int != 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    std::string note_ref = normalize_notes_ref(reference);
    result_int = git_note_remove(m_repo, note_ref.c_str(), author_sig.get(), committer_sig.get(), &target_oid);

    if (result_int != 0) return ETRC_RET(check_error(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int)));

    m_notes.clear();
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::remove_note_last_commit(const std::string &reference)
{
    Result result;
    ETRC_CALL_RET(result, reference);

    git::CommitHash commit_hash;
    result = get_last_commit(commit_hash);
    if (result.error()) return ETRC_RET(check_error(ESYSREPO_RESULT(result)));

    return ETRC_RET(remove_note(commit_hash, reference));
}

Result GitImpl::get_refspec(std::string &refspec_str, const std::string &src_ref, const std::string &dst_ref)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, refspec_str, src_ref, dst_ref);
    ETRC_CALL_RET_OUT_END(refspec_str);

    if (!is_open()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_REPO_NOT_OPEN));
    if (m_repo == nullptr) return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));

    std::string src_ref_name;
    std::string dst_ref_name;

    if (src_ref.empty() && dst_ref.empty())
    {
        // Push the head
        Guard<git_reference> head_ref;

        int result_int = git_repository_head(head_ref.get_p(), m_repo);
        if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

        src_ref_name = git_reference_name(head_ref.get());
    }
    if (!src_ref.empty())
    {
        Guard<git_reference> git_src_ref;
        result = resolve_ref(git_src_ref.get_p(), src_ref);
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

        src_ref_name = git_reference_name(git_src_ref.get());
    }
    if (!dst_ref.empty())
    {
        Guard<git_reference> git_dst_ref;
        result = resolve_ref(git_dst_ref.get_p(), src_ref);
        if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

        dst_ref_name = git_reference_name(git_dst_ref.get());
    }
    else
        dst_ref_name = src_ref_name;

    refspec_str = src_ref_name;
    refspec_str += ":";
    refspec_str += dst_ref_name;
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::push(const std::string &remote, const std::string &src_ref, const std::string &dst_ref)
{
    Result result;
    ETRC_CALL_RET(result, src_ref, dst_ref);

    git_push_options options;
    Guard<git_remote> remote_git;
    std::string refspec_str;

    result = get_refspec(refspec_str, src_ref, dst_ref);
    if (result.error()) return ETRC_RET(ESYSREPO_RESULT(result));

    char *refspec = (char *)refspec_str.c_str();
    const git_strarray refspecs = {&refspec, 1};

    int result_int = git_remote_lookup(remote_git.get_p(), m_repo, remote.c_str());
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    result_int = git_push_options_init(&options, GIT_PUSH_OPTIONS_VERSION);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    result_int = git_remote_push(remote_git.get(), &refspecs, &options);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::push_notes(const std::string &remote, const std::string &reference)
{
    Result result;
    ETRC_CALL_RET(result, remote, reference);

    git_push_options options;
    Guard<git_remote> remote_git;
    std::string note_ref = normalize_notes_ref(reference);
    char *refspec = (char *)note_ref.c_str();
    const git_strarray refspecs = {&refspec, 1};

    int result_int = git_remote_lookup(remote_git.get_p(), m_repo, remote.c_str());
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    result_int = git_push_options_init(&options, GIT_PUSH_OPTIONS_VERSION);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    result_int = git_remote_push(remote_git.get(), &refspecs, &options);
    if (result_int < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int));

    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitImpl::resolve_ref(git_reference **ref, const std::string &ref_str)
{
    Guard<git_object> git_obj;
    int result = 0;

    assert(ref != nullptr);
    assert(m_repo != nullptr);

    result = git_reference_dwim(ref, m_repo, ref_str.c_str());
    if (result == GIT_OK) return ESYSREPO_RESULT(ResultCode::OK);

    result = git_revparse_single(git_obj.get_p(), m_repo, ref_str.c_str());
    if (result == GIT_OK) return ESYSREPO_RESULT(ResultCode::OK);

    return ESYSREPO_RESULT(ResultCode::GENERIC_ERROR);
}

Result GitImpl::resolve_ref(git_annotated_commit **commit, const std::string &ref)
{
    git_reference *git_ref = nullptr;
    git_object *git_obj = nullptr;
    int result = 0;

    assert(commit != nullptr);
    assert(m_repo != nullptr);

    result = git_reference_dwim(&git_ref, m_repo, ref.c_str());
    if (result == GIT_OK)
    {
        const char *name = git_reference_name(git_ref);

        //! \TODO remote this
        const char *branch_name = nullptr;
        git_branch_name(&branch_name, git_ref);

        git_annotated_commit_from_ref(commit, m_repo, git_ref);
        git_reference_free(git_ref);
        return ESYSREPO_RESULT(ResultCode::OK);
    }

    result = git_revparse_single(&git_obj, m_repo, ref.c_str());
    if (result == GIT_OK)
    {
        result = git_annotated_commit_lookup(commit, m_repo, git_object_id(git_obj));
        git_object_free(git_obj);
        return ESYSREPO_RESULT(ResultCode::OK);
    }

    return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result);
}

Result GitImpl::resolve_ref(git_reference **ref, git_annotated_commit **commit, const std::string &ref_str)
{
    Guard<git_object> git_obj;
    int result = 0;

    assert(commit != nullptr);
    assert(ref != nullptr);
    assert(m_repo != nullptr);

    result = git_reference_dwim(ref, m_repo, ref_str.c_str());
    if (result == GIT_OK)
    {
        git_annotated_commit_from_ref(commit, m_repo, *ref);
        return ESYSREPO_RESULT(ResultCode::OK);
    }

    result = git_revparse_single(git_obj.get_p(), m_repo, ref_str.c_str());
    if (result == GIT_OK)
    {
        result = git_annotated_commit_lookup(commit, m_repo, git_object_id(git_obj.get()));
    }

    if (result == GIT_OK) return ESYSREPO_RESULT(ResultCode::OK);
    return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result);
}

Result GitImpl::find_ref(git_annotated_commit **commit, const std::string &ref, std::string &new_ref)
{
    std::vector<git::Remote> remotes;
    int found_remotes = 0;

    Result rresult = get_remotes(remotes);
    if (rresult.error()) return ESYSREPO_RESULT(rresult);

    int result = 0;

    for (auto &remote : remotes)
    {
        new_ref = "refs/remotes/" + remote.get_name() + "/" + ref;

        rresult = resolve_ref(commit, new_ref);
        if (rresult.ok())
        {
            self()->debug(0, "found branch : " + new_ref);
            ++found_remotes;
        }
    }
    if (found_remotes == 1) return ESYSREPO_RESULT(ResultCode::OK);
    return ESYSREPO_RESULT(ResultCode::GIT_FIND_REF_FAILED);
}

Result GitImpl::find_ref(git_reference **ref, git_annotated_commit **commit, const std::string &ref_str,
                         std::string &new_ref)
{
    std::vector<git::Remote> remotes;
    int found_remotes = 0;

    Result rresult = get_remotes(remotes);
    if (rresult.error()) return ESYSREPO_RESULT(rresult);

    int result = 0;
    for (auto &remote : remotes)
    {
        new_ref = "refs/remotes/" + remote.get_name() + "/" + ref_str;

        rresult = resolve_ref(ref, commit, new_ref);
        if (rresult.ok())
        {
            self()->debug(0, "found branch : " + new_ref);
            ++found_remotes;
        }
    }
    if (found_remotes == 1) return ESYSREPO_RESULT(ResultCode::OK);
    return ESYSREPO_RESULT(ResultCode::GIT_FIND_REF_FAILED);
}

Result GitImpl::convert_bin_hex(const git_oid &oid, std::string &hex_str)
{
    char temp[GIT_OID_HEXSZ + 1];

    int result = git_oid_fmt(temp, &oid);
    if (result < 0) return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result);
    temp[GIT_OID_HEXSZ] = 0;
    hex_str = std::string(temp);
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result GitImpl::convert_hex_bin(const std::string &hex_str, git_oid &oid)
{
    int result = git_oid_fromstrp(&oid, hex_str.c_str());
    if (result == GIT_OK) return ESYSREPO_RESULT(ResultCode::OK);
    return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result);
}

const std::string &GitImpl::get_version()
{
    return s_get_version();
}

const std::string &GitImpl::get_lib_name()
{
    return s_get_lib_name();
}

void GitImpl::convert(git::BranchType branch_type, git_branch_t &list_flags)
{
    switch (branch_type)
    {
        case git::BranchType::ALL: list_flags = GIT_BRANCH_ALL; break;
        case git::BranchType::LOCAL: list_flags = GIT_BRANCH_LOCAL; break;
        case git::BranchType::REMOTE: list_flags = GIT_BRANCH_REMOTE; break;
        default: list_flags = GIT_BRANCH_LOCAL;
    }
}

void GitImpl::set_agent_identity_path(const std::string &agent_identity_path)
{
    m_agent_identity_path = agent_identity_path;
}

const std::string &GitImpl::get_agent_identity_path() const
{
    return m_agent_identity_path;
}

void GitImpl::set_logger_if(std::shared_ptr<log::Logger_if> logger_if)
{
    m_ssh.set_logger_if(logger_if);
}

git_repository *GitImpl::get_repo()
{
    return m_repo;
}

Result GitImpl::get_sigs(git_signature **author, git_signature **committer)
{
    std::shared_ptr<git::Person> person = self()->get_author();

    int result_int = git_signature_now(author, person->get_name().c_str(), person->get_email().c_str());
    if (result_int < 0) return ESYSREPO_RESULT(ResultCode::GIT_GENERIC_ERROR);

    if (self()->get_committer() != nullptr) person = self()->get_committer();

    result_int = git_signature_now(committer, person->get_name().c_str(), person->get_email().c_str());
    if (result_int < 0) return ESYSREPO_RESULT(ResultCode::GIT_GENERIC_ERROR);

    return ESYSREPO_RESULT(ResultCode::OK);
}

std::string GitImpl::normalize_notes_ref(const std::string &reference)
{
    std::string the_ref = reference;

    if (reference.find("refs/notes/") == std::string::npos) the_ref = "refs/notes/" + reference;

    return the_ref;
}

const std::string &GitImpl::s_get_version()
{
    static std::string s_version = LIBGIT2_VERSION;
    return s_version;
}

const std::string &GitImpl::s_get_lib_name()
{
    static std::string s_lib_name = "libgit2";
    return s_lib_name;
}

const std::string &GitImpl::s_get_ssh_version()
{
    static std::string s_version = LIBSSH2_VERSION;
    return s_version;
}

const std::string &GitImpl::s_get_ssh_lib_name()
{
    static std::string s_lib_name = "libssh2";
    return s_lib_name;
}

} // namespace esys::repo::libgit2
