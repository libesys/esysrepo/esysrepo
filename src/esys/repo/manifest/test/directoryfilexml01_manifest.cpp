/*!
 * \file esys/repo/manifest/test/directoryfilexml01_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/manifest/directoryfilexml.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::manifest::test
{

/*! \class DirectoryFileXML01Manifest esys/repo/manifest/test/directoryfilexml01_manifest.cpp
 * "esys/repo/manifest/test/directoryfilexml01_manifest.cpp"
 *
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(DirectoryFileXML01Manifest)
{
    boost::filesystem::path path;

    path = repo::test::TestCaseCtrl::get().GetTempFilesFolder();
    path /= ("directoryfilexml01manifest");
    path = path.normalize().make_preferred();

    bool remove_all = true;

    try
    {
        if (boost::filesystem::exists(path)) boost::filesystem::remove_all(path);
    }
    catch (const boost::filesystem::filesystem_error &e)
    {
        remove_all = false;
        std::cerr << e.what() << std::endl;
    }

    ESYSTEST_REQUIRE_EQUAL(remove_all, true);

    bool folder_created = boost::filesystem::create_directories(path);
    ESYSTEST_REQUIRE_EQUAL(folder_created, true);

    DirectoryFileXML xml_file_write;
    auto write_directory = std::make_shared<Directory>();
    xml_file_write.set_directory(write_directory);

    write_directory->set_name("libesys");

    auto directory_item = std::make_shared<Directory::Item>();
    directory_item->set_name("esysrepo_dev");
    directory_item->set_url_ssh("ssh://git@gitlab.com/libesys/esysrepo/manifest");
    directory_item->set_url_https("https://gitlab.com/libesys/esysrepo/manifest");

    write_directory->add_item(directory_item);

    directory_item = std::make_shared<Directory::Item>();
    directory_item->set_name("esysfile_dev");
    directory_item->set_url_ssh("ssh://git@gitlab.com/libesys/esysfile_dev/manifest");
    directory_item->set_url_https("https://gitlab.com/libesys/esysfile_dev/manifest");

    write_directory->add_item(directory_item);

    directory_item = std::make_shared<Directory::Item>();
    directory_item->set_name("esystest_dev");
    directory_item->set_url_ssh("ssh://git@gitlab.com/libesys/esystest_dev/manifest");
    directory_item->set_url_https("https://gitlab.com/libesys/esystest_dev/manifest");

    write_directory->add_item(directory_item);

    directory_item = std::make_shared<Directory::Item>();
    directory_item->set_name("esysinstall_dev");
    directory_item->set_url_ssh("ssh://git@gitlab.com/libesys/esysinstall/manifest");
    directory_item->set_url_https("https://gitlab.com/libesys/esysinstall/manifest");

    write_directory->add_item(directory_item);

    directory_item = std::make_shared<Directory::Item>();
    directory_item->set_name("esystrace_dev");
    directory_item->set_url_ssh("ssh://git@gitlab.com/libesys/esystrace/manifest");
    directory_item->set_url_https("https://gitlab.com/libesys/esystrace/manifest");

    write_directory->add_item(directory_item);

    path /= "directory.xml";

    Result result = xml_file_write.write(path.make_preferred().string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    DirectoryFileXML xml_file_read;
    auto read_directory = std::make_shared<Directory>();
    xml_file_read.set_directory(read_directory);

    bool equal = (*read_directory == *write_directory);
    ESYSTEST_REQUIRE_EQUAL(equal, false);

    result = xml_file_read.read(path.make_preferred().string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    equal = (*read_directory == *write_directory);
    ESYSTEST_REQUIRE_EQUAL(equal, true);

    std::ostringstream oss;
    result = xml_file_write.write(oss);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    std::istringstream iss;
    iss.str(oss.str());
    read_directory = std::make_shared<Directory>();
    xml_file_read.set_directory(read_directory);

    equal = (*read_directory == *write_directory);
    ESYSTEST_REQUIRE_EQUAL(equal, false);

    result = xml_file_read.read(iss);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
    equal = (*read_directory == *write_directory);
    ESYSTEST_REQUIRE_EQUAL(equal, true);
}

} // namespace esys::repo::manifest::test
