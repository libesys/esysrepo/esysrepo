/*!
 * \file esys/repo/exe/cmdmanifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/exe/cmdmanifest.h"
#include "esys/repo/manifest/file.h"
#include "esys/repo/grepo/manifest.h"
#include "esys/repo/manifest/directorymngr.h"
#include "esys/repo/manifest/repository.h"
#include "esys/repo/manifest/xmlfile.h"
#include "esys/repo/githelper.h"
#include "esys/repo/userfolder.h"

#include <esys/trace/call.h>
#include <esys/trace/macros.h>

#include <boost/filesystem.hpp>

#include <iostream>
#include <fstream>
#include <sstream>

namespace esys::repo::exe
{

CmdManifest::TaskDelDir::TaskDelDir() = default;

void CmdManifest::TaskDelDir::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &CmdManifest::TaskDelDir::get_name() const
{
    return m_name;
}

CmdManifest::TaskAddDir::TaskAddDir() = default;

void CmdManifest::TaskAddDir::set_url_ssh(const std::string &url_ssh)
{
    m_url_ssh = url_ssh;
}

const std::string &CmdManifest::TaskAddDir::get_url_ssh() const
{
    return m_url_ssh;
}

void CmdManifest::TaskAddDir::set_url_https(const std::string &url_https)
{
    m_url_https = url_https;
}

const std::string &CmdManifest::TaskAddDir::get_url_https() const
{
    return m_url_https;
}

CmdManifest::CmdManifest()
    : Cmd("Manifest")
{
}

CmdManifest::~CmdManifest() = default;

void CmdManifest::set_task(Task task)
{
    m_task = task;
}

CmdManifest::Task CmdManifest::get_task() const
{
    return m_task;
}

void CmdManifest::set_task_add_dir(const TaskAddDir &task_add_dir)
{
    m_task_add_dir = task_add_dir;
    set_task(Task::ADD_DIR);
}

const CmdManifest::TaskAddDir &CmdManifest::get_task_add_dir() const
{
    return m_task_add_dir;
}

void CmdManifest::set_task_del_dir(const TaskDelDir &task_del_dir)
{
    m_task_del_dir = task_del_dir;
    set_task(Task::DEL_DIR);
}

const CmdManifest::TaskDelDir &CmdManifest::get_task_del_dir() const
{
    return m_task_del_dir;
}

void CmdManifest::set_dirs(const std::string &dirs)
{
    m_dirs = dirs;
}

const std::string &CmdManifest::get_dirs() const
{
    return m_dirs;
}

void CmdManifest::set_output_file(const std::string &output_file)
{
    m_output_file = output_file;
}

const std::string &CmdManifest::get_output_file() const
{
    return m_output_file;
}

Result CmdManifest::update_revision_as_head()
{
    for (auto location : get_manifest()->get_locations())
    {
        for (auto repo : location->get_repos())
        {
            Result result = update_revision_as_head(repo);
            if (result.error()) return ESYSREPO_RESULT(result);
        }
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result CmdManifest::update_revision_as_head(std::shared_ptr<manifest::Repository> repo)
{
    auto git_helper = new_git_helper();

    boost::filesystem::path repo_path = get_config_folder()->get_workspace_path();
    repo_path /= repo->get_path();
    repo_path = boost::filesystem::absolute(repo_path).normalize().make_preferred();

    std::string revision = repo->get_revision();
    if (revision.empty()) revision = get_manifest()->get_default_revision();

    Result rresult = git_helper->open(repo_path.string(), log::Level::DEBUG);
    if (rresult.error()) return ESYSREPO_RESULT(rresult);

    rresult = git_helper->fetch(log::Level::DEBUG);
    if (rresult.error()) return ESYSREPO_RESULT(rresult);

    std::string hash;
    rresult = git_helper->get_hash(revision, hash, log::Level::DEBUG);
    if (rresult.error())
    {
        git_helper->close_on_error(log::Level::DEBUG);
        return ESYSREPO_RESULT(rresult);
    }
    repo->set_revision(hash);

    return git_helper->close(log::Level::DEBUG);
}

Result CmdManifest::do_revision_as_head()
{
    Result result = only_one_folder_or_workspace();
    if (result.error()) return ESYSREPO_RESULT(result);

    result = open_esysrepo_folder();
    if (result.error()) return ESYSREPO_RESULT(result);

    result = load_manifest();
    if (result.error()) return ESYSREPO_RESULT(result);

    result = update_revision_as_head();
    if (result.error())
    {
        std::string err_str = "Failed to get the revision as HEAD";
        error(err_str);
        return ESYSREPO_RESULT(result, err_str);
    }

    std::shared_ptr<std::ofstream> fos;
    std::ostream *os = nullptr;

    if (get_output_file().empty())
        os = &std::cout;
    else
    {
        fos = std::make_shared<std::ofstream>(get_output_file());
        os = fos.get();
    }

    std::shared_ptr<manifest::FileBase> file;

    switch (get_manifest()->get_type())
    {
        case manifest::Type::GOOGLE_MANIFEST: file = std::make_shared<grepo::Manifest>(); break;
        case manifest::Type::ESYSREPO_MANIFEST: file = std::make_shared<manifest::XMLFile>(); break;
        default: error("Manifest format not supported."); return generic_error(-1);
    }

    file->set_data(get_manifest());
    result = file->write(*os);

    if (fos != nullptr) fos->close();
    return ESYSREPO_RESULT(result);
}

Result CmdManifest::do_list_dirs()
{
    UserFolder user_folder;

    auto result = user_folder.read();
    if (result.error()) return ESYSREPO_RESULT(result);

    if (user_folder.get_user_config() == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    auto mds = user_folder.get_user_config()->get_manifest_directories();
    if (mds == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    std::ostringstream oss;

    for (auto idx = 0; idx < mds->get_items().size(); ++idx)
    {
        auto md = mds->get_items()[idx];
        oss.str("");

        oss << "[" << idx << "] " << md->get_name() << std::endl;
        oss << "    " << md->get_url_ssh() << std::endl;
        oss << "    " << md->get_url_https();

        info(oss.str());
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result CmdManifest::do_list_projects()
{
    UserFolder user_folder;

    auto result = user_folder.read();
    if (result.error()) return ESYSREPO_RESULT(result);

    if (user_folder.get_user_config() == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    auto mds = user_folder.get_user_config()->get_manifest_directories();
    if (mds == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    manifest::DirectoryMngr mngr;

    mngr.set_folder_path(user_folder.get_user_config()->get_manifest_directories_path());
    mngr.set_directories(mds);

    result = mngr.load();
    if (result.error()) return ESYSREPO_RESULT(result);

    std::ostringstream oss;

    for (auto idx = 0; idx < mds->get_items().size(); ++idx)
    {
        auto md = mds->get_items()[idx];
        oss.str("");

        oss << "[" << idx << "] " << md->get_name() << std::endl;
        oss << "    urls:" << std::endl;
        oss << "        " << md->get_url_ssh() << std::endl;
        oss << "        " << md->get_url_https();

        if (md->get_directory() == nullptr)
        {
            warn("Manifest directory has no projects.");
            continue;
        }
        oss << std::endl << "    projects:";
        for (auto dir : md->get_directory()->get_items())
        {
            oss << std::endl << "        " << dir->get_name();
            // oss << "            " << dir
        }

        info(oss.str());
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result CmdManifest::do_add_dir()
{
    UserFolder user_folder;

    auto result = user_folder.read();
    if (result.error()) return ESYSREPO_RESULT(result);

    if (user_folder.get_user_config() == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    auto mds = user_folder.get_user_config()->get_manifest_directories();
    if (mds == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    auto item = std::make_shared<manifest::Directories::Item>();
    item->set_name(get_task_add_dir().get_name());
    item->set_url_ssh(get_task_add_dir().get_url_ssh());
    item->set_url_https(get_task_add_dir().get_url_https());

    mds->add_item(item->get_name(), item);

    result = user_folder.write();
    if (result.error()) return ESYSREPO_RESULT(result);

    info("Added manifest directory '" + get_task_del_dir().get_name() + "'.");
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result CmdManifest::do_del_dir()
{
    UserFolder user_folder;

    auto result = user_folder.read();
    if (result.error()) return ESYSREPO_RESULT(result);

    if (user_folder.get_user_config() == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    auto mds = user_folder.get_user_config()->get_manifest_directories();
    if (mds == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    result = mds->del_item(get_task_del_dir().get_name());
    if (result.error()) return ESYSREPO_RESULT(result);

    result = user_folder.write();
    if (result.error()) return ESYSREPO_RESULT(result);

    info("Deleted manifest directory '" + get_task_del_dir().get_name() + "'.");
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result CmdManifest::impl_run()
{
    Result result;
    ETRC_CALL_RET_NP(result);

    switch (get_task())
    {
        case Task::REVISION_AS_HEAD:
        case Task::NOT_SET: return ETRC_RET(do_revision_as_head());
        case Task::LIST_DIRS: return ETRC_RET(do_list_dirs());
        case Task::LIST_PROJECTS: return ETRC_RET(do_list_projects());
        case Task::ADD_DIR: return ETRC_RET(do_add_dir());
        case Task::DEL_DIR: return ETRC_RET(do_del_dir());
        default: return ETRC_RET(ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR));
    }
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

} // namespace esys::repo::exe
