/*!
 * \file esys/repo/exe/test/cmdmulticapture01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"
#include "esys/repo/exe/test/fixcmd.h"

#include <esys/repo/exe/cmdmulti.h>
#include <esys/repo/libgit2/git.h>

namespace esys::repo::exe::test
{

/*! \class CmdMultiCapture01 esys/repo/exe/test/cmdmulticapture01.cpp
 * "esys/repo/exe/test/cmdmulticapture01.cpp"
 *
 * \brief
 */
ESYSTEST_AUTO_TEST_CASE(CmdMultiCapture01)
{
    const std::string new_git_url = "ssh://git@gitlab.com/libesys/esysrepo/test1";
    libgit2::Git git;

    auto &ctrl = repo::test::TestCaseCtrl::get();

    boost::filesystem::path file_path = ctrl.delete_create_temp_folder("cmdmulticapture01");
    ESYSTEST_REQUIRE_EQUAL(file_path.string().empty(), false);

    boost::filesystem::path repo_orig_path = file_path / "repo_orig";
    auto result_bool = boost::filesystem::create_directories(repo_orig_path);
    ESYSTEST_REQUIRE(result_bool);

    boost::filesystem::path new_git_path = file_path / "new_git";
    result_bool = boost::filesystem::create_directories(new_git_path);
    ESYSTEST_REQUIRE(result_bool);

    FixCmd m_fix_cmd_sync;

    m_fix_cmd_sync.set_temp_sub_folder("cmdmulticapture01");
    m_fix_cmd_sync.set_manifest_url("ssh://git@gitlab.com/libesys/esysrepo/test_google_manifest.git");
    m_fix_cmd_sync.set_sub_folder_for_repo("repo_orig");
    m_fix_cmd_sync.run();

    libgit2::Git git_root_repo;

    auto result = git_root_repo.open(repo_orig_path.string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git_root_repo.add_remote("new_remote", new_git_url);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git_root_repo.fetch("new_remote");
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git_root_repo.checkout("commit_c");
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git_root_repo.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    CmdMulti multi;
    multi.set_workspace_path(m_fix_cmd_sync.get_file_path().string());
    multi.set_task(CmdMulti::Task::CAPTURE);
    multi.set_logger_if(m_fix_cmd_sync.get_logger());

    result = multi.run();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git_root_repo.open(repo_orig_path.string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    git_root_repo.add_notes_ref("esysrepo");

    git::Commit last_commit;

    result = git_root_repo.get_last_commit(last_commit, true);
    if (result.error()) std::cout << "ERROR " << result << std::endl;

    std::vector<std::shared_ptr<git::Note>> notes;
    ESYSTEST_REQUIRE_EQUAL(last_commit.get_notes("esysrepo", notes), 0);
    ESYSTEST_REQUIRE_EQUAL(notes.size(), 1);

    result = git_root_repo.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    auto capture = std::make_shared<manifest::Capture>();
    result = multi.load_capture(capture, notes[0]->get_message());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    auto items = capture->get_items();
    ESYSTEST_REQUIRE_EQUAL(items.size(), 1);

    auto item = items[0];
    ESYSTEST_REQUIRE_EQUAL(item->get_path(), ".");
    ESYSTEST_REQUIRE_EQUAL(item->get_remote_name(), "new_remote");
    ESYSTEST_REQUIRE_EQUAL(item->get_remote_url(), new_git_url);
    ESYSTEST_REQUIRE_EQUAL(item->get_revision(), "commit_c");
}

} // namespace esys::repo::exe::test
