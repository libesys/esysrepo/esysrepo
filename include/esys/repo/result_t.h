/*!
 * \file esys/repo/result_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/result.h"

#ifdef _MSC_VER
#define ESYSREPO_RESULT_T(result, ...) esys::repo::Result_t(result, __FILE__, __LINE__, __FUNCSIG__, __VA_ARGS__)
#else
#define ESYSREPO_RESULT_T(result, ...) \
    esys::repo::Result_t(result, __FILE__, __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__)
#endif

//<swig_inc/>

namespace esys::repo
{

/*! \class Result_t esys/repo/result_t.h "esys/repo/result_t.h"
 * \brief
 */
template<typename R>
class Result_t : public Result
{
public:
    using Result::Result;

    template<typename V>
    friend void PrintResult_t(std::ostream &os, const Result_t<V> &result);

    //! Default constructor
    Result_t();

    //! Copy constructor
    Result_t(const Result_t &result);

    Result_t(R value);

    Result_t(Result result);

    //! Constructor
    /*!
     * \param[in] result the result
     * \param[in] file the file where the Result was created
     * \param[in] line_number the line number when the Result was created
     * \param[in] function the name of the function where the Result was created
     * \param[in] value the return value
     */
    Result_t(const Result &result, const std::string &file, int line_number, const std::string &function, R value);

    //! Constructor
    /*!
     * \param[in] result the result
     * \param[in] file the file where the Result was created
     * \param[in] line_number the line number when the Result was created
     * \param[in] function the name of the function where the Result was created
     * \param[in] value the return value
     * \param[in] text additional text associated with the return value
     */
    Result_t(const Result &result, const std::string &file, int line_number, const std::string &function, R value,
             const std::string &text);

    //! Constructor
    /*!
     * \param[in] result_code the Resultcode
     * \param[in] file the file where the Result was created
     * \param[in] line_number the line number when the Result was created
     * \param[in] function the name of the function where the Result was created
     * \param[in] value the return value
     */
    Result_t(ResultCode result_code, const std::string &file, int line_number, const std::string &function, R value);

    //! Constructor
    /*!
     * \param[in] result the result
     * \param[in] file the file where the Result was created
     * \param[in] line_number the line number when the Result was created
     * \param[in] function the name of the function where the Result was created
     */
    Result_t(Result_t<R> result, const std::string &file, int line_number, const std::string &function);

    //! Destructor
    ~Result_t() override;

    void set(const R &return_value);
    R get() const;

    operator R() const;
    R operator()() const;

    //! Assignment operator
    /*!
     * \return the result code
     */
    Result_t &operator=(const Result &other); //<swig_out/>

    //! Assignment operator
    /*!
     * \return the result code
     */
    Result_t &operator=(const Result_t &other); //<swig_out/>

    void print(std::ostream &os) const override;

    static Result_t<R> OK;

private:
    R m_return_value;
};

template<typename R>
Result_t<R> Result_t<R>::OK;

template<typename R>
Result_t<R>::Result_t()
    : Result()
{
}

template<typename R>
Result_t<R>::Result_t(const Result_t &result)
    : Result(result)
{
    set(result.get());
}

template<typename R>
Result_t<R>::Result_t(Result result)
    : Result(result)
{
}

template<typename R>
Result_t<R>::Result_t(const Result &result, const std::string &file, int line_number, const std::string &function,
                      R value)
    : Result(result, file, line_number, function)
    , m_return_value(value)
{
}

template<typename R>
Result_t<R>::Result_t(const Result &result, const std::string &file, int line_number, const std::string &function,
                      R value, const std::string &text)
    : Result(result, file, line_number, function, text)
    , m_return_value(value)
{
}

template<typename R>
Result_t<R>::Result_t(R value)
    : Result()
    , m_return_value(value)
{
}

template<typename R>
Result_t<R>::Result_t(ResultCode result_code, const std::string &file, int line_number, const std::string &function,
                      R value)
    : Result(result_code, file, line_number, function)
    , m_return_value(value)
{
}

template<typename R>
Result_t<R>::Result_t(Result_t<R> result, const std::string &file, int line_number, const std::string &function)
    : Result(result, file, line_number, function)
{
    set(result.get());
}

template<typename R>
Result_t<R>::~Result_t()
{
}

template<typename R>
void Result_t<R>::set(const R &return_value)
{
    m_return_value = return_value;
}

template<typename R>
R Result_t<R>::get() const
{
    return m_return_value;
}

template<typename R>
Result_t<R>::operator R() const
{
    return m_return_value;
}

template<typename R>
R Result_t<R>::operator()() const
{
    return m_return_value;
}

template<typename R>
Result_t<R> &Result_t<R>::operator=(const Result &other)
{
    set_result_code(other.get_result_code());
    set_error_info(other.get_error_info());
    return *this;
}

template<typename R>
Result_t<R> &Result_t<R>::operator=(const Result_t &other)
{
    if (&other == this) return *this;

    set_result_code(other.get_result_code());
    set_error_info(other.get_error_info());
    set_line_number(other.get_line_number());
    set(other.get());
    return *this;
}

template<typename V>
void PrintResult_t(std::ostream &os, const Result_t<V> &result)
{
    os << "Return value :" << std::endl;
    os << "    type  : " << typeid(V).name() << std::endl;
    os << "    value : " << result.get() << std::endl;
}

template<>
ESYSREPO_API void PrintResult_t<bool>(std::ostream &os, const Result_t<bool> &result);

template<typename R>
void Result_t<R>::print(std::ostream &os) const
{
    PrintResult_t<R>(os, *this);
}

} // namespace esys::repo

//<swig>%template(ResultBool) esys::repo::Result_t<bool>;</swig>

namespace std
{

template<typename T>
ostream &operator<<(ostream &os, const esys::repo::Result_t<T> &result)
{
    os << (const esys::repo::Result &)(result) << std::endl;
    esys::repo::PrintResult_t<T>(os, result);
    return os;
}

}
