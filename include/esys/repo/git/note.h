/*!
 * \file esys/repo/git/note.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/signature.h"

#include <string>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class Note esys/repo/git/note.h "esys/repo/git/note.h"
 * \brief Class to hold all information of a Git note
 */
class ESYSREPO_API Note
{
public:
    //! Default constructor
    Note();

    //! Destructor
    ~Note();

    //! Set the message of the note
    /*!
     * \param[in] message the message of the note
     */
    void set_message(const std::string &message);

    //! Get the message of the note
    /*!
     * \return the message of the note
     */
    const std::string &get_message() const;

    //! Set the author of the note
    /*!
     * \param[in] author the author of the note
     */
    void set_author(const Signature &author);

    //! Get the author of the note
    /*!
     * \return the author of the note
     */
    const Signature &get_author() const;

    //! Get the author of the note
    /*!
     * \return the author of the note
     */
    Signature &get_author();

    //! Set the committer of the note
    /*!
     * \param[in] author the committer of the note
     */
    void set_committer(const Signature &committer);

    //! Get the committer of the note
    /*!
     * \return the committer of the note
     */
    const Signature &get_committer() const;

    //! Get the committer of the note
    /*!
     * \return the committer of the note
     */
    Signature &get_committer();

    //! Set the reference of the note
    /*!
     * \param[in] author the reference of the note
     */
    void set_reference(const std::string &reference);

    //! Get the reference of the note
    /*!
     * \return the reference of the note
     */
    const std::string &get_reference() const;

    //! Equal to comparison operator
    bool operator==(const Note &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Note &other) const;

private:
    //!< \cond DOXY_IMPL
    std::string m_message;   //!< The message of the note
    Signature m_author;      //!< The author of the note
    Signature m_committer;   //!< The committer of the note
    std::string m_reference; //!< The reference
    //!< \endcond
};

} // namespace esys::repo::git
