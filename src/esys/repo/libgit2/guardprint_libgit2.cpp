/*!
 * \file esys/repo/libgit2/guardprint_libgit2.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/libgit2/guard.h"
#include "esys/repo/libgit2/guardprint.h"
#include "esys/repo/libgit2/ostream.h"

namespace esys::repo::libgit2
{

template<>
ESYSREPO_API void guard_print<git_remote>(std::ostream &os, const Guard<git_remote> &object)
{
    os << object.get();
}

} // namespace esys::repo::libgit2
