/*!
 * \file esys/repo/libgit2/guardprint.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <git2.h>

#include <cassert>
#include <memory>
#include <ostream>

namespace esys::repo::libgit2
{

template<typename T>
class Guard;

template<typename T>
void guard_print(std::ostream &os, const Guard<T> &object)
{
    assert(false);
}

template<>
ESYSREPO_API void guard_print<git_remote>(std::ostream &os, const Guard<git_remote> &object);

} // namespace esys::repo::libgit2
