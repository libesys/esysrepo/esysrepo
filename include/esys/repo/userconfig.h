/*!
 * \file esys/repo/userconfig.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/directories.h"

#include <string>

//<swig_inc/>

//<swig>%shared_ptr(esys::repo::UserConfig);</swig>

namespace esys::repo
{

/*! \class UserConfig esys/repo/userconfig.h "esys/repo/userconfig.h"
 * \brief Hold data of an user configuration
 */
class ESYSREPO_API UserConfig
{
public:
    //! Default configuration
    UserConfig();

    //! Destructor
    ~UserConfig();

    //! Set the temporary path of the UserFolder
    /*!
     * \param[in] temp_path the temporary path of the UserFolder
     */
    void set_temp_path(const std::string &temp_path);

    //! Get the temporary path of the UserFolder
    /*!
     * \return the temporary path of the UserFolder
     */
    const std::string &get_temp_path() const;

    //! Set the path of folder holding the manifest directories
    /*!
     * \param[in] manifest_directories_path the path of folder holding the manifest directories
     */
    void set_manifest_directories_path(const std::string &manifest_directories_path);

    //! Get the path of folder holding the manifest directories
    /*!
     * \return the path of folder holding the manifest directories
     */
    const std::string &get_manifest_directories_path() const;

    //! Set the folder path to store the execution traces
    /*!
     * \param[in] traces_path the folder path to store the execution traces
     */
    void set_traces_path(const std::string &traces_path);

    //! Get the folder path to store the execution traces
    /*!
     * \return the folder path to store the execution traces
     */
    const std::string &get_traces_path() const;

    void set_manifest_directories(std::shared_ptr<manifest::Directories> manifest_directories);

    std::shared_ptr<manifest::Directories> get_manifest_directories() const;

    void clear();

    //! Equal to comparison operator
    bool operator==(const UserConfig &cfg) const;

    //! Not equal to comparison operator
    bool operator!=(const UserConfig &cfg) const;

private:
    //!< \cond DOXY_IMPL
    std::string m_temp_path;                 //!< The path of the temporary folder
    std::string m_manifest_directories_path; //!< The folder path for the manifest directories
    std::string m_traces_path;               //!< The folder path where the execution traces are saved
    std::shared_ptr<manifest::Directories> m_manifest_directories;
    //!< \endcond
};

} // namespace esys::repo
