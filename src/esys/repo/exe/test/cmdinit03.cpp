/*!
 * \file esys/repo/exe/test/cmdinit03.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/libgit2/git.h>
#include <esys/repo/exe/cmdinit.h>
#include <esys/repo/filesystem.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::exe::test
{

/*! \class CmdInit03 esys/repo/exe/test/cmdinit03.cpp
 * "esys/repo/exe/test/cmdinit03.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(CmdInit03)
{
    boost::filesystem::path file_path;

    auto &ctrl = repo::test::TestCaseCtrl::get();
    file_path = ctrl.delete_create_temp_folder("cmdinit03");
    ESYSTEST_REQUIRE_EQUAL(file_path.string().empty(), false);

    CmdInit cmd_init;
    cmd_init.create_logger(file_path.string());
    ESYSTEST_REQUIRE_NE(cmd_init.get_logger_if(), nullptr);

    cmd_init.set_project_name("cppmyths_dev");
    cmd_init.set_workspace_path(file_path.string());

    Result result = cmd_init.run();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
}

} // namespace esys::repo::exe::test
