/*!
 * \file esys/repo/manifest/directorymngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/directories.h"
#include "esys/repo/result.h"

#include <memory>

namespace esys::repo::manifest
{

/*! \class DirectoryMngr esys/repo/manifest/directorymngr.h "esys/repo/manifest/directorymngr.h"
 * \brief
 */
class ESYSREPO_API DirectoryMngr
{
public:
    //! Default constructor
    DirectoryMngr();

    //! Destructor
    ~DirectoryMngr();

    void set_folder_path(const std::string &folder_path);
    const std::string &get_folder_path() const;

    void set_directories(std::shared_ptr<Directories> directories);
    std::shared_ptr<Directories> get_directories() const;

    Result load();
    Result load(std::shared_ptr<Directories::Item> directories_item);

private:
    //!< \cond DOXY_IMPL
    std::string m_folder_path;
    std::shared_ptr<Directories> m_directories;

    //!< \endcond
};

} // namespace esys::repo::manifest
