/*!
 * \file esys/repo/git/branchtype.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <ostream>

//<swig_inc/>

namespace esys::repo::git
{

enum class BranchType
{
    NOT_SET,
    LOCAL,
    REMOTE,
    ALL
};

} // namespace git

namespace std
{
ESYSREPO_API ostream &operator<<(ostream &os, esys::repo::git::BranchType branch_type); //<swig_out/>
}
