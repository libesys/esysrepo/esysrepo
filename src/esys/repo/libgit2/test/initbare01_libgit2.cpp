/*!
 * \file esys/repo/libgit2/test/initbare01_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/libgit2/git.h>

#include <libssh2.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::libgit2::test
{

/*! \class InitBare01LibGit2 esys/repo/libgit2/test/initbare01_libgit2.cpp
 * "esys/repo/libgit2/test/initbare01_libgit2.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(InitBare01LibGit2)
{
    auto &ctrl = repo::test::TestCaseCtrl::get();

    boost::filesystem::path file_path = ctrl.delete_create_temp_folder("initbare01libgit2");
    ESYSTEST_REQUIRE_EQUAL(file_path.string().empty(), false);

    Git git_bare;

    boost::filesystem::path git_bare_path = file_path / "bare";

    auto result = git_bare.init_bare(file_path.string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git_bare.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
}

} // namespace esys::repo::libgit2::test
