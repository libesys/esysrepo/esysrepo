/*!
 * \file esys/repo/manifest/test/directorymngr01_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/manifest/directorymngr.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::manifest::test
{

/*! \class DirectoryMngr01Manifest esys/repo/manifest/test/directorymngr01_manifest.cpp
 * "esys/repo/manifest/test/directorymngr01_manifest.cpp"
 *
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(DirectoryMngr01Manifest)
{
    boost::filesystem::path path;

    auto &ctrl = repo::test::TestCaseCtrl::get();
    path = ctrl.delete_create_temp_folder("directorymngr01manifest");
    ESYSTEST_REQUIRE_EQUAL(path.string().empty(), false);

    DirectoryMngr mngr;

    mngr.set_folder_path(path.string());

    auto directories = std::make_shared<Directories>();
    auto item = std::make_shared<Directories::Item>();
    item->set_name("libesys");
    item->set_url_ssh("ssh://git@gitlab.com/libesys/esysrepo/directory");
    item->set_url_https("https://gitlab.com/libesys/esysrepo/directory");
    directories->add_item("libesys", item);
    mngr.set_directories(directories);

    auto result = mngr.load();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    auto project = directories->find_project("esysrepo_dev");
    ESYSTEST_REQUIRE_NE(project, nullptr);
    ESYSTEST_REQUIRE_EQUAL(project->get_name(), "esysrepo_dev");
}

} // namespace esys::repo::manifest::test
