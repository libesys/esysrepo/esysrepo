/*!
 * \file esys/repo/libgit2/test/headbranchremote01_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/libgit2/git.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::libgit2::test
{

/*! \class RemoteHeadBranch01LibGit2 esys/repo/libgit2/test/headbranchremote01_libgit2.cpp
 * "esys/repo/libgit2/test/headbranchremote01_libgit2.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(HeadBranchRemote01LibGit2)
{
    auto &ctrl = repo::test::TestCaseCtrl::get();

    boost::filesystem::path file_path = ctrl.delete_create_temp_folder("headbranchremote01libgit2");
    ESYSTEST_REQUIRE_EQUAL(file_path.string().empty(), false);

    Git git;

    Result result =
        git.clone("ssh://git@gitlab.com/libesys/esysrepo/test.git", file_path.normalize().make_preferred().string());
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    git::Remote remote;
    git::Branch branch;
    result = git.get_head_branch_remote(branch, remote);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
}

} // namespace esys::repo::libgit2::test
