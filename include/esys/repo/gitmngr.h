/*!
 * \file esys/repo/gitmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/gitbase.h"

#include <esys/log/user.h>

#include <vector>
#include <memory>

//<swig_inc/>

namespace esys
{

namespace repo
{

/*! \class GitMngr esys/repo/gitmngr.h "esys/repo/gitmgr.h"
 * \brief Git Manager
 *
 */
class ESYSREPO_API GitMngr : public log::User
{
public:
    typedef std::shared_ptr<GitBase> (*NewPtrFct)();

    //! Constructor
    GitMngr();

    //! Destructor
    ~GitMngr() override;

    static void set_new_ptr(NewPtrFct new_ptr_fct);
    static NewPtrFct get_new_ptr();
    static std::shared_ptr<GitBase> new_ptr();

private:
    //!< \cond DOXY_IMPL
    static NewPtrFct m_new_ptr_fct;
    //!< \endcond
};

} // namespace repo

} // namespace esys
