/*!
 * \file esys/repo/manifest/type_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/type.h"
#include "esys/repo/grepo/manifest.h"
#include "esys/repo/manifest/base.h"

namespace esys::repo::manifest
{

Result convert(Type type, std::string &text)
{
    switch (type)
    {
        case Type::ESYSREPO_MANIFEST: text = "esysrepo"; break;
        case Type::GIT_SUPER_PROJECT: text = "git_super_project"; break;
        case Type::GOOGLE_MANIFEST: text = "grepo"; break;
        default: return ESYSREPO_RESULT(ResultCode::MANIFEST_TYPE_UNKNOWN);
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

Result convert(const std::string &text, Type &type)
{
    if (text == manifest::Base::get_folder_name())
        type = Type::ESYSREPO_MANIFEST;
    else if (text == "git_super_project")
        type = Type::GIT_SUPER_PROJECT;
    else if (text == grepo::Manifest::get_folder_name())
        type = Type::GOOGLE_MANIFEST;
    else
    {
        type = Type::UNKNOWN;
        return ESYSREPO_RESULT(ResultCode::MANIFEST_TYPE_UNKNOWN);
    }
    return ESYSREPO_RESULT(ResultCode::OK);
}

} // namespace esys::repo::manifest
