/*!
 * \file esys/repo/manifest/test/multifilexml01_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/manifest/multifilexml.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::manifest::test
{

/*! \class MultiFileXML01Manifest esys/repo/manifest/test/multifilexml01_manifest.cpp
 * "esys/repo/manifest/test/multifilexml01_manifest.cpp"
 *
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(MultiFileXML01Manifest)
{
    boost::filesystem::path path;

    path = repo::test::TestCaseCtrl::get().GetTempFilesFolder();
    path /= ("multifilexmlwrite01manifest");
    path = path.normalize().make_preferred();

    bool remove_all = true;

    try
    {
        if (boost::filesystem::exists(path)) boost::filesystem::remove_all(path);
    }
    catch (const boost::filesystem::filesystem_error &e)
    {
        remove_all = false;
        std::cerr << e.what() << std::endl;
    }

    ESYSTEST_REQUIRE_EQUAL(remove_all, true);

    bool folder_created = boost::filesystem::create_directories(path);
    ESYSTEST_REQUIRE_EQUAL(folder_created, true);

    MultiFileXML xml_file_write;
    auto write_capture = std::make_shared<Capture>();
    xml_file_write.set_capture(write_capture);

    write_capture->add_item("src/esysbuild", "branch_x");
    write_capture->add_item("src/esyscibuild", "branch_y");
    auto capture_item = std::make_shared<Capture::Item>();
    capture_item->set_path("src/esysfile");
    capture_item->set_revision("branch_z");
    capture_item->set_remote_name("new_remote");
    capture_item->set_remote_url(path.string());
    write_capture->add_item(capture_item);

    path /= "multi.xml";

    Result result = xml_file_write.write(path.make_preferred().string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    MultiFileXML xml_file_read;
    auto read_capture = std::make_shared<Capture>();
    xml_file_read.set_capture(read_capture);

    bool equal = (*read_capture == *write_capture);
    ESYSTEST_REQUIRE_EQUAL(equal, false);

    result = xml_file_read.read(path.make_preferred().string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    equal = (*read_capture == *write_capture);
    ESYSTEST_REQUIRE_EQUAL(equal, true);

    std::ostringstream oss;
    result = xml_file_write.write(oss);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    std::istringstream iss;
    iss.str(oss.str());
    read_capture = std::make_shared<Capture>();
    xml_file_read.set_capture(read_capture);

    equal = (*read_capture == *write_capture);
    ESYSTEST_REQUIRE_EQUAL(equal, false);

    result = xml_file_read.read(iss);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
    equal = (*read_capture == *write_capture);
    ESYSTEST_REQUIRE_EQUAL(equal, true);
}

} // namespace esys::repo::manifest::test
