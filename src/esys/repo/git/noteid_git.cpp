/*!
 * \file esys/repo/git/noteid_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/noteid.h"

namespace esys::repo::git
{

NoteId::NoteId() = default;

NoteId::~NoteId() = default;

void NoteId::set_reference(const std::string &reference)
{
    m_reference = reference;
}

const std::string &NoteId::get_reference() const
{
    return m_reference;
}

void NoteId::set_commit_hash(const CommitHash &commit_hash)
{
    m_commit_hash = commit_hash;
}

const CommitHash &NoteId::get_commit_hash() const
{
    return m_commit_hash;
}

CommitHash &NoteId::get_commit_hash()
{
    return m_commit_hash;
}

bool NoteId::operator==(const NoteId &other) const
{
    bool result = CommitHash::operator==(other);
    if (!result) return false;
    if (get_reference() != other.get_reference()) return false;
    if (get_commit_hash() != other.get_commit_hash()) return false;
    return true;
}

bool NoteId::operator!=(const NoteId &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::NoteId &note_id)
{
    os << "note hash   : " << (const esys::repo::git::CommitHash &)note_id << std::endl;
    os << "reference   : " << note_id.get_reference() << std::endl;
    os << "commit hash : " << note_id.get_commit_hash() << std::endl;
    return os;
}

} // namespace std
