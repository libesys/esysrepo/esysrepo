/*!
 * \file esys/repo/exe/cmdsync.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/exe/cmd.h"

#include "esys/repo/gitbase.h"

#include <string>
#include <vector>
#include <memory>

//<swig_inc/>

//<swig>%shared_ptr(esys::repo::exe::CmdSync);</swig>

namespace esys::repo::exe
{

class ESYSREPO_API CmdSync : public Cmd
{
public:
    CmdSync();
    ~CmdSync() override;

    //! Set if the manifest is known to be a git super project
    /*!
     *  \param[in] git_super_project if true, the "manifest" to use is a git super project
     */
    void set_force(bool force);
    bool get_force() const;

    void set_branch(const std::string &branch);
    const std::string &get_branch() const;

    void set_alt_address(bool alt_address);
    bool get_alt_address() const;

    void set_multi(bool multi);
    bool get_multi() const;

    Result sync_manifest();
    Result handle_capture_item(std::shared_ptr<manifest::Capture::Item> item);
    Result do_multi();
    void set_git_folder_path(const std::string &git_folder_path);
    const std::string &get_git_folder_path() const;

protected:
    //!< \cond DOXY_IMPL
    Result impl_run() override;

private:
    bool m_force = false;
    std::string m_branch;
    bool m_alt_address = false;
    bool m_multi = false;
    std::string m_git_folder_path;
    //!< \endcond
};

} // namespace esys::repo::exe
