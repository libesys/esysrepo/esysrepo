/*!
 * \file esys/repo/libgit2/guardequal_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/libgit2/guardequal.h"
#include "esys/repo/libgit2/guard.h"

namespace esys::repo::libgit2
{

template<>
ESYSREPO_API bool guard_equal<git_remote>(const Guard<git_remote> &left, const Guard<git_remote> &right)
{
    if (git_remote_name(left.get()) != git_remote_name(right.get())) return false;
    if (git_remote_url(left.get()) != git_remote_url(right.get())) return false;
    return true;
}

} // namespace esys::repo::libgit2
