/*!
 * \file esys/repo/cli/cmdmanifest_cli.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/cli/cmdstatus.h"

#include <msword2md/cpp/string.h>

#include <termcolor/termcolor.hpp>

#include <boost/filesystem.hpp>

#include <vector>

namespace esys::repo::cli
{

#include "esys/repo/cli/cmdstatus_doc.cpp"

CmdStatus::CmdStatus(AppBase *app)
    : BaseType(app, "status", "Show the working tree status")
{
}

CmdStatus::~CmdStatus() = default;

std::shared_ptr<po::options_description> CmdStatus::get_desc()
{
    auto desc = Cmd::get_desc();
    if (desc != nullptr) return desc;

    desc = std::make_shared<po::options_description>("Status options");
    // clang-format off
    desc->add_options()
        ("help,h", "produce help message")
        ("quiet,q", po::value<bool>()->default_value(false)->implicit_value(true), "Only print the name of modified projects")
        ;
    // clang-format on
    Cmd::set_desc(desc);
    return desc;
}

int CmdStatus::configure_cmd(CmdType &cmd)
{
    if (get_vm()["quiet"].as<bool>()) cmd.set_quiet(true);

    return 0;
}

int CmdStatus::print_doc(std::ostream &os)
{
    os << cmdstatus_doc_strings;
    return 0;
}

} // namespace esys::repo::cli
