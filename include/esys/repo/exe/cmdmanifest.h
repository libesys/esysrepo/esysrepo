/*!
 * \file esys/repo/exe/cmdmanifest.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021-2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/exe/cmd.h"
#include "esys/repo/git/commit.h"

#include <memory>
#include <ostream>
#include <vector>

namespace esys::repo::exe
{

/*! \class CmdManifest esys/repo/exe/cmdmanifest.h "esys/repo/exe/cmdmanifest.h"
 * \brief Manifest command
 */
class ESYSREPO_API CmdManifest : public Cmd
{
public:
    enum class Task
    {
        NOT_SET,
        REVISION_AS_HEAD,
        LIST_DIRS,
        LIST_PROJECTS,
        ADD_DIR,
        DEL_DIR,
    };

    class ESYSREPO_API TaskDelDir
    {
    public:
        TaskDelDir();

        void set_name(const std::string &name);
        const std::string &get_name() const;

    private:
        std::string m_name;
    };

    class ESYSREPO_API TaskAddDir : public TaskDelDir
    {
    public:
        TaskAddDir();

        void set_url_ssh(const std::string &url_ssh);
        const std::string &get_url_ssh() const;

        void set_url_https(const std::string &url_https);
        const std::string &get_url_https() const;

    private:
        std::string m_url_ssh;
        std::string m_url_https;
    };

    //! Default constructor
    CmdManifest();

    //! Destructor
    ~CmdManifest() override;

    void set_task(Task task);
    Task get_task() const;

    void set_task_add_dir(const TaskAddDir &task_add_dir);
    const TaskAddDir &get_task_add_dir() const;

    void set_task_del_dir(const TaskDelDir &task_del_dir);
    const TaskDelDir &get_task_del_dir() const;

    void set_dirs(const std::string &dirs);
    const std::string &get_dirs() const;

    void set_output_file(const std::string &output_file);
    const std::string &get_output_file() const;

    Result update_revision_as_head();
    Result update_revision_as_head(std::shared_ptr<manifest::Repository> repo);

    Result do_revision_as_head();
    Result do_list_dirs();
    Result do_list_projects();
    Result do_add_dir();
    Result do_del_dir();

protected:
    //!< \cond DOXY_IMPL
    Result impl_run() override;

private:
    Task m_task = Task::NOT_SET;
    std::string m_output_file;
    std::string m_dirs;
    TaskAddDir m_task_add_dir;
    TaskDelDir m_task_del_dir;
    //!< \endcond;
};

} // namespace esys::repo::exe
