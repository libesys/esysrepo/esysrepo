/*!
 * \file esys/repo/git/signature_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/signature.h"

namespace esys::repo::git
{

Signature::Signature() = default;

Signature::~Signature() = default;

void Signature::set_date_time(const std::chrono::system_clock::time_point &date_time)
{
    m_date_time = date_time;
}

const std::chrono::system_clock::time_point &Signature::get_date_time() const
{
    return m_date_time;
}

void Signature::set_offset(int offset)
{
    m_offset = offset;
}

int Signature::get_offset() const
{
    return m_offset;
}

bool Signature::operator==(const Signature &other) const
{
    if (!Person::operator==(other)) return false;
    if (get_date_time() != other.get_date_time()) return false;
    if (get_offset() != other.get_offset()) return false;
    return true;
}

bool Signature::operator!=(const Signature &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git
