/*!
 * \file esys/repo/workspace.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/loadfolder.h"
#include "esys/repo/manifest.h"
#include "esys/repo/result.h"
#include "esys/repo/manifest/loader.h"

#include <esys/log/user.h>

#include <string>

//<swig_inc/>

namespace esys::repo
{

/*! \class Workspace esys/repo/workspace.h "esys/repo/workspace.h"
 * \brief Handle the workspace
 */
class ESYSREPO_API Workspace : public log::User
{
public:
    enum class Kind
    {
        NOT_SET,
        ESYSREPO,
        GREPO,
    };

    Workspace();
    ~Workspace() override;

    void set_path(const std::string &path);
    const std::string &get_path() const;

    void set_folder_path(const std::string &folder_path);
    const std::string &get_folder_path() const;

    void set_kind(Kind kind);
    Kind get_kind() const;

    Result find(const std::string &path = "");
    Result load(const std::string &path = "");

    std::string get_full_path(std::shared_ptr<manifest::Repository> repo) const;

    void set_load_folder(std::shared_ptr<LoadFolder> load_folder);
    std::shared_ptr<LoadFolder> get_load_folder() const;
    std::shared_ptr<ConfigFolder> get_config_folder() const;
    std::shared_ptr<Manifest> get_manifest() const;

    static bool is_grepo_folder(const std::string &path);
    static bool is_esysrepo_folder(const std::string &path);

private:
    std::string m_path;
    std::string m_folder_path;
    Kind m_kind = Kind::NOT_SET;
    std::shared_ptr<LoadFolder> m_load_folder;

    std::shared_ptr<ConfigFolder> m_config_folder;
    std::shared_ptr<Manifest> m_manifest;
};

} // namespace esys::repo
