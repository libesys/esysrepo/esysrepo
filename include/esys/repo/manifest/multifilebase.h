/*!
 * \file esys/repo/manifest/multifilebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/capture.h"
#include "esys/repo/result.h"

#include <string>

namespace esys::repo::manifest
{

/*! \class MultiFileBase esys/repo/manifest/multifilebase.h "esys/repo/manifest/multifilebase.h"
 * \brief To store the content of a Capture instance to a file
 */
class ESYSREPO_API MultiFileBase
{
public:
    //! Default constructor
    MultiFileBase();

    //! Destructor
    virtual ~MultiFileBase();

    //! Set the Capture data
    /*!
     * \param[in] capture the capture data
     */
    void set_capture(std::shared_ptr<Capture> capture);

    //! Get the Capture data
    /*!
     * \return the capture data
     */
    std::shared_ptr<Capture> get_capture() const;

    //! Read the capture from a file
    /*!
     * \param[in] path the path of the manifest
     * \return ResultCode::OK if successful, otherwise an error
     */
    virtual Result read(const std::string &path) = 0;

    //! Read the capture from an input stream
    /*!
     * \param[in] is the input stream
     * \return ResultCode::OK if successful, otherwise an error
     */
    virtual Result read(std::istream &is) = 0;

    //! Write a Capture to a file
    /*!
     * \param[in] path the path of the Capture data
     * \return 0 if successful, < 0 otherwise
     */
    virtual Result write(const std::string &path) = 0;

    //! Write a Capture to a stream
    /*!
     * \param[in] os the stream
     */
    virtual Result write(std::ostream &os) = 0;

private:
    //!< \cond DOXY_IMPL
    std::string m_filename;
    std::shared_ptr<Capture> m_capture; //!< The abstract data of a manifest
    //!< \endcond
};

} // namespace esys::repo::manifest
