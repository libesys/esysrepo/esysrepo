/*!
 * \file esys/repo/manifest/base.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/filebase.h"

#include <string>

namespace esys
{

namespace repo
{

namespace manifest
{

/*! \class Base esys/repo/manifest/base.h "esys/repo/manifest/base.h"
 * \brief Base class for all ESysRepo manifest file format
 */
class ESYSREPO_API Base : public FileBase
{
public:
    //! Default constructor
    Base();

    //! Destructor
    ~Base() override;

    static const std::string &get_folder_name();

private:
    //!< \cond DOXY_IMPL
    static const std::string s_folder_name;
    //!< \endcond
};

} // namespace manifest

} // namespace repo

} // namespace esys
