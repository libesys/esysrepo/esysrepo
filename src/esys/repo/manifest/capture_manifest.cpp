/*!
 * \file esys/repo/manifest/capture_capture.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/capture.h"

namespace esys::repo::manifest
{

Capture::Capture() = default;

Capture::~Capture() = default;

void Capture::add_item(const std::string &path, const std::string &revision)
{
    auto item = std::make_shared<Item>(path, revision);
    m_map_path_item[path] = item;
    m_items.push_back(item);
}

void Capture::add_item(std::shared_ptr<Item> item)
{
    m_items.push_back(item);
}

void Capture::clear()
{
    m_items.clear();
    m_map_path_item.clear();
}

std::shared_ptr<Capture::Item> Capture::find_item(const std::string &path) const
{
    auto it = m_map_path_item.find(path);

    if (it == m_map_path_item.end()) return nullptr;
    return it->second;
}

std::vector<std::shared_ptr<Capture::Item>> &Capture::get_items()
{
    return m_items;
}
const std::vector<std::shared_ptr<Capture::Item>> &Capture::get_items() const
{
    return m_items;
}

std::map<std::string, std::shared_ptr<Capture::Item>, std::less<>> &Capture::get_map()
{
    return m_map_path_item;
}

const std::map<std::string, std::shared_ptr<Capture::Item>, std::less<>> &Capture::get_map() const
{
    return m_map_path_item;
}

bool Capture::operator==(const Capture &other) const
{
    if (get_items().size() != other.get_items().size()) return false;

    for (auto idx = 0; idx < get_items().size(); ++idx)
    {
        if ((get_items()[idx] == nullptr) || (other.get_items()[idx] == nullptr)) return false;
        if (*get_items()[idx] != *other.get_items()[idx]) return false;
    }
    return true;
}

bool Capture::operator!=(const Capture &other) const
{
    return !operator==(other);
}

Capture::Item::Item() = default;

Capture::Item::Item(const std::string &path, const std::string &revision)
    : m_path(path)
    , m_revision(revision)
{
}

Capture::Item::~Item() = default;

void Capture::Item::set_path(const std::string &path)
{
    m_path = path;
}

const std::string &Capture::Item::get_path() const
{
    return m_path;
}

void Capture::Item::set_revision(const std::string &revision)
{
    m_revision = revision;
}

const std::string &Capture::Item::get_revision() const
{
    return m_revision;
}

void Capture::Item::set_remote_name(const std::string &remote_name)
{
    m_remote_name = remote_name;
}

const std::string &Capture::Item::get_remote_name() const
{
    return m_remote_name;
}

void Capture::Item::set_remote_url(const std::string &remote_url)
{
    m_remote_url = remote_url;
}

const std::string &Capture::Item::get_remote_url() const
{
    return m_remote_url;
}

bool Capture::Item::operator==(const Item &other) const
{
    if (get_path() != other.get_path()) return false;
    if (get_revision() != other.get_revision()) return false;
    if (get_remote_name() != other.get_remote_name()) return false;
    if (get_remote_url() != other.get_remote_url()) return false;

    return true;
}

bool Capture::Item::operator!=(const Item &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::manifest
