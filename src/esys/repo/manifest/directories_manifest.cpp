/*!
 * \file esys/repo/manifest/directories_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/directories.h"

namespace esys::repo::manifest
{

Directories::Item::Item() = default;

Directories::Item::~Item() = default;

void Directories::Item::set_directory(std::shared_ptr<Directory> directory)
{
    m_directory = directory;
}

std::shared_ptr<Directory> Directories::Item::get_directory() const
{
    return m_directory;
}

bool Directories::Item::operator==(const Item &other) const
{
    if (Directory::Item::operator!=(other)) return false;
    if ((get_directory() == nullptr) && (other.get_directory() == nullptr)) return true;
    if ((get_directory() != nullptr) && (other.get_directory() != nullptr))
    {
        return *get_directory() == *other.get_directory();
    }
    return false;
}

bool Directories::Item::operator!=(const Item &other) const
{
    return !operator==(other);
}

Directories::Directories() = default;

Directories::~Directories() = default;

void Directories::add_item(const std::string &name, std::shared_ptr<Item> item)
{
    m_items.push_back(item);
    m_map_items[name] = item;
}

Result Directories::del_item(const std::string &name)
{
    auto it = m_map_items.find(name);
    if (it == m_map_items.end()) return ESYSREPO_RESULT(ResultCode::GENERIC_ERROR);

    m_map_items.erase(name);
    for (auto it = m_items.begin(); it != m_items.end(); ++it)
    {
        if ((*it)->get_name() == name)
        {
            m_items.erase(it);
            return ESYSREPO_RESULT(ResultCode::OK);
        }
    }
    return ESYSREPO_RESULT(ResultCode::GENERIC_ERROR);
}

const std::vector<std::shared_ptr<Directories::Item>> &Directories::get_items() const
{
    return m_items;
}

std::shared_ptr<Directories::Item> Directories::find_item(const std::string &name) const
{
    auto it = m_map_items.find(name);
    if (it == m_map_items.end()) return nullptr;
    return it->second;
}

std::shared_ptr<Directory::Item> Directories::find_project(const std::string &name) const
{
    auto it = m_map_projects.find(name);
    if (it == m_map_projects.end()) return nullptr;
    return it->second;
}

void Directories::add_projects(std::shared_ptr<Directory> directory)
{
    for (auto project : directory->get_items())
    {
        m_map_projects[project->get_name()] = project;
    }
}

void Directories::clear()
{
    m_items.clear();
    m_map_items.clear();
    m_map_projects.clear();
}

void Directories::add_default_libesys()
{
    if (find_item("libesys") != nullptr) return;

    auto item = std::make_shared<manifest::Directories::Item>();
    item->set_name("libesys");
    item->set_url_ssh("ssh://git@gitlab.com/libesys/esysrepo/directory");
    item->set_url_https("https://gitlab.com/libesys/esysrepo/directory");
    add_item("libesys", item);
}

bool Directories::operator==(const Directories &other) const
{
    if (get_items().size() != other.get_items().size()) return false;

    for (auto idx = 0; idx < get_items().size(); ++idx)
    {
        if (*get_items()[idx] != *other.get_items()[idx]) return false;
    }
    return true;
}

bool Directories::operator!=(const Directories &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::manifest
