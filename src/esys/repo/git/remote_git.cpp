/*!
 * \file esys/repo/git/remote_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/remote.h"

namespace esys::repo::git
{

Remote::Remote() = default;

Remote::~Remote() = default;

void Remote::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &Remote::get_name() const
{
    return m_name;
}

void Remote::set_url(const std::string &url)
{
    m_url = url;
}

const std::string &Remote::get_url() const
{
    return m_url;
}

bool Remote::operator==(const Remote &other) const
{
    if (get_name() != other.get_name()) return false;
    if (get_url() != other.get_url()) return false;
    return true;
}

bool Remote::operator!=(const Remote &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::Remote &remote)
{
    os << "name : " << remote.get_name() << std::endl;
    os << "url  : " << remote.get_url();
    return os;
}

} // namespace std
