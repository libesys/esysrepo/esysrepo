/*!
 * \file esys/repo/exe/cmdenv.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/exe/cmd.h"

#include "esys/repo/gitbase.h"

#include <string>
#include <vector>
#include <memory>

namespace esys::repo::exe
{

class ESYSREPO_API CmdEnv : public Cmd
{
public:
    CmdEnv();
    ~CmdEnv() override;

protected:
    //!< \cond DOXY_IMPL
    Result impl_run() override;

private:
    //!< \endcond
};

} // namespace esys::repo::exe
