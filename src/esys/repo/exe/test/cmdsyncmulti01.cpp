/*!
 * \file esys/repo/exe/test/cmdsyncmulti01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"
#include "esys/repo/exe/test/fixcmd.h"

#include <esys/repo/exe/cmdmulti.h>
#include <esys/repo/libgit2/git.h>
#include <esys/repo/manifest/multifilexml.h>

namespace esys::repo::exe::test
{

/*! \class CmdMultiCapture01 esys/repo/exe/test/cmdsyncmulti01.cpp
 * "esys/repo/exe/test/cmdsyncmulti01.cpp"
 *
 * \brief
 */
ESYSTEST_AUTO_TEST_CASE(CmdSyncMulti01)
{
    // The new branch which will be inside the capture
    const std::string new_branch = "branch_a_b_c";

    auto &ctrl = repo::test::TestCaseCtrl::get();
    // Create a temporary folder fo all files
    boost::filesystem::path file_path = ctrl.delete_create_temp_folder("cmdsyncmulti01");
    ESYSTEST_REQUIRE_EQUAL(file_path.string().empty(), false);

    // Create a temporary folder where the superbuild will be synced
    boost::filesystem::path repo_orig_path = file_path / "repo_orig";
    auto result_bool = boost::filesystem::create_directories(repo_orig_path);
    ESYSTEST_REQUIRE(result_bool);

    // Get the superbuild repos
    FixCmd m_fix_cmd_sync;

    m_fix_cmd_sync.set_temp_sub_folder("cmdsyncmulti01");
    m_fix_cmd_sync.set_manifest_url("ssh://git@gitlab.com/libesys/esysrepo/test_google_manifest.git");
    m_fix_cmd_sync.set_sub_folder_for_repo("repo_orig");
    m_fix_cmd_sync.run();

    // Open the git repo where the capture will be created
    libgit2::Git git_root_repo;

    auto result = git_root_repo.open(repo_orig_path.string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    // Create the capture
    auto capture = std::make_shared<manifest::Capture>();

    capture->add_item(".", new_branch);

    // Create the XML file holding the capture data
    manifest::MultiFileXML multi_xml;

    multi_xml.set_capture(capture);
    std::ostringstream oss;
    result = multi_xml.write(oss);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    // Add a author, othwerwise adding a note will fail
    auto person = std::make_shared<git::Person>();

    std::string email = "esysrepo@libesys.org";
    std::string name = "ESysRepo";
    person->set_email(email);
    person->set_name(name);
    git_root_repo.set_author(person);

    // Add the note with the capture data
    git::NoteId note_id;
    result = git_root_repo.add_note(note_id, "esysrepo", oss.str(), true);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    // Close the repo
    result = git_root_repo.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    // Do the sync with multi
    auto cmd_sync = m_fix_cmd_sync.get_cmd_sync();
    cmd_sync.set_multi(true);

    result = cmd_sync.run();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    // Re-open the repository to see if the capture was done correctly
    result = git_root_repo.open(repo_orig_path.string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    git::Branch branch;
    git::Remote remote;

    // Get info about the branch and remote at the head
    result = git_root_repo.get_head_branch_remote(branch, remote);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    // Close the repo
    result = git_root_repo.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    // Test that the capture did checkout the correct branch
    ESYSTEST_REQUIRE_EQUAL(branch.get_name(), new_branch);
}

} // namespace esys::repo::exe::test
