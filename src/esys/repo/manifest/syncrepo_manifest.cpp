/*!
 * \file esys/repo/manifest/syncrepo_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/syncrepo.h"
#include "esys/repo/manifest/location.h"

#include "esys/repo/filesystem.h"

#include <esys/trace/call.h>
#include <esys/trace/macros.h>

#include <boost/filesystem.hpp>

#include <sstream>

namespace esys::repo::manifest
{

SyncRepo::SyncRepo()
    : log::User()
{
}

SyncRepo::~SyncRepo() = default;

int SyncRepo::run()
{
    int result = 0;
    ETRC_CALL_RET_NP(result);

    return ETRC_RET(process_repo());
}

int SyncRepo::process_repo()
{
    int result = 0;
    ETRC_CALL_RET_NP(result);

    boost::filesystem::path path = get_config_folder()->get_workspace_path();
    if (get_repo()->get_path() != ".") path /= get_repo()->get_path();

    if (!GitBase::is_repo(path.string())) return ETRC_RET(clone());
    return ETRC_RET(fetch_update());
}

bool SyncRepo::has_branch(GitHelper &git_helper, const std::string &branch)
{
    bool result_bool = false;
    ETRC_CALL_RET(result_bool, branch);

    git::Branches branches;
    Result result = git_helper.get_branches(branches, git::BranchType::REMOTE, log::Level::DEBUG);
    if (result.error())
    {
        git_helper.error("Couldn't get the list of local branches");
        return ETRC_RET(false);
    }

    branches.sort();

    auto branch_ptr = branches.find(branch);

    if (branch_ptr != nullptr)
    {
        git_helper.debug(0, "Branch found : " + branch);
        return ETRC_RET(true);
    }
    git_helper.debug(0, "Branch not found : " + branch);
    return ETRC_RET(false);
}

std::string SyncRepo::get_checkout_revision(GitHelper &git_helper)
{
    std::string result;
    ETRC_CALL_RET_NP(result);

    if (get_branch().empty()) return ETRC_RET(get_repo()->get_revision());

    Result_t<bool> branch_exists =
        git_helper.has_branch(get_branch(), git::BranchType::REMOTE, esys::log::Level::DEBUG);

    std::ostringstream oss;
    oss << "branch_exists = " << branch_exists;
    debug(0, oss.str());

    if (branch_exists) return ETRC_RET(get_branch());
    return ETRC_RET(get_repo()->get_revision());
}

std::shared_ptr<GitHelper> SyncRepo::new_git_helper()
{
    auto git_helper = std::make_shared<GitHelper>(get_git(), get_logger_if(), static_cast<int>(get_repo_idx()));

    get_git()->set_logger_if(git_helper);
    return git_helper;
}

int SyncRepo::clone()
{
    int result = 0;
    ETRC_CALL_RET_NP(result);

    std::shared_ptr<GitHelper> git_helper = new_git_helper();
    std::string url = get_repo_url();
    std::string branch_to_checkout;

    boost::filesystem::path path = get_config_folder()->get_workspace_path();
    ETRC_MSG("path : " + get_repo()->get_path());

    if (get_repo()->get_path() != ".")
    {
        // A simple clone can be made
        path /= get_repo()->get_path();
        Result rresult = git_helper->clone(url, path.string(), false, log::Level::INFO);
        if (rresult.error())
        {
            git_helper->close_on_error(log::Level::DEBUG);
            return ETRC_RET(rresult.get_result_code_int());
        }

        if (!get_branch().empty())
        {
            Result result = git_helper->fetch(get_log_level());
            if (result.error())
            {
                git_helper->close_on_error(log::Level::DEBUG);
                return ETRC_RET(result.get_result_code_int());
            }
        }

        branch_to_checkout = get_checkout_revision(*git_helper.get());
        git_helper->debug(0, "branch_to_checkout = " + branch_to_checkout);
        if (branch_to_checkout.empty()) return ETRC_RET(0);

        rresult = git_helper->checkout(branch_to_checkout, false, log::Level::INFO);
        if (rresult.error())
        {
            git_helper->close_on_error(log::Level::DEBUG);
            return ETRC_RET(rresult.get_result_code_int());
        }
        rresult = git_helper->close(log::Level::DEBUG);
        return ETRC_RET(rresult.get_result_code_int());
    }

    // Here since the folder where to clone is not empty so a temporary folder is used
    boost::filesystem::path temp_path = get_config_folder()->get_temp_path();
    std::ostringstream oss;
    oss << "repo_temp" << get_repo_idx();
    temp_path /= oss.str();

    Result rresult = git_helper->clone(url, temp_path.string(), path.string(), true, get_log_level());
    if (rresult.error())
    {
        git_helper->close_on_error(log::Level::DEBUG);
        return ETRC_RET(rresult.get_result_code_int());
    }

    rresult = git_helper->open(path.string(), esys::log::INFO);
    if (rresult.error())
    {
        return ETRC_RET(rresult.get_result_code_int());
    }

    if (!get_branch().empty())
    {
        rresult = git_helper->fetch(get_log_level());
        if (rresult.error())
        {
            git_helper->close_on_error(log::Level::DEBUG);
            return ETRC_RET(rresult.get_result_code_int());
        }
    }

    branch_to_checkout = get_checkout_revision(*git_helper.get());
    git_helper->debug(0, "branch_to_checkout = " + branch_to_checkout);
    if (branch_to_checkout.empty()) return ETRC_RET(0);

    rresult = git_helper->checkout(branch_to_checkout, get_force(), log::Level::INFO);
    if (rresult.error())
    {
        git_helper->close_on_error(log::Level::DEBUG);
        return ETRC_RET(rresult.get_result_code_int());
    }
    rresult = git_helper->close(log::Level::DEBUG);
    return ETRC_RET(rresult.get_result_code_int());
}

int SyncRepo::fetch_update()
{
    int result_int = 0;
    ETRC_CALL_RET_NP(result_int);

    std::shared_ptr<GitHelper> git_helper = new_git_helper();
    git_helper->set_display_repo_idx(get_display_repo_idx());

    boost::filesystem::path path = get_config_folder()->get_workspace_path();
    if (get_repo()->get_path() != ".") path /= get_repo()->get_path();

    Result rresult = git_helper->open(path.string(), log::Level::INFO);
    if (rresult.error()) return ETRC_RET(rresult.get_result_code_int());

    bool dirty = false;
    rresult = git_helper->is_dirty(dirty, log::Level::DEBUG);
    if (rresult.error()) return ETRC_RET(rresult.get_result_code_int());

    if (dirty)
    {
        git_helper->warn("Changes detected in repo, no sync.");
        return ETRC_RET(0);
    }

    if (get_branch().empty())
        return ETRC_RET(normal_sync(*git_helper.get()));
    else
        return ETRC_RET(branch_sync(*git_helper.get()));
}

int SyncRepo::normal_sync(GitHelper &git_helper)
{
    int result_int = 0;
    ETRC_CALL_RET_NP(result_int);

    bool detached = false;
    Result result = git_helper.is_detached(detached, log::Level::DEBUG);
    if (result.error())
    {
        git_helper.error("Couldn't detect if the git repo is detached or not.");
        return ETRC_RET(result.get_result_code_int());
    }

    if (detached)
    {
        git_helper.warn("Sync repo aborted: the git repo is detached.");
        return ETRC_RET(-2);
    }

    result = git_helper.fetch(log::Level::DEBUG);
    if (result.error())
    {
        git_helper.error("Fetch failed on the git repo");
        return ETRC_RET(result.get_result_code_int());
    }

    git::Branches branches;
    result = git_helper.get_branches(branches, git::BranchType::LOCAL, log::Level::DEBUG);
    if (result.error())
    {
        git_helper.error("Couldn't get the list of local branches");
        return ETRC_RET(result.get_result_code_int());
    }

    branches.sort();

    if (branches.get_head() == nullptr)
    {
        git_helper.error("Couldn't get the HEAD.");
        return ETRC_RET(-4);
    }

    std::vector<std::string> refs;
    git::MergeAnalysisResult merge_analysis_result;
    std::vector<git::CommitHash> commits;

    refs.push_back(branches.get_head()->get_remote_branch());

    result = git_helper.merge_analysis(refs, merge_analysis_result, commits, log::Level::DEBUG);
    if (result.error())
    {
        git_helper.error("Merge analysis failed.");
        return ETRC_RET(-5);
    }

    if (merge_analysis_result == git::MergeAnalysisResult::UP_TO_DATE)
    {
        git_helper.info("Git repo up to date.");
        return ETRC_RET(0);
    }
    if (merge_analysis_result != git::MergeAnalysisResult::FASTFORWARD)
    {
        git_helper.warn("Sync manifest aborted: can't be fastforwarded.");
        return ETRC_RET(-6);
    }

    // For a fastforward, there should be only one commit
    if (commits.size() != 1)
    {
        git_helper.error("Fast forward failed.");
        return ETRC_RET(-7);
    }

    git_helper.info("Fast forwarding git repo ...");

    result = git_helper.fastforward(commits[0], log::Level::DEBUG);
    if (result.error())
    {
        git_helper.error("Fast forward failed.");
        return ETRC_RET(-8);
    }
    git_helper.info("Fast forward completed.");
    return ETRC_RET(0);
}

int SyncRepo::branch_sync(GitHelper &git_helper)
{
    int result_int = false;
    ETRC_CALL_RET_NP(result_int);

    if (!has_branch(git_helper, get_branch())) return ETRC_RET(normal_sync(git_helper));

    Result result = git_helper.checkout(get_branch(), get_force(), log::Level::DEBUG);
    if (result.ok())
        info("Checkout branch " + get_branch() + ".");
    else
        error("Failed to checkout branch " + get_branch() + ".");
    return ETRC_RET(result.get_result_code_int());
}

void SyncRepo::set_log_level(log::Level log_level)
{
    m_log_level = log_level;
}

log::Level SyncRepo::get_log_level()
{
    return m_log_level;
}

void SyncRepo::set_repo_idx(std::size_t repo_idx)
{
    m_repo_idx = repo_idx;
}

std::size_t SyncRepo::get_repo_idx() const
{
    return m_repo_idx;
}

std::size_t &SyncRepo::get_repo_idx()
{
    return m_repo_idx;
}

void SyncRepo::set_display_repo_idx(bool display_repo_idx)
{
    m_display_repo_idx = display_repo_idx;
}

bool SyncRepo::get_display_repo_idx() const
{
    return m_display_repo_idx;
}

std::string SyncRepo::get_repo_url()
{
    assert(get_repo() != nullptr);
    assert(get_repo()->get_location() != nullptr);

    std::string url;
    if (!get_alt_address())
        url = get_repo()->get_location()->get_address();
    else
        url = get_repo()->get_location()->get_alt_address();

    url += "/" + get_repo()->get_name();

    return url;
}

void SyncRepo::set_branch(const std::string &branch)
{
    m_branch = branch;
}

const std::string &SyncRepo::get_branch() const
{
    return m_branch;
}

void SyncRepo::set_alt_address(bool alt_address)
{
    m_alt_address = alt_address;
}

bool SyncRepo::get_alt_address() const
{
    return m_alt_address;
}

void SyncRepo::set_force(bool force)
{
    m_force = force;
}

bool SyncRepo::get_force() const
{
    return m_force;
}

} // namespace esys::repo::manifest
