/*!
 * \file esys/repo/manifest/kind.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/result.h"

#include <string>

namespace esys
{

namespace repo
{

namespace manifest
{

enum class Kind
{
    NOT_SET,
    UNKNOWN,
    ISOLATED, //!< Manifest is in its own git repo
    EMBEDDED  //!< Manifest is embedded in a git repo with source code
};

Result convert(Kind type, std::string &text);
Result convert(const std::string &text, Kind &kind);

} // namespace manifest

} // namespace repo

} // namespace esys
