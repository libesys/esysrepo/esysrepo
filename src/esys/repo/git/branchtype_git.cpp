/*!
 * \file esys/repo/git/branchtype_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/branchtype.h"

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, esys::repo::git::BranchType branch_type)
{
    switch (branch_type)
    {
        case esys::repo::git::BranchType::NOT_SET: os << "NOT_SET"; break;
        case esys::repo::git::BranchType::LOCAL: os << "LOCAL"; break;
        case esys::repo::git::BranchType::REMOTE: os << "REMOTE"; break;
        case esys::repo::git::BranchType::ALL: os << "ALL"; break;
        default: os << "<unknonwn>";
    }
    return os;
}

} // namespace std
