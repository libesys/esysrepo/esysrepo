/*!
 * \file esys/repo/gitstats/dataperiodauthor.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/person.h"

#include <map>
#include <memory>
#include <string>

//<swig_inc/>

namespace esys::repo::gitstats
{

/*! \class DataPeriodAuthor esys/repo/gitstats/dataperiodauthor.h "esys/repo/gitstats/dataperiodauthor.h"
 * \brief
 */
class ESYSREPO_API DataPeriodAuthor
{
public:
    //! Default constructor
    DataPeriodAuthor();

    //! Constructor
    DataPeriodAuthor(std::shared_ptr<git::Person>);

    //! Destructor
    ~DataPeriodAuthor();

    void set_author(std::shared_ptr<git::Person> author);
    std::shared_ptr<git::Person> get_author();
    const std::shared_ptr<git::Person> get_author() const;

    void set_commit_count(unsigned int commit_count);
    unsigned int get_commit_count() const;
    unsigned int &get_commit_count();
    void inc_commit_count();

    void inc_line_added(unsigned int count);
    unsigned int get_line_added() const;

    void inc_line_removed(unsigned int count);
    unsigned int get_line_removed() const;

    void inc_renames(unsigned int count);
    unsigned int get_renames() const;

    void inc_files_changed(unsigned int count);
    unsigned int get_files_changed() const;

private:
    //!< \cond DOXY_IMPL
    std::shared_ptr<git::Person> m_author;
    unsigned int m_commit_count = 0;
    unsigned int m_line_added = 0;
    unsigned int m_line_removed = 0;
    unsigned int m_renames = 0;
    unsigned int m_files_changed = 0;
    //!< \endcond
};

} // namespace esys::repo::gitstats
