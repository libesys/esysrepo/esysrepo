/*!
 * \file esys/repo/cli/cmdinit.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/cli/cmd_t.h"
#include "esys/repo/exe/cmdinit.h"

namespace esys::repo::cli
{

class ESYSREPO_API CmdInit : public Cmd_t<exe::CmdInit>
{
public:
    using BaseType = Cmd_t<exe::CmdInit>;

    explicit CmdInit(AppBase *app = nullptr);
    ~CmdInit() override;

    int configure_cmd(CmdType &cmd) override;
    std::shared_ptr<po::options_description> get_desc() override;

    int print_doc(std::ostream &os) override;
};

} // namespace esys::repo::cli
