/*!
 * \file esys/repo/githelper.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/githelper.h"
#include "esys/repo/filesystem.h"

#include <esys/log/consolelockguard.h>

#include <esys/trace/call.h>
#include <esys/trace/macros.h>

#include <boost/filesystem.hpp>

#include <sstream>
#include <iostream>

namespace std
{

static ostream &operator<<(ostream &os, const vector<string> &strings)
{
    os << "{" << std::endl;

    for (auto idx = 0; idx < strings.size(); ++idx)
    {
        if (idx != 0) os << "," << std::endl;
        os << "    " << strings[idx];
    }
    os << std::endl << "}";
    return os;
}

} // namespace std

namespace esys::repo
{

GitHelper::GitHelper(std::shared_ptr<GitBase> git, std::shared_ptr<Logger_if> log_if, int repo_idx)
    : log::User()
    , m_git(git)
    , m_repo_idx(repo_idx)
{
    set_logger_if(log_if);
}

GitHelper::~GitHelper()
{
    if (get_auto_close() && get_git() && get_git()->is_open()) close_on_error(log::Level::DEBUG);
}

void GitHelper::debug(int level, const std::string &msg)
{
    std::ostringstream oss;

    init_oss(oss, msg);

    clean_cout();
    log::User::debug(level, oss.str());
    init_oss_clear();
}

void GitHelper::info(const std::string &msg)
{
    std::ostringstream oss;

    init_oss(oss, msg);

    clean_cout();
    log::User::info(oss.str());
    init_oss_clear();
}

void GitHelper::warn(const std::string &msg)
{
    std::ostringstream oss;

    init_oss(oss, msg);

    clean_cout();
    log::User::warn(oss.str());
    init_oss_clear();
}

void GitHelper::error(const std::string &msg)
{
    std::ostringstream oss;

    init_oss(oss, msg);

    clean_cout();
    log::User::error(oss.str());
    init_oss_clear();
}

void GitHelper::critical(const std::string &msg)
{
    std::ostringstream oss;

    init_oss(oss, msg);

    clean_cout();
    log::User::critical(oss.str());
    init_oss_clear();
}

void GitHelper::log(log::Level level, const std::string &msg)
{
    std::ostringstream oss;

    init_oss(oss, msg);

    clean_cout();
    log::User::log(level, oss.str());
    init_oss_clear();
}

void GitHelper::log(const std::string &msg, log::Level level, int debug_level)
{
    std::ostringstream oss;

    init_oss(oss, msg);

    clean_cout();
    log::User::log(oss.str(), level, debug_level);
    init_oss_clear();
}

void GitHelper::error(const std::string &msg, int result)
{
    std::ostringstream oss;

    init_oss(oss, msg);

    clean_cout();
    log::User::error(oss.str(), result);
    init_oss_clear();
}

void GitHelper::done(const std::string &msg, const std::string &rel_path, uint64_t elapsed_time)
{
    ETRC_CALL(msg, rel_path, elapsed_time);

    std::ostringstream oss;

    init_oss(oss, msg);

    oss << " done.\n";
    oss << "    path : " << rel_path << "\n";
    oss << "    elapsed time (s): " << (elapsed_time / THOUSAND) << "." << (elapsed_time % THOUSAND);
    clean_cout();
    log::User::info(oss.str());
}

Result GitHelper::open(const std::string &folder, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET(result, folder, log_level, debug_level);

    boost::filesystem::path rel_folder = boost::filesystem::relative(folder);

    std::string msg = "Opening ...\n    path  : " + rel_folder.string();

    log(msg, log_level, debug_level);

    result = get_git()->open(rel_folder.string());
    if (result.error()) error("Failed with error ", result.get_result_code_int());
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::clone(const std::string &url, const std::string &path, bool do_close, log::Level log_level,
                        int debug_level)
{
    Result result;
    ETRC_CALL_RET(result, url, path, do_close, log_level, debug_level);
    /*boost::filesystem::path rel_path = boost::filesystem::relative(path);

    std::string msg = "Cloning ...\n    url  : " + url + "\n    path : " + rel_path.string();

    log(msg, log_level, debug_level);

    int result = get_git()->clone(url, path);
    if (result < 0)
        error("Failed with error ", result);
    else
        done("Cloning", get_git()->get_last_cmd_elapsed_time());
    if (!do_close) return result;
    return close(log::Level::DEBUG);*/
    result = clone_branch(url, "", path, do_close, log_level, debug_level);
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::clone_branch(const std::string &url, const std::string &branch, const std::string &path,
                               bool do_close, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET(result, url, branch, path, do_close, log_level, debug_level);

    boost::filesystem::path rel_path = boost::filesystem::relative(path);

    std::string msg = "Cloning ...\n    path : " + rel_path.string();
    if (!branch.empty()) msg += "\n    rev  : " + branch;
    msg += "\n    url  : " + url;

    log(msg, log_level, debug_level);

    result = get_git()->clone(url, path);
    if (result.ok()) done("Cloning", rel_path.string(), get_git()->get_last_cmd_elapsed_time());
    /*else
        error("Failed with error ", result); */

    if (!do_close) return ETRC_RET(ESYSREPO_RESULT(result));
    return ETRC_RET(ESYSREPO_RESULT(close(log::Level::DEBUG)));
}

Result GitHelper::clone(const std::string &url, const std::string &temp_path, const std::string &path, bool do_close,
                        log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET(result, url, temp_path, path, do_close, log_level, debug_level);

    auto start_time = std::chrono::steady_clock::now();

    boost::filesystem::path rel_temp_path = boost::filesystem::relative(temp_path);

    if (boost::filesystem::exists(temp_path))
    {
        if (log_level == log::Level::DEBUG)
        {
            std::ostringstream oss;

            init_oss(oss);
            oss << "Remove all: " << rel_temp_path.string();

            int result = log_wrap(boost_no_all::remove_all, log::Level::DEBUG)(oss.str(), temp_path);
            if (result < 0)
            {
                std::string err_str = "Failed to remove all : " + rel_temp_path.string();
                error(err_str);
                return ETRC_RET(ESYSREPO_RESULT(ResultCode::RAW_INT_ERROR, result, err_str));
            }
        }
        else
        {
            int result = boost_no_all::remove_all(temp_path);
            if (result < 0)
            {
                std::string err_str = "Failed to delete path : " + temp_path;
                error(err_str);
                return ETRC_RET(ESYSREPO_RESULT(ResultCode::RAW_INT_ERROR, result, err_str));
            }
        }
    }

    std::string clone_msg;
    boost::filesystem::path rel_path = boost::filesystem::relative(path);

    clone_msg = "Cloning ...\n    path : " + rel_path.string() + "\n    url  : " + url;
    log(clone_msg, log::Level::INFO);

    if (log_level == log::Level::DEBUG)
    {
        clone_msg = "Cloning ...\n    path : " + rel_temp_path.string() + "\n    url  : " + url;
        log(clone_msg, log::Level::DEBUG);
    }

    result = get_git()->clone(url, temp_path);
    if (result.error())
    {
        error("Failed to clone");
        return ETRC_RET(ESYSREPO_RESULT(result));
    }

    if (do_close) close_on_error(log::Level::DEBUG);

    if (log_level == log::Level::DEBUG)
    {
        std::ostringstream oss;

        init_oss(oss);
        oss << "Move:\n    src  : " << rel_temp_path.string() << "\n    dest : " << rel_path.string();

        int result = log_wrap(boost_no_all::move, log::Level::DEBUG)(oss.str(), temp_path, path, true);
        if (result < 0) return ETRC_RET(ESYSREPO_RESULT(ResultCode::RAW_INT_ERROR, result));
    }
    else
    {
        int result = boost_no_all::move(temp_path, path, true);
        if (result == -1)
        {
            std::string err_str = "Failed to copy from temp_path to path : " + temp_path + " -> " + path;
            error(err_str);
            return ETRC_RET(ESYSREPO_RESULT(ResultCode::RAW_INT_ERROR, result, err_str));
        }
        else if (result == -2)
            warn("Failed to delete temp_path : " + temp_path);
    }
    auto stop_time = std::chrono::steady_clock::now();
    auto d_milli = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time).count();

    done("Cloning", rel_path.string(), static_cast<uint64_t>(d_milli));
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

Result GitHelper::close(log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET(result, log_level, debug_level);

    if (!get_git()->is_open()) return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));

    log("Closing ...", log_level, debug_level);

    result = get_git()->close();
    std::string err_str;
    if (result.error())
    {
        err_str = "Close git repo failed with error ";
        error("Close git repo failed with error ", result.get_result_code_int());
    }
    else
        log("Closed.", log_level, debug_level);

    return ETRC_RET(ESYSREPO_RESULT(result, err_str));
}

void GitHelper::close_on_error(log::Level log_level, int debug_level)
{
    if (!get_git()->is_open())
    {
        log("Called instenal_close on not open repo ", log_level, debug_level);
        return;
    }

    log("Closing ...", log_level, debug_level);

    auto result = get_git()->close();
    std::string err_str;
    if (result.error())
    {
        error("Close git repo failed with error ", result.get_result_code_int());
    }
    else
        log("Closed.", log_level, debug_level);
}

Result GitHelper::fastforward(const git::CommitHash &commit, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET(result, commit, log_level, debug_level);

    log("Fast forwarding ...", log_level, debug_level);

    result = get_git()->fastforward(commit);
    if (result.error())
        error("Failed with error ", result.get_result_code_int());
    else
        log("Fast forwarded successfully.", log_level, debug_level);

    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::fetch(const std::string &remote, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET(result, remote, log_level, debug_level);

    log("Fetching ...", log_level, debug_level);

    result = get_git()->fetch(remote);
    if (result.error())
        error("Failed with error ", result.get_result_code_int());
    else
        log("Fetched successfully.", log_level, debug_level);

    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::fetch(log::Level log_level, int debug_level)
{
    return fetch("", log_level, debug_level);
}

Result GitHelper::get_branches(git::Branches &branches, git::BranchType branch_type, log::Level log_level,
                               int debug_level)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, branches, branch_type, log_level, debug_level);
    ETRC_CALL_RET_OUT_END(branches);

    log("Getting branches ...", log::Level::DEBUG);
    result = get_git()->get_branches(branches, branch_type);
    if (result.error())
        error("Failed with error ", result.get_result_code_int());
    else
        log("Got branches.", log::Level::DEBUG);

    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result_t<bool> GitHelper::has_branch(const std::string &name, git::BranchType branch_type, log::Level log_level,
                                     int debug_level)
{
    Result_t<bool> result(false);
    ETRC_CALL_RET(result, name, branch_type, log_level, debug_level);

    log("Check branch existance " + name + " ...", log::Level::DEBUG);
    result = get_git()->has_branch(name, branch_type);
    if (result)
        log("Found it.", log::Level::DEBUG);
    else
        log("Not Found", log::Level::DEBUG);
    return ETRC_RET(ESYSREPO_RESULT_T(result));
}

Result GitHelper::get_hash(const std::string &revision, std::string &hash, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, revision, hash, log_level, debug_level);
    ETRC_CALL_RET_OUT_END(hash);

    log("Check remote hash for " + revision + " ...", log::Level::DEBUG);
    result = get_git()->get_hash(revision, hash, git::BranchType::REMOTE);
    if (result.ok())
        log("Found it.", log::Level::DEBUG);
    else
        log("Not Found", log::Level::DEBUG);
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::get_hash_local(const std::string &revision, std::string &hash, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, revision, hash, log_level, debug_level);
    ETRC_CALL_RET_OUT_END(hash);

    log("Check local hash for " + revision + " ...", log::Level::DEBUG);
    result = get_git()->get_hash(revision, hash, git::BranchType::LOCAL);
    if (result.ok())
        log("Found it.", log::Level::DEBUG);
    else
        log("Not Found", log::Level::DEBUG);
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::is_dirty(bool &dirty, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, dirty, log_level, debug_level);
    ETRC_CALL_RET_OUT_END(dirty);

    result = get_git()->is_dirty(dirty);
    if (result.error())
        error("Failed to check there are changes in the git repo.");
    else
    {
        if (dirty)
            log("Dirty git repo", log_level, debug_level);
        else
            log("No changes in the git repo", log_level, debug_level);
    }
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::is_detached(bool &detached, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, detached, log_level, debug_level);
    ETRC_CALL_RET_OUT_END(detached);

    result = get_git()->is_detached(detached);
    if (result.error())
        error("Failed to check if the git repo is detached or not.");
    else
    {
        if (detached)
            log("Git repo is detached", log_level, debug_level);
        else
            log("Git repo is not detached", log_level, debug_level);
    }
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::get_status(git::RepoStatus &status, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, status, log_level, debug_level);
    ETRC_CALL_RET_OUT_END(status);

    result = get_git()->get_status(status);
    if (result.error())
        error("Failed to get the repo status");
    else
        log("Succeeded to get repo status", log_level, debug_level);

    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::checkout(const std::string &branch, bool force, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET(result, branch, force, log_level, debug_level);

    result = get_git()->checkout(branch, force);
    if (result.error())
        error("Failed to checkout");
    else
        log("Checkout succeeded", log_level, debug_level);
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::merge_analysis(const std::vector<std::string> &refs, git::MergeAnalysisResult &merge_analysis_result,
                                 std::vector<git::CommitHash> &commits, log::Level log_level, int debug_level)
{
    Result result;
    ETRC_CALL_RET_BEGIN(result, refs, merge_analysis_result, commits, log_level, debug_level);
    ETRC_CALL_RET_OUT_END(merge_analysis_result, commits);

    result = get_git()->merge_analysis(refs, merge_analysis_result, commits);
    if (result.error())
        error("Merge analysis failed");
    else
        log("Merge analysis succeeded", log_level, debug_level);
    return ETRC_RET(ESYSREPO_RESULT(result));
}

Result GitHelper::move(const std::string &src, const std::string &dst, bool recursive, log::Level log_level,
                       int debug_level)
{
    Result rresult;
    ETRC_CALL_RET(rresult, src, dst, recursive, log_level, debug_level);

    boost::filesystem::path rel_src = boost::filesystem::relative(src);
    boost::filesystem::path rel_dst = boost::filesystem::relative(dst);
    std::ostringstream oss;

    init_oss(oss);
    oss << "Move:\n    src  : " << rel_src.string() << "\n    dest : " << rel_dst.string();

    int result = log_wrap(boost_no_all::move, log_level)(oss.str(), rel_src.string(), rel_dst.string(), true);
    if (result == -1) return ETRC_RET(ESYSREPO_RESULT(ResultCode::FAILED_TO_COPY));
    if (result == -1) return ETRC_RET(ESYSREPO_RESULT(ResultCode::FAILED_TO_REMOVE_ALL));
    return ETRC_RET(ESYSREPO_RESULT(ResultCode::OK));
}

void GitHelper::set_git(std::shared_ptr<GitBase> git)
{
    m_git = git;
}

std::shared_ptr<GitBase> GitHelper::get_git() const
{
    return m_git.lock();
}

void GitHelper::set_auto_close(bool auto_close)
{
    m_auto_close = auto_close;
}

bool GitHelper::get_auto_close() const
{
    return m_auto_close;
}

void GitHelper::set_repo_idx(int repo_idx)
{
    m_repo_idx = repo_idx;
}

int GitHelper::get_repo_idx() const
{
    return m_repo_idx;
}

void GitHelper::set_display_repo_idx(bool display_repo_idx)
{
    m_display_repo_idx = display_repo_idx;
}

bool GitHelper::get_display_repo_idx() const
{
    return m_display_repo_idx;
}

void GitHelper::init_oss_done()
{
    m_init_oss_done = true;
}

void GitHelper::init_oss_clear()
{
    m_init_oss_done = false;
}

bool GitHelper::is_init_oss_done()
{
    return m_init_oss_done;
}

void GitHelper::init_oss(std::ostringstream &oss)
{
    if (is_init_oss_done()) return;

    oss.str("");
    if ((get_repo_idx() != -1) && (get_display_repo_idx())) oss << "[" << get_repo_idx() << "] ";
    init_oss_done();
}

void GitHelper::init_oss(std::ostringstream &oss, const std::string &msg)
{
    init_oss(oss);

    oss << msg;
}

void GitHelper::clean_cout()
{
    log::ConsoleLockGuard<log::User> lock(this);

    std::string s = "             ";
    std::cout << "\r";

    for (int idx = 0; idx < CLEAN_OUT_REPETITION; ++idx) std::cout << s;
    std::cout << "\r";
}

GitHelper::AutoClose::AutoClose(GitHelper *git_helper)
    : m_git_helper(git_helper)
{
}

GitHelper::AutoClose::~AutoClose()
{
    if (m_git_helper != nullptr) m_git_helper->close(log::Level::DEBUG);
}

} // namespace esys::repo
