/*!
 * \file esys/repo/libgit2/gitimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/libgit2/git.h"
#include "esys/repo/libgit2/libgit2.h"
#include "esys/repo/libgit2/guard.h"
#include "esys/repo/libgit2/notes.h"
#include "esys/repo/git/diffdelta.h"
#include "esys/repo/git/note.h"
#include "esys/repo/ssh.h"

#include <esys/log/logger_if.h>

#include <git2.h>

#include <memory>

namespace esys::repo::libgit2
{

class ESYSREPO_API GitImpl
{
public:
    GitImpl(Git *self);
    ~GitImpl();

    Result open(const std::string &folder);
    bool is_open();

    Result init_bare(const std::string &folder_path);
    Result close();
    Result get_remotes(std::vector<git::Remote> &remotes);
    Result get_head_branch_remote(git::Branch &branch, git::Remote &remote);
    Result add_remote(const std::string &name, const std::string &url);
    Result get_branches(git::Branches &branches, git::BranchType branch_type = git::BranchType::LOCAL);

    Result clone(const std::string &url, const std::string &path, const std::string &branch = "");
    Result checkout(const std::string &branch, bool force = false);
    Result reset(const git::CommitHash &commit, git::ResetType type = git::ResetType::SOFT);
    Result fastforward(const git::CommitHash &commit);

    Result get_last_commit(git::CommitHash &commit);
    Result get_last_commit(git::Commit &commit, bool get_all_notes = false);

    Result get_parent_commit(const git::CommitHash &commit, git::CommitHash &parent, int nth_parent = 1);

    Result is_dirty(bool &dirty);
    Result is_detached(bool &detached);

    Result get_status(git::RepoStatus &repo_status);
    Result handle_status_entry(git::RepoStatus &repo_status, const git_status_entry *status_entry);
    Result handle_status_entry_current(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                       const git_status_entry *status_entry);
    Result handle_status_entry_index(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                     const git_status_entry *status_entry);
    Result handle_status_entry_work_dir(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                        const git_status_entry *status_entry);
    Result handle_status_entry_conflicted(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                          const git_status_entry *status_entry);
    Result handle_status_entry_ignored(git::RepoStatus &repo_status, std::shared_ptr<git::Status> status,
                                       const git_status_entry *status_entry);

    Result from_to(git_status_t status, std::shared_ptr<git::Status> status_ptr);
    Result from_to(const git_diff_delta *delta, git::DiffDelta &diff_delta);
    Result from_to(const git_diff_file &file, git::DiffFile &diff_file);

    int check_error(int result, const std::string &action = "", bool show_result = true);
    Result check_error(Result result, bool show_result = true);

    const std::string &get_version();
    const std::string &get_lib_name();

    static const std::string &s_get_version();
    static const std::string &s_get_lib_name();

    static const std::string &s_get_ssh_version();
    static const std::string &s_get_ssh_lib_name();

    Git *self() const;

    Result_t<bool> is_ssh_agent_running();

    Result merge_analysis(const std::vector<std::string> &refs, git::MergeAnalysisResult &merge_analysis_result,
                          std::vector<git::CommitHash> &commits);

    Result fetch(const std::string &remote);

    Result fetch_all_notes(const std::string &remote = "");

    Result add_note(git::NoteId &note_id, const std::string &reference, const std::string &message,
                    bool overwrite = false);

    Result remove_note(const git::NoteId &note_id);
    Result remove_note(const git::CommitHash &commit_hash, const std::string &reference);
    Result remove_note_last_commit(const std::string &reference);

    Result get_refspec(std::string &refspec_str, const std::string &src_ref, const std::string &dst_ref);
    Result push(const std::string &remote, const std::string &src_ref = "", const std::string &dst_ref = "");
    Result push_notes(const std::string &remote, const std::string &reference = "");

    Result_t<bool> has_branch(const std::string &name, git::BranchType branch_type = git::BranchType::LOCAL);
    Result get_hash(const std::string &revision, std::string &hash,
                    git::BranchType branch_type = git::BranchType::REMOTE);

    Result walk_commits(std::shared_ptr<git::WalkCommit> walk_commit);
    Result diff(const git::CommitHash &commit_hash, std::shared_ptr<git::Diff> diff);
    Result get_ahead_behind(git::AheadBehind &ahead_behind, const git::Branch &local_branch);
    Result get_ahead_behind(git::AheadBehind &ahead_behind, const std::string &first_ref,
                            const std::string &second_ref);

    Result treeish_to_tree(Guard<git_tree> &tree, git_repository *repo, const char *treeish);

    Result resolve_ref(git_annotated_commit **commit, const std::string &ref);
    Result resolve_ref(git_reference **ref, const std::string &ref_str);
    Result resolve_ref(git_reference **ref, git_annotated_commit **commit, const std::string &ref_str);

    Result find_ref(git_annotated_commit **commit, const std::string &ref, std::string &new_ref);
    Result find_ref(git_reference **ref, git_annotated_commit **commit, const std::string &ref_str,
                    std::string &new_ref);

    Result convert_bin_hex(const git_oid &oid, std::string &hex_str);
    Result convert_hex_bin(const std::string &hex_str, git_oid &oid);

    static void convert(git::BranchType branch_type, git_branch_t &list_flags);

    void set_agent_identity_path(const std::string &agent_identity_path);
    const std::string &get_agent_identity_path() const;

    void set_logger_if(std::shared_ptr<log::Logger_if> logger_if);

    git_repository *get_repo();

    Result get_signature(const git_signature *signature_obj, git::Signature &signature);

    static std::string normalize_notes_ref(const std::string &reference);

    Result get_sigs(git_signature **author, git_signature **committer);

    void close_on_error();

private:
    static std::unique_ptr<LibGit2> s_libgt2;

    static int libgit2_credentials_cb(git_credential **out, const char *url, const char *name, unsigned int types,
                                      void *payload);
    static int libgit2_sideband_progress_cb(const char *str, int len, void *data);
    static int libgit2_transfer_progress_cb(const git_indexer_progress *stats, void *payload);
    static int libgit2_update_tips_cb(const char *refname, const git_oid *a, const git_oid *b, void *data);
    static int libgit2_pack_progress_cb(int stage, uint32_t current, uint32_t total, void *payload);

    Result get_commit(const git_oid &oid_commit, git::Commit &commit, bool get_all_notes = false);
    Result get_commit_hash(const git_oid &oid_commit, git::CommitHash &commit_hash);

    Result get_notes(git_commit *commit_obj, git::Commit &commit, const std::string &notes_ref);
    Result find_remote(Guard<git_remote> &remote, const std::string &remote_name = "");

    Result get_commit_hash_from_branch(const std::string &branch, git_oid &oid);

    Git *m_self = nullptr;
    git_repository *m_repo = nullptr;
    Guard<git_credential> m_credential;
    SSH m_ssh;
    std::string m_agent_identity_path;
    Notes m_notes;
};

} // namespace esys::repo::libgit2
