/*!
 * \file esys/repo/manifest/capturefilexml_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/multifilexml.h"
#include "esys/repo/manifest/multifilexmlimpl.h"

namespace esys::repo::manifest
{

MultiFileXML::MultiFileXML()
    : MultiFileBase()
{
    m_impl = std::make_unique<MultiFileXMLImpl>(this);
}

MultiFileXML::~MultiFileXML() = default;

Result MultiFileXML::read(const std::string &path)
{
    return get_impl()->read(path);
}

Result MultiFileXML::read(std::istream &is)
{
    return get_impl()->read(is);
}

Result MultiFileXML::write(const std::string &path)
{
    return get_impl()->write(path);
}

Result MultiFileXML::write(std::ostream &os)
{
    return get_impl()->write(os);
}

MultiFileXMLImpl *MultiFileXML::get_impl() const
{
    return m_impl.get();
}

} // namespace esys::repo::manifest
