/*!
 * \file esys/repo/git/status_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/status.h"

namespace esys::repo::git
{

Status::Status() = default;

Status::~Status() = default;

void Status::set_type(StatusType type)
{
    m_type = type;
}

StatusType Status::get_type() const
{
    return m_type;
}

StatusType &Status::get_type()
{
    return m_type;
}

void Status::set_sub_type(StatusSubType sub_type)
{
    m_sub_type = sub_type;
}

StatusSubType Status::get_sub_type() const
{
    return m_sub_type;
}

StatusSubType &Status::get_sub_type()
{
    return m_sub_type;
}

DiffDelta &Status::get_diff_delta()
{
    return m_diff_delta;
}

const DiffDelta &Status::get_diff_delta() const
{
    return m_diff_delta;
}

bool Status::operator==(const Status &other) const
{
    if (get_type() != other.get_type()) return false;
    if (get_sub_type() != other.get_sub_type()) return false;
    if (get_diff_delta() != other.get_diff_delta()) return false;
    return true;
}

bool Status::operator!=(const Status &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git
