/*!
 * \file esys/repo/libgit2/git_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/libgit2/git.h"
#include "esys/repo/libgit2/gitimpl.h"

namespace esys::repo::libgit2
{

bool Git::s_detect_ssh_agent_done = false;
bool Git::s_ssh_agent_running = false;

std::shared_ptr<GitBase> Git::new_ptr()
{
    return std::make_shared<Git>();
}

Git::Git()
    : GitBase()
{
    m_impl = std::make_unique<GitImpl>(this);
}

Git::~Git() = default;

Result Git::open(const std::string &folder)
{
    set_folder(folder);
    return get_impl()->open(folder);
}

bool Git::is_open()
{
    return get_impl()->is_open();
}

Result Git::close()
{
    return get_impl()->close();
}

Result Git::init_bare(const std::string &folder_path)
{
    return get_impl()->init_bare(folder_path);
}

void Git::close_on_error()
{
    return get_impl()->close_on_error();
}

Result Git::get_remotes(std::vector<git::Remote> &remotes)
{
    return get_impl()->get_remotes(remotes);
}

Result Git::get_head_branch_remote(git::Branch &branch, git::Remote &remote)
{
    return get_impl()->get_head_branch_remote(branch, remote);
}

Result Git::add_remote(const std::string &name, const std::string &url)
{
    return get_impl()->add_remote(name, url);
}

Result Git::get_branches(git::Branches &branches, git::BranchType branch_type)
{
    return get_impl()->get_branches(branches, branch_type);
}

Result Git::clone(const std::string &url, const std::string &path, const std::string &branch)
{
    set_url(url);
    return get_impl()->clone(url, path, branch);
}

Result Git::checkout(const std::string &branch, bool force)
{
    return get_impl()->checkout(branch, force);
}

Result Git::reset(const git::CommitHash &commit, git::ResetType type)
{
    return get_impl()->reset(commit, type);
}

Result Git::fastforward(const git::CommitHash &commit)
{
    return get_impl()->fastforward(commit);
}

Result Git::get_last_commit(git::CommitHash &commit)
{
    return get_impl()->get_last_commit(commit);
}

Result Git::get_last_commit(git::Commit &commit, bool get_all_notes)
{
    return get_impl()->get_last_commit(commit, get_all_notes);
}

Result Git::get_parent_commit(const git::CommitHash &commit, git::CommitHash &parent, int nth_parent)
{
    return get_impl()->get_parent_commit(commit, parent, nth_parent);
}

Result Git::is_dirty(bool &dirty)
{
    return get_impl()->is_dirty(dirty);
}

Result Git::is_detached(bool &detached)
{
    return get_impl()->is_detached(detached);
}

Result Git::get_status(git::RepoStatus &repo_status)
{
    return m_impl->get_status(repo_status);
}

Result_t<bool> Git::is_ssh_agent_running(bool log_once)
{
    if (!s_detect_ssh_agent_done) detect_ssh_agent(log_once);

    return s_ssh_agent_running;
}

void Git::detect_ssh_agent(bool log_once)
{
    if (s_detect_ssh_agent_done) return;

    s_detect_ssh_agent_done = true;
    s_ssh_agent_running = m_impl->is_ssh_agent_running();

    if (s_ssh_agent_running && log_once) info("SSH agent detected");
}

Result Git::merge_analysis(const std::vector<std::string> &refs, git::MergeAnalysisResult &merge_analysis_result,
                           std::vector<git::CommitHash> &commits)
{
    return m_impl->merge_analysis(refs, merge_analysis_result, commits);
}

Result Git::fetch(const std::string &remote)
{
    return m_impl->fetch(remote);
}

Result Git::fetch_all_notes(const std::string &remote)
{
    return m_impl->fetch_all_notes(remote);
}

Result Git::add_note(git::NoteId &note_id, const std::string &reference, const std::string &message, bool overwrite)
{
    return m_impl->add_note(note_id, reference, message, overwrite);
}

Result Git::remove_note(const git::NoteId &note_id)
{
    return m_impl->remove_note(note_id);
}

Result Git::remove_note(const git::CommitHash &commit_hash, const std::string &reference)
{
    return m_impl->remove_note(commit_hash, reference);
}

Result Git::remove_note_last_commit(const std::string &reference)
{
    return m_impl->remove_note_last_commit(reference);
}

Result Git::push(const std::string &remote, const std::string &src_ref, const std::string &dst_ref)
{
    return m_impl->push(remote, src_ref, dst_ref);
}

Result Git::push_notes(const std::string &remote, const std::string &reference)
{
    return m_impl->push_notes(remote, reference);
}

Result_t<bool> Git::has_branch(const std::string &name, git::BranchType branch_type)
{
    return m_impl->has_branch(name, branch_type);
}

Result Git::get_hash(const std::string &revision, std::string &hash, git::BranchType branch_type)
{
    return m_impl->get_hash(revision, hash, branch_type);
}

Result Git::walk_commits(std::shared_ptr<git::WalkCommit> walk_commit)
{
    return m_impl->walk_commits(walk_commit);
}

Result Git::diff(const git::CommitHash &commit_hash, std::shared_ptr<git::Diff> diff)
{
    return m_impl->diff(commit_hash, diff);
}

Result Git::get_ahead_behind(git::AheadBehind &ahead_behind, const git::Branch &local_branch)
{
    return m_impl->get_ahead_behind(ahead_behind, local_branch);
}

Result Git::get_ahead_behind(git::AheadBehind &ahead_behind, const std::string &first_ref,
                             const std::string &second_ref)
{
    return m_impl->get_ahead_behind(ahead_behind, first_ref, second_ref);
}

void Git::set_url(const std::string &url)
{
    m_url = url;
}

const std::string &Git::get_url()
{
    return m_url;
}

void Git::set_folder(const std::string &folder)
{
    m_folder = folder;
}

const std::string &Git::get_folder()
{
    return m_folder;
}

const std::string &Git::get_version()
{
    return m_impl->get_version();
}

const std::string &Git::get_lib_name()
{
    return m_impl->get_lib_name();
}

void Git::debug(int level, const std::string &msg)
{
    // log::ConsoleLockGuard<log::User> lock(this);
    //! \TODO must be fixed to allow writing after all console is written

    GitBase::debug(level, msg);
}

const std::string &Git::s_get_version()
{
    return GitImpl::s_get_version();
}

const std::string &Git::s_get_lib_name()
{
    return GitImpl::s_get_lib_name();
}

const std::string &Git::s_get_ssh_version()
{
    return GitImpl::s_get_ssh_version();
}

const std::string &Git::s_get_ssh_lib_name()
{
    return GitImpl::s_get_ssh_lib_name();
}

GitImpl *Git::get_impl() const
{
    return m_impl.get();
}

void Git::set_logger_if(std::shared_ptr<log::Logger_if> logger_if)
{
    get_impl()->set_logger_if(logger_if);
}

} // namespace esys::repo::libgit2
