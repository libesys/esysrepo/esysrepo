/*!
 * \file esys/repo/git/status.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/statustype.h"
#include "esys/repo/git/statussubtype.h"
#include "esys/repo/git/diffdelta.h"

#include <string>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class Status esys/repo/git/status.h "esys/repo/git/status.h"
 * \brief Status information about a file in a git repo
 */
class ESYSREPO_API Status
{
public:
    //! Default constructor
    Status();

    //! Destructor
    virtual ~Status();

    void set_type(StatusType type);
    StatusType get_type() const;
    StatusType &get_type();

    void set_sub_type(StatusSubType sub_type);
    StatusSubType get_sub_type() const;
    StatusSubType &get_sub_type();

    DiffDelta &get_diff_delta();
    const DiffDelta &get_diff_delta() const;

    //! Equal to comparison operator
    bool operator==(const Status &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Status &other) const;

private:
    //!< \cond DOXY_IMPL
    StatusType m_type = StatusType::NOT_SET;
    StatusSubType m_sub_type = StatusSubType::NOT_SET;
    DiffDelta m_diff_delta;
    //!< \endcond
};

} // namespace esys::repo::git
