/*!
 * \file esys/repo/progresscallbackbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/progresscallbackbase.h"

namespace esys::repo
{

ProgressCallbackBase::ProgressCallbackBase() = default;

ProgressCallbackBase::~ProgressCallbackBase() = default;

} // namespace esys::repo
