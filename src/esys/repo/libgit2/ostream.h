/*!
 * \file esys/repo/libgit2/ostream.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <git2.h>

#include <ostream>

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, git_remote *data);

} // namespace std
