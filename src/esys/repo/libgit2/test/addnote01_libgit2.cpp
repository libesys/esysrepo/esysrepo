/*!
 * \file esys/repo/libgit2/test/addnote01_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/libgit2/git.h>

#include <libssh2.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::libgit2::test
{

/*! \class AddNote01LibGit2 esys/repo/libgit2/test/addnote01_libgit2.cpp
 * "esys/repo/libgit2/test/addnote01_libgit2.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(AddNote01LibGit2)
{
    auto &ctrl = repo::test::TestCaseCtrl::get();

    boost::filesystem::path file_path = ctrl.delete_create_temp_folder("addnote01libgit2");
    ESYSTEST_REQUIRE_EQUAL(file_path.string().empty(), false);

    Git git;

    Result result =
        git.clone("ssh://git@gitlab.com/libesys/esysrepo/test.git", file_path.normalize().make_preferred().string());
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    git.add_notes_ref("esysrepo");

    result = git.checkout("branch_notes");
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    git::Commit last_commit;

    result = git.get_last_commit(last_commit, true);
    if (result.error()) std::cout << "ERROR " << result << std::endl;

    std::vector<std::shared_ptr<git::Note>> notes;
    ESYSTEST_REQUIRE_EQUAL(last_commit.get_notes("esysrepo", notes), 0);
    ESYSTEST_REQUIRE_EQUAL(notes.size(), 0);

    auto person = std::make_shared<git::Person>();
    std::string message = "a new note";

    std::string email = "esysrepo@libesys.org";
    std::string name = "ESysRepo";
    person->set_email(email);
    person->set_name(name);
    git.set_author(person);

    git::NoteId note_id;
    result = git.add_note(note_id, "esysrepo", "a new note", true);
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.get_last_commit(last_commit, true);
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
    ESYSTEST_REQUIRE_EQUAL(last_commit.get_hash().get_hash(), "3c49ba347517df805d2a2df292c19bdfcec69015");
    ESYSTEST_REQUIRE_EQUAL(last_commit.get_summary(), "Set a_b_c");
    ESYSTEST_REQUIRE_EQUAL(last_commit.get_body(),
                           "String theory turns the page on the standard description of the universe\n"
                           "by replacing all matter and force particles with just one element:\n"
                           "tiny vibrating strings that twist and turn in complicated ways that,\n"
                           "from our perspective, look like particles.");

    notes.clear();
    ESYSTEST_REQUIRE_EQUAL(last_commit.get_notes("esysrepo", notes), 0);
    ESYSTEST_REQUIRE_EQUAL(notes.size(), 1);
    ESYSTEST_REQUIRE_EQUAL(notes[0]->get_message(), message);
    ESYSTEST_REQUIRE_EQUAL(notes[0]->get_author().get_name(), name);
    ESYSTEST_REQUIRE_EQUAL(notes[0]->get_author().get_email(), email);
    ESYSTEST_REQUIRE_EQUAL(notes[0]->get_committer().get_name(), name);
    ESYSTEST_REQUIRE_EQUAL(notes[0]->get_committer().get_email(), email);

    result = git.remove_note(note_id);
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    last_commit.clear();
    result = git.get_last_commit(last_commit, true);
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
    ESYSTEST_REQUIRE_EQUAL(last_commit.get_notes("esysrepo", notes), 0);
    ESYSTEST_REQUIRE_EQUAL(notes.size(), 0);

    result = git.add_note(note_id, "esysrepo", "a new new note", true);
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.remove_note_last_commit("esysrepo");
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
}

} // namespace esys::repo::libgit2::test
