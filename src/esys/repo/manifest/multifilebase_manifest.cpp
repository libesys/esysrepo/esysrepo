/*!
 * \file esys/repo/manifest/multifilebase_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/multifilebase.h"

namespace esys::repo::manifest
{

MultiFileBase::MultiFileBase() = default;

MultiFileBase::~MultiFileBase() = default;

void MultiFileBase::set_capture(std::shared_ptr<Capture> capture)
{
    m_capture = capture;
}

std::shared_ptr<Capture> MultiFileBase::get_capture() const
{
    return m_capture;
}

} // namespace esys::repo::manifest
