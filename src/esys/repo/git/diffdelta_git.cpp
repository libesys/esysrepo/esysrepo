/*!
 * \file esys/repo/git/diffdelta.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/diffdelta.h"

namespace esys::repo::git
{

DiffDelta::DiffDelta() = default;

DiffDelta::~DiffDelta() = default;

void DiffDelta::set_type(DiffDeltaType type)
{
    m_type = type;
}

DiffDeltaType DiffDelta::get_type() const
{
    return m_type;
}

void DiffDelta::set_similatiry(std::size_t similarity)
{
    m_similarity = similarity;
}

std::size_t DiffDelta::get_similarity() const
{
    return m_similarity;
}

void DiffDelta::set_file_count(std::size_t file_count)
{
    m_file_count = file_count;
}

std::size_t DiffDelta::get_file_count() const
{
    return m_file_count;
}

DiffFile &DiffDelta::get_old_file()
{
    return m_old_file;
}

const DiffFile &DiffDelta::get_old_file() const
{
    return m_old_file;
}

DiffFile &DiffDelta::get_new_file()
{
    return m_new_file;
}

const DiffFile &DiffDelta::get_new_file() const
{
    return m_new_file;
}

bool DiffDelta::operator==(const DiffDelta &other) const
{
    if (get_type() != other.get_type()) return false;
    if (get_similarity() != other.get_similarity()) return false;
    if (get_file_count() != other.get_file_count()) return false;
    if (get_old_file() != other.get_old_file()) return false;
    if (get_new_file() != other.get_new_file()) return false;
    return true;
}

bool DiffDelta::operator!=(const DiffDelta &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git
