/*!
 * \file esys/repo/libgit2/test/pushtobare01_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/libgit2/git.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::libgit2::test
{

/*! \class PushToBare01LibGit2 esys/repo/libgit2/test/pushtobare01_libgit2.cpp
 * "esys/repo/libgit2/test/pushtobare01_libgit2.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(PushToBare01LibGit2)
{
    auto &ctrl = repo::test::TestCaseCtrl::get();

    boost::filesystem::path file_path = ctrl.delete_create_temp_folder("pushtobare01libgit2");
    ESYSTEST_REQUIRE_EQUAL(file_path.string().empty(), false);

    boost::filesystem::path git_orig_path = file_path / "git_orig";
    auto result_bool = boost::filesystem::create_directories(git_orig_path);
    ESYSTEST_REQUIRE(result_bool);

    boost::filesystem::path bare_path = file_path / "bare";
    result_bool = boost::filesystem::create_directories(bare_path);
    ESYSTEST_REQUIRE(result_bool);

    Git git;

    Result result = git.clone("ssh://git@gitlab.com/libesys/esysrepo/test.git",
                              git_orig_path.normalize().make_preferred().string());
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.init_bare(bare_path.normalize().make_preferred().string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.open(git_orig_path.normalize().make_preferred().string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.add_remote("bare", bare_path.normalize().make_preferred().string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    //result = git.push("bare", "refs/heads/master:refs/heads/master");
    result = git.push("bare", "master");
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    boost::filesystem::path git_new_path = file_path / "git_new";
    result_bool = boost::filesystem::create_directories(git_new_path);
    ESYSTEST_REQUIRE(result_bool);

    result =
        git.clone(bare_path.normalize().make_preferred().string(), git_new_path.normalize().make_preferred().string());
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
}

} // namespace esys::repo::libgit2::test
