/*!
 * \file esys/repo/manifest/directorymngr_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/manifest/directorymngr.h"
#include "esys/repo/manifest/directoryfilexml.h"
#include "esys/repo/githelper.h"
#include "esys/repo/libgit2/git.h"

#include <boost/filesystem.hpp>

namespace esys::repo::manifest
{

DirectoryMngr::DirectoryMngr() = default;

DirectoryMngr::~DirectoryMngr() = default;

void DirectoryMngr::set_folder_path(const std::string &folder_path)
{
    m_folder_path = folder_path;
}

const std::string &DirectoryMngr::get_folder_path() const
{
    return m_folder_path;
}

void DirectoryMngr::set_directories(std::shared_ptr<Directories> directories)
{
    m_directories = directories;
}

std::shared_ptr<Directories> DirectoryMngr::get_directories() const
{
    return m_directories;
}

Result DirectoryMngr::load()
{
    if (get_folder_path().empty()) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);
    if (get_directories() == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    if (!boost::filesystem::exists(get_folder_path()))
    {
        bool result_bool = boost::filesystem::create_directories(get_folder_path());
        if (!result_bool) return ESYSREPO_RESULT(ResultCode::FOLDER_CREATION_ERROR, get_folder_path());
    }

    Result rresult;
    for (auto item : get_directories()->get_items())
    {
        auto result = load(item);
        if (result.error()) rresult.add(ESYSREPO_RESULT(result));
    }
    return ESYSREPO_RESULT(rresult);
}

Result DirectoryMngr::load(std::shared_ptr<Directories::Item> directories_item)
{
    if (directories_item == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);
    if (directories_item->get_url_ssh().empty()) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);

    Result result;
    auto name = directories_item->get_name();
    boost::filesystem::path git_folder_path = get_folder_path();
    git_folder_path /= name;
    boost::filesystem::path git_git_folder_path = git_folder_path / ".git";
    auto git = std::make_shared<libgit2::Git>();
    GitHelper git_helper;
    git_helper.set_git(git);
    git_helper.set_auto_close(true);

    if (!boost::filesystem::exists(git_git_folder_path))
    {
        result = git_helper.clone(directories_item->get_url_ssh(), git_folder_path.string(), true, log::Level::DEBUG);
        if (result.error()) return ESYSREPO_RESULT(result);
    }
    else
    {
        result = git_helper.open(git_folder_path.string(), log::Level::DEBUG);
        if (result.error())
        {
            //error("Couldn't open manifest directory '" + name + "'.");
        }
        else
        {
            result = git_helper.fetch(log::Level::DEBUG);
            if (result.error())
            {

            }

        }
    }

    if (result.error()) return ESYSREPO_RESULT(result);

    boost::filesystem::path file_path = git_folder_path / "directory.xml";
    if (!boost::filesystem::exists(file_path))
        return ESYSREPO_RESULT(ResultCode::FILE_NOT_EXISTING, file_path.string());

    DirectoryFileXML file_xml;

    result = file_xml.read(file_path.string());
    if (result.error()) return ESYSREPO_RESULT(result);

    directories_item->set_directory(file_xml.get_directory());

    if (get_directories() == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);
    get_directories()->add_projects(file_xml.get_directory());
    return ESYSREPO_RESULT(ResultCode::OK);
}

} // namespace esys::repo::manifest
