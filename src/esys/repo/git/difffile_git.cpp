/*!
 * \file esys/repo/git/difffile_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/difffile.h"

namespace esys::repo::git
{

DiffFile::DiffFile() = default;

DiffFile::~DiffFile() = default;

void DiffFile::set_id(const std::string &id)
{
    m_id = id;
}

std::string &DiffFile::get_id()
{
    return m_id;
}

const std::string &DiffFile::get_id() const
{
    return m_id;
}

void DiffFile::set_path(const std::string &path)
{
    m_path = path;
}

std::string &DiffFile::get_path()
{
    return m_path;
}

const std::string &DiffFile::get_path() const
{
    return m_path;
}

void DiffFile::set_size(uint64_t size)
{
    m_size = size;
}

uint64_t &DiffFile::get_size()
{
    return m_size;
}

uint64_t DiffFile::get_size() const
{
    return m_size;
}

void DiffFile::set_mode(FileMode mode)
{
    m_mode = mode;
}

FileMode DiffFile::get_mode() const
{
    return m_mode;
}

bool DiffFile::operator==(const DiffFile &other) const
{
    if (get_id() != other.get_id()) return false;
    if (get_path() != other.get_path()) return false;
    if (get_size() != other.get_size()) return false;
    if (get_mode() != other.get_mode()) return false;
    return true;
}

bool DiffFile::operator!=(const DiffFile &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git
