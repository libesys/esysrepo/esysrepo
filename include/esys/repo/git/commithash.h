/*!
 * \file esys/repo/git/commithash.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"

#include <string>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class CommitHash esys/repo/git/commithash.h "esys/repo/git/commithash.h"
 * \brief Hold information about a commit
 */
class ESYSREPO_API CommitHash
{
public:
    //! Default constructor
    CommitHash();

    //! Destructor
    virtual ~CommitHash();

    //! Set the commit hash
    /*!
     * \param[in] hash the commit hash
     */
    void set_hash(const std::string &hash);

    //! Get the commit hash
    /*!
     * \return the commit hash
     */
    const std::string &get_hash() const; //<swig_out/>

    //! Get the commit hash
    /*!
     * \return the commit hash
     */
    std::string &get_hash();

    //! Equal to comparison operator
    bool operator==(const CommitHash &other) const;

    //! Not equal to comparison operator
    bool operator!=(const CommitHash &other) const;

private:
    //!< \cond DOXY_IMPL
    std::string m_hash;
    //!< \endcond
};

} // namespace esys::repo::git

namespace std
{
ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::CommitHash &commit_has); //<swig_out/>
}
