/*!
 * \file esys/repo/libgit2/notes_libgit2.cpp.
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/libgit2/notes.h"
#include "esys/repo/libgit2/gitimpl.h"
#include "esys/repo/libgit2/git.h"

namespace esys::repo::libgit2
{

Notes::Notes(Git *git)
    : m_git(git)
{
}

Git *Notes::get_git()
{
    return m_git;
}

GitImpl *Notes::get_git_impl()
{
    return m_git->get_impl();
}

Result Notes::load_all()
{
    if (is_loaded()) return ESYSREPO_RESULT(ResultCode::OK);

    Result result;

    for (auto const &ref : get_git()->get_notes_references())
    {
        result = load(ref);
        if (result.error()) return ESYSREPO_RESULT(result);
    }

    set_loaded(true);

    return ESYSREPO_RESULT(ResultCode::OK);
}

Result Notes::load(const std::string &ref)
{
    Guard<git_iterator> it;
    std::string the_ref = GitImpl::normalize_notes_ref(ref);

    auto result_int = git_note_iterator_new(it.get_p(), get_git_impl()->get_repo(), the_ref.c_str());
    if (result_int != 0)
    {
        // It would seem that this error means there are no notes to iterate over
        if (result_int == GIT_ENOTFOUND) return ESYSREPO_RESULT(ResultCode::OK);
        return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int);
    }

    git_oid note_id;
    git_oid annotated_id;
    auto ref_map_notes_it = m_ref_map_notes.find(ref);

    if (ref_map_notes_it != m_ref_map_notes.end()) return ESYSREPO_RESULT(ResultCode::GENERIC_ERROR);

    auto ref_map_notes = std::make_shared<RefMapNotesItem>();
    ref_map_notes->m_ref = ref;

    m_ref_map_notes[ref] = ref_map_notes;

    do
    {
        result_int = git_note_next(&note_id, &annotated_id, it.get());
        if (result_int == 0) add_note(ref_map_notes, note_id, annotated_id);
    } while (result_int == 0);

    if (result_int == GIT_ITEROVER) return ESYSREPO_RESULT(ResultCode::OK);
    return ESYSREPO_RESULT(ResultCode::GIT_RAW_INT_ERROR, result_int);
}

Notes::OIDCommitMapNoteItem::OIDCommitMapNoteItem() = default;

Notes::OIDCommitMapNoteItem::OIDCommitMapNoteItem(Notes *notes, const git_oid &note_oid, const git_oid &comment_oid,
                                                  RefMapNotesItem *ref_map)
    : m_notes(notes)
    , m_note_oid(note_oid)
    , m_comment_oid(comment_oid)
    , m_ref_map(ref_map)
{
}

std::shared_ptr<git::Note> Notes::OIDCommitMapNoteItem::get_note()
{
    if (m_note != nullptr) return m_note;

    Guard<git_note> note_obj;

    if (m_ref.empty()) m_ref = GitImpl::normalize_notes_ref(m_ref_map->m_ref);

    auto result_int =
        git_note_read(note_obj.get_p(), m_notes->get_git_impl()->get_repo(), m_ref.c_str(), &m_comment_oid);
    if (result_int != 0) return nullptr;

    m_note = std::make_shared<git::Note>();
    auto message = git_note_message(note_obj.get());
    if (message != nullptr) m_note->set_message(message);

    m_note->set_reference(m_ref_map->m_ref);

    auto author = git_note_author(note_obj.get());
    Result result = m_notes->get_git_impl()->get_signature(author, m_note->get_author());
    if (result.error()) return nullptr;

    auto committer = git_note_committer(note_obj.get());
    result = m_notes->get_git_impl()->get_signature(author, m_note->get_committer());
    if (result.error()) return nullptr;

    return m_note;
}

bool Comparegit_oid::operator()(const git_oid &lhs, const git_oid &rhs) const
{
    for (int idx = 0; idx < GIT_OID_RAWSZ; ++idx)
    {
        if (lhs.id[idx] < rhs.id[idx]) return true;
    }
    return false;
}

void Notes::add_note(std::shared_ptr<RefMapNotesItem> map, const git_oid &note_oid, const git_oid &commit_oid)
{
    OIDCommitMapNoteItem item(this, note_oid, commit_oid, map.get());

    std::shared_ptr<OIDCommitMapNoteVec> oid_map_notes_vec;

    auto oid_map_notes_it = m_oid_map_notes.find(commit_oid);
    if (oid_map_notes_it == m_oid_map_notes.end())
    {
        oid_map_notes_vec = std::make_shared<OIDCommitMapNoteVec>();
        m_oid_map_notes[commit_oid] = oid_map_notes_vec;
    }
    else
        oid_map_notes_vec = oid_map_notes_it->second;
    oid_map_notes_vec->push_back(item);

    std::shared_ptr<OIDCommitMapNoteVec> iod_commit_map_note_vec;
    auto iod_commit_map_note_it = map->m_iod_commit_map_note.find(commit_oid);
    if (iod_commit_map_note_it == map->m_iod_commit_map_note.end())
    {
        iod_commit_map_note_vec = std::make_shared<OIDCommitMapNoteVec>();
        map->m_iod_commit_map_note[commit_oid] = iod_commit_map_note_vec;
    }
    else
        iod_commit_map_note_vec = iod_commit_map_note_it->second;

    iod_commit_map_note_vec->push_back(item);
}

Result Notes::get_note(const git_oid &note_oid, std::vector<std::shared_ptr<git::Note>> &notes) const
{
    notes.clear();

    auto it = m_oid_map_notes.find(note_oid);
    if (it == m_oid_map_notes.end()) return ESYSREPO_RESULT(ResultCode::OK);

    auto vec = it->second;

    if (vec == nullptr) return ESYSREPO_RESULT(ResultCode::OK);

    for (auto &item : *vec.get())
    {
        auto note = item.get_note();
        if (note == nullptr) return ESYSREPO_RESULT(ResultCode::INTERNAL_ERROR);
        notes.push_back(note);
    }

    return ESYSREPO_RESULT(ResultCode::OK);
}

bool Notes::is_loaded() const
{
    return m_loaded;
}

void Notes::set_loaded(bool loaded)
{
    m_loaded = loaded;
}

void Notes::clear()
{
    m_ref_map_notes.clear();
    m_oid_map_notes.clear();
    set_loaded(false);
}

} // namespace esys::repo::libgit2
