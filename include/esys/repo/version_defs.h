/*!
 * \file esys/repo/version_defs.h
 * \brief Version definitions
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#define ESYSREPO_MAJOR_VERSION 0
#define ESYSREPO_MINOR_VERSION 0
#define ESYSREPO_RELEASE_NUMBER 1
#define ESYSREPO_PATCH_VERSION 1

#define ESYSREPO_BETA_NUMBER 0
