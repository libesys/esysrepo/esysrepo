/*!
 * \file esys/repo/libgit2/test/lastcommit01_libgit2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/libgit2/git.h>

#include <libssh2.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::repo::libgit2::test
{

/*! \class LastCommit01LibGit2 esys/repo/libgit2/test/lastcommit01_libgit2.cpp
 * "esys/repo/libgit2/test/lastcommit01_libgit2.cpp"
 *
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(LastCommit01LibGit2)
{
    auto &ctrl = repo::test::TestCaseCtrl::get();

    boost::filesystem::path file_path = ctrl.delete_create_temp_folder("lastcommit01libgit2");
    ESYSTEST_REQUIRE_EQUAL(file_path.string().empty(), false);

    Git git;

    Result result =
        git.clone("ssh://git@gitlab.com/libesys/esysrepo/test.git", file_path.normalize().make_preferred().string());
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    result = git.checkout("branch_a_b_c");
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);

    git::CommitHash last_commit_hash;
    git::Commit last_commit;

    result = git.get_last_commit(last_commit_hash);
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
    ESYSTEST_REQUIRE_EQUAL(last_commit_hash.get_hash(), "3c49ba347517df805d2a2df292c19bdfcec69015");

    result = git.get_last_commit(last_commit);
    if (result.error()) std::cout << "ERROR " << result << std::endl;
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
    ESYSTEST_REQUIRE_EQUAL(last_commit.get_hash().get_hash(), "3c49ba347517df805d2a2df292c19bdfcec69015");

    result = git.close();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
}

} // namespace esys::repo::libgit2::test
