/*!
 * \file esys/repo/git/branch_git.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/esysrepo_prec.h"
#include "esys/repo/git/branch.h"

namespace esys::repo::git
{

Branch::Branch() = default;

Branch::Branch(const std::string &name, bool is_head)
    : m_name(name)
    , m_is_head(is_head)
{
}

Branch::Branch(const std::string &name, BranchType type, bool is_head)
    : m_name(name)
    , m_type(type)
    , m_is_head(is_head)
{
}

Branch::~Branch() = default;

void Branch::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &Branch::get_name() const
{
    return m_name;
}

void Branch::set_ref_name(const std::string &ref_name)
{
    m_ref_name = ref_name;
}

const std::string &Branch::get_ref_name() const
{
    return m_ref_name;
}

void Branch::set_type(BranchType type)
{
    m_type = type;
}

BranchType Branch::get_type() const
{
    return m_type;
}

void Branch::set_is_head(bool is_head)
{
    m_is_head = is_head;
}

bool Branch::get_is_head() const
{
    return m_is_head;
}

void Branch::set_remote_branch(const std::string &remote_tracking)
{
    m_remote_branch = remote_tracking;
}

const std::string &Branch::get_remote_branch() const
{
    return m_remote_branch;
}

void Branch::set_remote_branch_name(const std::string &remote_branch_name)
{
    m_remote_branch_name = remote_branch_name;
}

const std::string &Branch::get_remote_branch_name() const
{
    return m_remote_branch_name;
}

void Branch::set_remote_name(const std::string &remote_name)
{
    m_remote_name = remote_name;
}

const std::string &Branch::get_remote_name() const
{
    return m_remote_name;
}

void Branch::set_detached(bool detached)
{
    m_detached = detached;
}

bool Branch::get_detached() const
{
    return m_detached;
}

bool Branch::operator==(const Branch &other) const
{
    if (get_name() != other.get_name()) return false;
    if (get_ref_name() != other.get_ref_name()) return false;
    if (get_type() != other.get_type()) return false;
    if (get_is_head() != other.get_is_head()) return false;

    //!\TODO should other parameter be added?
    return true;
}

bool Branch::operator!=(const Branch &other) const
{
    return !operator==(other);
}

} // namespace esys::repo::git

namespace std
{

ESYSREPO_API ostream &operator<<(ostream &os, const esys::repo::git::Branch &branch)
{
    os << "name : " << branch.get_name();
    if (branch.get_is_head()) os << " [HEAD]";
    if (branch.get_is_head()) os << " [detached]";
    os << std::endl;
    os << "ref  : " << branch.get_ref_name() << std::endl;

    return os;
}

} // namespace std
