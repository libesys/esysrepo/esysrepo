/*!
 * \file esys/repo/manifest/loadergitsuper.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/loaderbase.h"

namespace esys
{

namespace repo
{

namespace manifest
{

/*! \class LoaderGitSuper esys/repo/manifest/loadergitsuper.h "esys/repo/manifest/loadergitsuper.h"
 * \brief Load a git super project
 */
class ESYSREPO_API LoaderGitSuper : public LoaderBase
{
public:
    //! Default constructor
    LoaderGitSuper();

    //! Destructor
    ~LoaderGitSuper() override;

    Result run() override;
};

} // namespace manifest

} // namespace repo

} // namespace esys
