/*!
 * \file esys/repo/git/signature.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/person.h"

#include <chrono>
#include <string>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class Signature esys/repo/git/signature.h "esys/repo/git/signature.h"
 * \brief Class to hold all information of a Git signature
 */
class ESYSREPO_API Signature: public Person
{
public:
    //! Default constructor
    Signature();

    //! Destructor
    ~Signature();

    //! Set the date and time
    /*!
     * \param[in] date_time the date and time
     */
    void set_date_time(const std::chrono::system_clock::time_point &date_time);

    //! Get the date and time
    /*!
     * \return the date and time
     */
    const std::chrono::system_clock::time_point &get_date_time() const;

    //! Set the offset
    /*!
     * \param[in] offset the offset
     */
    void set_offset(int offset);

    //! Get the offset
    /*!
     * \return the offset
     */
    int get_offset() const;

    //! Equal to comparison operator
    bool operator==(const Signature &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Signature &other) const;

private:
    //!< \cond DOXY_IMPL
    std::chrono::system_clock::time_point m_date_time; //!< The date & time
    int m_offset = 0;                                  //!< The offest
    //!< \endcond
};

} // namespace esys::repo::git
