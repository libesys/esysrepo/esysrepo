/*!
 * \file esys/repo/manifest/multifilexmlimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/manifest/multifilexml.h"

#include <esysfile/xml/data.h>

namespace esys::repo::manifest
{

/*! \class MultiFileXMLImpl esys/repo/manifest/multifilexmlimpl.h "esys/repo/manifest/multifilexmlimpl.h"
 * \brief To store the content of a Capture instance to a file
 */
class ESYSREPO_API MultiFileXMLImpl
{
public:
    //! Default constructor
    MultiFileXMLImpl(MultiFileXML *self);

    //! Destructor
    ~MultiFileXMLImpl();

    Result read(const std::string &path);

    Result read(std::istream &is);

    Result write(const std::string &path);

    Result write(std::ostream &os);

    MultiFileXML *self();

    Result read(std::shared_ptr<esysfile::xml::Data> data);
    Result read_capture(std::shared_ptr<esysfile::xml::Element> el);

    Result write(std::shared_ptr<esysfile::xml::Element> parent_el, std::shared_ptr<Capture> capture);
    Result write_xml();

private:
    //!< \cond DOXY_IMPL
    MultiFileXML *m_self = nullptr;
    std::shared_ptr<esysfile::xml::Data> m_xml_data;
    //!< \endcond
};

} // namespace esys::repo::manifest
