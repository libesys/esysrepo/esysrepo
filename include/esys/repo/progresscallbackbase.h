/*!
 * \file esys/repo/progresscallbackbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/progress.h"

//<swig_inc/>

namespace esys
{

namespace repo
{

class ESYSREPO_API ProgressCallbackBase
{
public:
    ProgressCallbackBase();
    virtual ~ProgressCallbackBase();

    virtual void git_progress_notif(const git::Progress &progress) = 0;
};

} // namespace repo

} // namespace esys
