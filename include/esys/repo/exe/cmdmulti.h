/*!
 * \file esys/repo/exe/cmdmulti.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/exe/cmd.h"
#include "esys/repo/git/commit.h"

#include <memory>
#include <ostream>
#include <vector>

#ifdef DELETE
#undef DELETE
#endif

namespace esys::repo::exe
{

/*! \class CmdMulti esys/repo/exe/cmdmulti.h "esys/repo/exe/cmdmulti.h"
 * \brief Multi command
 */
class ESYSREPO_API CmdMulti : public Cmd
{
public:
    enum class Task
    {
        NOT_SET,
        CAPTURE,
        SHOW,
        DELETE,
        PUSH
    };

    //! Default constructor
    CmdMulti();

    //! Destructor
    ~CmdMulti() override;

    void set_task(Task task);
    Task get_task() const;

    Result do_task_capture();
    Result do_task_delete();
    Result do_task_push();
    Result do_task_show();

protected:
    //!< \cond DOXY_IMPL
    Result impl_run() override;

private:
    Task m_task;
    //!< \endcond
};

} // namespace esys::repo::exe
