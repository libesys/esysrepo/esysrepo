/*!
 * \file esys/repo/git/filestatus.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/repo/esysrepo_defs.h"
#include "esys/repo/git/status.h"

#include <memory>

//<swig_inc/>

namespace esys::repo::git
{

/*! \class FileStatus esys/repo/git/filestatus.h "esys/repo/git/filestatus.h"
 * \brief Status information about one file
 */
class ESYSREPO_API FileStatus
{
public:
    FileStatus(const std::string &old_name = "");
    ~FileStatus();

    void set_old_name(const std::string &old_name);
    const std::string &get_old_name() const;

    void add(std::shared_ptr<Status> status);

    void set_head_to_index(std::shared_ptr<Status> head_to_index);
    std::shared_ptr<Status> get_head_to_index() const;

    void set_index_to_work_dir(std::shared_ptr<Status> index_to_work_dir);
    std::shared_ptr<Status> get_index_to_work_dir() const;

    std::vector<std::shared_ptr<Status>> &get_status();
    const std::vector<std::shared_ptr<Status>> &get_status() const;

    //! Equal to comparison operator
    bool operator==(const FileStatus &other) const;

    //! Not equal to comparison operator
    bool operator!=(const FileStatus &other) const;

private:
    //!< \cond DOXY_IMPL
    bool is_equal(const std::vector<std::shared_ptr<Status>> &left,
                  const std::vector<std::shared_ptr<Status>> &right) const;

    std::string m_old_name;
    std::shared_ptr<Status> m_head_to_index;
    std::shared_ptr<Status> m_index_to_work_dir;
    std::vector<std::shared_ptr<Status>> m_status;
    //!< \endcond
};

} // namespace esys::repo::git
