/*!
 * \file esys/repo/manifest/test/detect01_manifest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/repo/test/esysrepo_t_prec.h"

#include <esys/repo/grepo/manifest.h>
#include <esys/repo/manifest/detect.h>

namespace esys::repo::manifest::test
{

/*! \class Detect01 esys/repo/manifest/test/detect01_manifest.cpp
 * "esys/repo/manifest/test/detect01_manifest.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(Detect01)
{
    boost::filesystem::path folder_path;

    folder_path = repo::test::TestCaseCtrl::get().GetTestFilesFolder();
    folder_path /= grepo::Manifest::get_folder_name();
    folder_path /= "repo01";

    Detect detect;

    detect.set_folder_path(folder_path.string());
    Result result = detect.detect();
    ESYSTEST_REQUIRE_EQUAL(result.ok(), true);
}

} // namespace esys::repo::manifest::test
